import React from "react";

export interface IPropImage {
  height: number | string;
  src: string;
}
export default function ImageBackground(props: IPropImage) {
  const { src, height } = props;
  return (
    <div
      style={{
        width: "100%",
        // height: height,
        // backgroundImage: `url(${src})`,
      }}
      className="image-bg"
    >
      <img src={src} alt="" />
    </div>
  );
}
