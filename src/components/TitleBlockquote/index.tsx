import React, { ReactNode } from "react";
import { Col, Row } from "react-bootstrap";
import "./title-block-qoute.css";
export interface IPropsTitleBlockquote {
  title: ReactNode;
  blockquote: string;
  paragraph1: ReactNode;
  paragraph2: ReactNode;
}
export default function TitleBlockquote(props: IPropsTitleBlockquote) {
  const { title, blockquote, paragraph1, paragraph2 } = props;
  return (
    <>
      <Row style={{ justifyContent: "center" }} className="gy-lg-4 gy-3">
        <Col xs={12} sm={10} lg={10}>
          <div
            className="text-topic-joan text-topic-line"
          >
            {title}
          </div>
        </Col>
        <Col className="flex-center py-1 py-sm-2" xs={12} lg={9} sm={8} >
          <blockquote className="text-center">{blockquote}</blockquote>
        </Col>
      </Row>
      <Row className="mt-1">
        <Col lg={6} xs={12} sm={6}>
          <div className="text-paragraph">{paragraph1}</div>
        </Col>
        <Col lg={6} xs={12} sm={6}>
          <div className="text-paragraph">{paragraph2}</div>
        </Col>
      </Row>
    </>
  );
}
