import React from "react";
import { Col, Row } from "react-bootstrap";

interface IPropsLayout {
  children: JSX.Element;
}
export default function ContainerOrange({ children }: IPropsLayout) {
  return <div className="bg-gradient-style2 px-3 py-3 flex-center h-100">{children}</div>;
}
