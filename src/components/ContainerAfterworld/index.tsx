import React from "react";
import { Col, Row } from "react-bootstrap";

interface IPropsLayout {
  children: JSX.Element;
  dividerCaption: string;
  captionBottom?: string;
}
export default function ContainerAfterWorld({
  children,
  dividerCaption,
}: // captionBottom,
IPropsLayout) {
  return (
    <div className="bg-gradient-style1 px-3 py-3 h-100">
      <Row className="p-0 mb-3 ">
        <Col sm={12} lg={12} xs={12}>
          <p style={{ color: "#A7755A" }} className="text-divider-grey mb-1">
            {dividerCaption}
          </p>
          <hr className="divider-orange" />
        </Col>
      </Row>
      {/* <Row className="p-0 mb-3 d-flex d-lg-none d-sm-none">
        <Col sm={8} lg={10} xs={3} className="">
          <p className="text-divider-grey mb-1">inspire</p>
          <hr className="divider-orange" />
        </Col>
        <Col sm={4} lg={2} xs={12}>
          <p style={{ color: "#A7755A" }} className="text-divider-grey mb-1">
            {dividerCaption}
          </p>
          <hr className="divider-orange" />
        </Col>
      </Row> */}
      <div className="h-100 ">{children} </div>
    </div>
  );
}
