import React from "react";
import { Container } from "react-bootstrap";

interface IProps {
  children: JSX.Element;
  bgcolor?: string;
}

const WrapperContent = ({ children, bgcolor }: IProps) => {
  return (
    <div
      className="h-100 p-sm-3 p-lg-0 p-2 py-5 py-sm-5 "
    >
      {children}
    </div>
  );
};

export default WrapperContent;
