import React, { useEffect } from "react";
import { Col, Row } from "react-bootstrap";
import "./Inpractice.css";

interface IProps {
  title: string;
  paragraph: any[];
  img?: string;
}

const Inpractice = ({ title, paragraph, img }: IProps) => {
  return (
    <>
      <div className="wrapper-inp-content">
        <div className="inp-title">
          <div className="inp-pin">
            <h2>
              in
              <br />
              practice
            </h2>
          </div>
          <h1
            style={{ fontFamily: "monster" }}
            className="px-lg-4  pt-3 pb-4 mb-0"
          >
            {title}
          </h1>
        </div>
        {/* with-img */}
        <div className={`inp-body ${img ? "with-img" : ""} pt-2`}>
          <Row className="w-100 m-0 p-0">
            {paragraph.map((item: any, index: number) => {
              return (
                <Col className="p-0 p-lg-3" lg={6} sm={12} key={index} id={`col-${index}`}>
                  <div className="text-paragraph">{item}</div>
                </Col>
              );
            })}
          </Row>
        </div>
        <div>
          {img && (
            <img
              src={img}
              style={{ height: "auto", objectFit: "inherit" }}
              alt=""
              className="mt-3"
            />
          )}
        </div>
      </div>
    </>
  );
};

export default Inpractice;
