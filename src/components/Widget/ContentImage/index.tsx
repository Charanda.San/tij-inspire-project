import React from "react";
import { Col, Row } from "react-bootstrap";
import "./content-image.css";

interface IParagraph {
  col: number;
  content: any;
}

interface IImage {
  path: string;
  description?: string;
}

interface IProps {
  title: string;
  paragraph: any[] | IParagraph[];
  img?: IImage;
  bgColor?: string;
  border?: boolean;
  height?: string | number;
}
const ContentImage = ({
  title,
  paragraph,
  img,
  bgColor,
  height,
  border = true,
}: IProps) => {
  return (
    <div
      style={{
        backgroundColor: bgColor ? bgColor : "#B49479",
        padding: "10px",
        height: "fit-content",
      }}
    >
      <div style={{ border: border ? "1px solid #E6D9BA" : "none" }}>
        <div
          style={{ backgroundColor: "#E6D9BA", padding: "5px 0" }}
          className="text-center"
        >
          <h5
            style={{ color: "#AE5023", fontSize: "18px" }}
            className="m-0 text-lora"
          >
            {title}
          </h5>
        </div>

        <div
          className="py-2 d-flex flex-column "
          style={{ height: "calc(100% - 34px)" }}
        >
          {img && (
            <div className="mx-3 mb-3 position-relative">
              <img
                src={img.path}
                alt=""
                style={{ height: "auto" }}
                // className="" 
              />
              {img.description && (
                <div className="position-absolute text-content-image-description">{img.description}</div>
              )}
            </div>
          )}
          <div
            className="text-paragraph p-1 mt-3 mt-lg-0"
            style={{ flex: "1" }}
          >
            <Row className="gx-2 ">
              {paragraph.map((item: any) => {
                return (
                  <Col lg={item?.col ? item?.col : 6} className="px-2  px-lg-2">
                    {item?.col ? item?.content : item}
                  </Col>
                );
              })}
            </Row>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContentImage;
