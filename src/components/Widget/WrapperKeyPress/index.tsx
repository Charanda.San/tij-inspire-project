import React, { useCallback, useEffect, useState } from "react";
import { Outlet } from "react-router-dom";

interface IProps {
  children: any;
}

const WrapperKeyPress = ({ children }: IProps) => {
  // const [key, setKey] = useState(0);
  useEffect(() => {
    const handleKeyPress = (e: any) => {
      const keyCode: number = e.keyCode;
      // 37 ArrowLeft
      // 39 Arrow Right
      if (keyCode === 37) {
        (document.getElementsByClassName("click-prev-btn")[0] as any).click();
        // setKey(keyCode);
      }

      if (keyCode === 39) {
        (document.getElementsByClassName("click-next-btn")[0] as any).click();
        // setKey(keyCode);
      }
    };
    window.addEventListener("keyup", (e) => handleKeyPress(e));
    return () => {
      window.removeEventListener("keyup", (e) => handleKeyPress(e));
    };
  }, []);

  return <>{children}</>;
};

export default WrapperKeyPress;
