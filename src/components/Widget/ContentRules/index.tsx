import React from "react";
import { Col, Row } from "react-bootstrap";
import "./ContentRules.css";

interface IRules {
  rule: number;
  text: any;
}

interface IProps {
  title: string;
  content: IRules[];
  type?: "rule" | "strategy";
  top?: any;
  bottom?: any;
}
const ContentRules = ({
  title,
  content,
  type = "rule",
  top,
  bottom,
}: IProps) => {
  return (
    <div
      style={{
        // backgroundColor: "#AE7A5A",
        padding: "10px",
      }}
      // className="h-100"
    >
      <div style={{ border: "1px solid #E6D9BA" }} className="h-100 p-1">
        <div
          style={{ backgroundColor: "#AE7A5A", padding: "5px" }}
          className="text-center title-eb"
        >
          <h5 style={{ color: "#E6D9BA", fontSize: "14px",fontWeight:600 }} className="m-0">
            {title}
          </h5>
        </div>

        <div
          className="d-flex flex-column"
          style={{ height: "calc(100% - 34px)", backgroundColor: "#E6D9BA" }}
        >
          <div className="text-param p-1 " style={{ flex: "1" }}>
            {top && (
              <Row>
                <Col lg={12}>{top}</Col>
              </Row>
            )}
            <Row>
              {content.map((item: IRules) => {
                return (
                  <Col lg={12} className="item-rule">
                    {type === "rule" ? (
                      <div className="text-center py-1 rule-content">
                        <h5>Rule {item.rule}</h5>
                        <p >{item.text}</p>
                      </div>
                    ) : (
                      <div className="text-center py-2 rule-content">
                        <p>{item.text}</p>
                        <h5>Strategy {item.rule}</h5>
                      </div>
                    )}

                  </Col>
                );
              })}
            </Row>
            {bottom && (
              <Row>
                <Col lg={12}>{bottom}</Col>
              </Row>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContentRules;
