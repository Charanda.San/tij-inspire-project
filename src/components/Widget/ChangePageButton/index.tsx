import React, { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./change-page-button.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { routes } from "src/routes";
import StickyFooter from "@/components/StickyFooter";

interface IChangeButton {
  type: "next" | "prev";
  setRoute: (e: any) => void;
  route: number;
}
const ChangePageButton = ({ type, setRoute, route }: IChangeButton) => {
  const navigate = useNavigate();
  const [y, setY] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", (e) => handleNavigation(e));
    return () => {
      window.removeEventListener("scroll", (e) => handleNavigation(e));
    };
  }, [y]);

  

  const handleNavigation = useCallback(
    (e: any) => {
      const window = e.currentTarget;
      setY(window.scrollY > getOffsetPage());
    },
    [y]
  );

  const handleNext = () => {
    if (route < routes.length - 1) {
      setRoute(route + 1);
      // navigate(routes[route + 1].path);
    }
  };

  const handlePrevious = () => {
    if (route > 0) {
      setRoute(route - 1);
      // navigate(routes[route - 1].path);
    }
  };

  const getOffsetPage = (): number => {
    const pageEml: any = document.getElementsByClassName("page")[0];
    return pageEml?.offsetHeight - window.innerHeight;
  };

  return (
    <>
     <div
      className={`wrapper-button ${type}-btn d-none d-lg-flex`}
    >
      {type === "prev" ? (
        <button
          className={`button-page click-${type}-btn`}
          onClick={handlePrevious}
        >
          <FontAwesomeIcon icon={faChevronLeft} />
          <span style={{ marginLeft: "3px" }} className="span-button">
            PREV
          </span>
        </button>
      ) : (
        <button
          className={`button-page click-${type}-btn`}
          onClick={handleNext}
        >
          <span style={{ marginRight: "3px" }} className="span-button">
            NEXT
          </span>
          <FontAwesomeIcon icon={faChevronRight} />
        </button>
      )}
    </div>
    <div className="d-flex d-lg-none align-items-center">
    {type === "prev" ? (
        <button
          className='button-page'
          onClick={handlePrevious}
        >
          <FontAwesomeIcon style={{fontSize:16 ,marginRight:5}} icon={faChevronLeft} />
          <span  className="span-button-mobile">
            PREV
          </span>
        </button>
      ) : (
        <button
        className='button-page'
        onClick={handleNext}
        >
          <span  className="span-button-mobile">
            NEXT
          </span>
          <FontAwesomeIcon style={{fontSize:16 ,marginLeft:5}} icon={faChevronRight} />
        </button>
      )}
    </div>
    </>
   
  );
};

export default ChangePageButton;
