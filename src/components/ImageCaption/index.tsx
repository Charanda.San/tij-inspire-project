import React from "react";
import { Col, Row } from "react-bootstrap";

export interface IPropsImageCaption {
  image: string;
  captionText: string;
  positionX: "left" | "right";
  positionY: "top" | "bottom";
}
export default function ImageCaption(props: IPropsImageCaption) {
  const { image, captionText, positionX, positionY } = props;
  const Text = () => (
    <p
      color="#4D4E4D"
      style={{ textAlign: positionX === "left" ? "right" : "left" }}
      className="text-caption-image"
    >
      {captionText}
    </p>
  );
  const TextBottom = () => (
    <p
      color="#4D4E4D"
      style={{ textAlign: positionX === "left" ? "right" : "left" }}
      className="text-caption-image position-text-image position-relative"
    >
      {captionText}
    </p>
  );
  const Image = () => (
    <Col xs={12} sm={12} lg={9}>
      <div style={{ height: "100%", marginBottom: "10px" }}>
        <img src={image} className="image-page" />
      </div>
    </Col>
  );
  const CaptionTop = () => (
    <Col xs={12} sm={12} lg={3} className="">
      <Text />
    </Col>
  );

  const CaptionBottom = () => (
    <Col xs={12} sm={12} lg={3} className="position-relative">
      <TextBottom />
    </Col>
  );

  return (
    <>
      <Row className="gy-1 gx-2">
        {positionX === "left" ? (
          <>
            {positionY === "top" ? <CaptionTop /> : <CaptionBottom />}
            <Image />
          </>
        ) : (
          <>
            <Image />
            {positionY === "top" ? <CaptionTop /> : <CaptionBottom />}
          </>
        )}
      </Row>
    </>
  );
}
