import React, { ReactNode } from "react";
import { Col, Row } from "react-bootstrap";
import "./ContentGreen.css";

export interface IPropsContent {
  title: ReactNode;
  subTitle: string;
  paragraphs: ReactNode[];
  componentRightSide: ReactNode;
  textDividerLeft: string;
  textDividerRight: string;
  character: string;
}
export default function ContentGreen(props: IPropsContent) {
  const {
    title,
    subTitle,
    paragraphs,
    character,
    componentRightSide,
    textDividerLeft,
    textDividerRight,
  } = props;
  return (
    <>
      <Row style={{ height: "100%" }} className="pd-0">
        <Col
          xl={6}
          sm={12}
          xs={12}
          className="bg-green py-3 px-4 px-sm-4 content-height-scroll-content-green h-100"
        >
          {/* <p className="text-divider-grey mb-1 hidden-xs">{textDividerLeft}</p> */}
          {/* เปลี่ยนตาม requirement >> หัวกระดาษ ที่เป็น คำว่า INSPIRE ซ้ายมือ และชื่อบทขวามือ เด้งไปมา บางอันก็อยู่บจสุด บางอันอยู่กลางหน้า สามารถตัด INSPIRE ออกจากทุกหน้า แล้ว ปรับชื่อบนอยู่บนมุมซ้ายแทนได้หรือไม่  */}
          <p className="text-divider-grey mb-1 ">{textDividerRight}</p>

          {/* <p className="text-divider-grey mb-1 d-lg-none d-sm-none d-block">
            {textDividerRight}
          </p> */}

          <hr className="divider-orange" />
          <h4 className="title-eb mt-3 mb-4">{title}</h4>
          <Row className="mt-4 gx-2">
            <Col xs={12} sm={10} lg={10}>
              <div>
                <p className="text-roboto-condensed fst-italic fw-bold">
                  <span className="first-cha">{character}</span>
                  {subTitle}
                </p>
                <hr style={{ width: 50 }} className="divider-white" />
              </div>
            </Col>
            {paragraphs.map((paragraph: ReactNode) => (
              <Col xl={4} xs={12}>
                <p className="text-roboto-condensed">{paragraph}</p>
              </Col>
            ))}
          </Row>
        </Col>
        <Col
          xl={6}
          sm={12}
          xs={12}
          style={{ backgroundColor: "#F5F3EA" }}
          className="py-3 px-4 content-height-scroll-content-green h-100  "
        >
          {/* <div className="hidden-xs">
            <p style={{ textAlign: "end" }} className="text-divider-grey mb-1 ">
              {textDividerRight}
            </p>
            <hr className="divider-orange" />
          </div> */}

          <div className="mt-1 px-sm-2">{componentRightSide}</div>
        </Col>
      </Row>
    </>
  );
}
