import React from "react";
import { Col, Row } from "react-bootstrap";
import QuoteImage from "../QuoteImage";

export interface IPropsImageWithQuote {
  src: string;
  caption: string;
  quote: string;
  subQuote: any;
}
export default function ImageWiteQuote({
  subQuote,
  quote,
  src,
  caption,
}: IPropsImageWithQuote) {
  return (
    <>
      <Row
        className="bg-gradient-style1 px-lg-3 py-lg-2 py-3 px-3 py-sm-4 flex-center"
        style={{ height: "100%" }}
      >
        <Col xs={12} sm={10} lg={9}>
          <Row className="gy-lg-3 gy-3">
            <Col xs={12} sm={12} lg={12}>
              <img src={src} className="image-page" />
            </Col>
            <Col xs={12} sm={6} lg={4}  >
              <p style={{fontFamily: 'Neue Haas Unica'}} className="text-caption-image">{caption}</p>
            </Col>
          </Row>
        </Col>
        <Col xs={12} sm={8} lg={3} className="px-lg-4 mt-3 mt-sm-4">
          <QuoteImage quote={quote} subQuote={subQuote} fontSub='Neue Haas Unica' />
        </Col>
      </Row>
    </>
  );
}
