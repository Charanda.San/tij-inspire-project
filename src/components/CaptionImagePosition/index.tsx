import { CssStyleClass } from "@fortawesome/fontawesome-svg-core";
import React, { CSSProperties } from "react";
import { Col, Row } from "react-bootstrap";

export interface ICaptionPosition {
  position: any;
  caption: any;
}
export interface IPropsCaptionPosition {
  captions: ICaptionPosition[];
  position: "top" | "bottom";
  style?: "monster" | "bookman";
}
export default function CaptionImagePosition({
  captions,
  position,
  style,
}: IPropsCaptionPosition) {
  const className = position === "bottom" ? "position-text-image" : "";
  const TextCaption = ({ position, caption }: ICaptionPosition) =>
    style === "monster" ? (
      <p
        style={{ fontFamily: "Neue Haas Unica" }}
        className="text-caption-image mb-2"
      >
        <b style={{ fontWeight: 500, fontFamily: "Monster" }}>{position}</b>
        {caption}
      </p>
    ) : (
      <p className="text-caption-image mb-2">
        <b style={{ fontWeight: 700 }}>{position}</b>
        {caption}
      </p>
    );

  return (
    <div className={className}>
      {captions.map((text) => (
        <TextCaption position={text.position} caption={text.caption} />
      ))}
    </div>
  );
}
