import React, { ReactNode } from "react";
import { Col, Row } from "react-bootstrap";
import "./LayoutText.css";

export interface LayoutTextProps {
  leftComponent: ReactNode;
  rightComponent: ReactNode;
  divider?: string;
}

export default function LayoutText({
  leftComponent,
  rightComponent,
}: LayoutTextProps) {
  return (
    <div className="div-outside">
      <Row className="bg-white px-lg-2 py-lg-3  h-100 px-2 py-2 gy-3 gy-lg-0 ">
        <Col lg={6} className="overflow-auto h-100">
          {leftComponent}
        </Col>
        <Col lg={6} className="overflow-auto h-100">
          {rightComponent}
        </Col>
      </Row>
    </div>
  );
}
