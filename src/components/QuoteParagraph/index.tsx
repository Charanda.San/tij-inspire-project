import React from "react";
import "./QouteParagraph.css";

interface IQuoteParagraph {
  quote: any;
  subQuote: any;
  color?: string;
}

const QuoteParagraph = ({ quote, subQuote, color }: IQuoteParagraph) => {
  return (
    <div
      style={{ color: color ?? "#000000" }}
      className="d-block mx-auto px-0 px-lg-3"
    >
      <p className="text-center quote-paragraph-text">{quote}</p>
      <div className="d-block ms-auto" style={{ width: "60%" }}>
        <p className="text-center quote-paragraph-sub-text">{subQuote}</p>
      </div>
    </div>
  );
};

export default QuoteParagraph;
