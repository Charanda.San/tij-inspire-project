import React from "react";
import { Col, Row } from "react-bootstrap";

interface IPropsLayout {
  children: JSX.Element;
  dividerCaption: string;
}
export default function ContainerLight({
  children,
  dividerCaption,
}: IPropsLayout) {
  return (
    <div className="bg-gradient-style1 px-3 px-lg-4 py-3 h-100 ">
      <div className="p-0 mb-3 d-flex w-100 ">
        {/* <div style={{ width: "65%" }} className="hidden-xs">
          <p className="text-divider-grey mb-1 ">inspire</p>
          <hr className="divider-orange" />
        </div> */}
        <div
          className=" "
          style={{ width: "100%" }}
        >
          <div style={{ width: "100%" }}>
            <p className="text-divider-grey mb-1 ">{dividerCaption}</p>
            <hr className="divider-orange" />
          </div>
        </div>
      </div>
      {/* <div className="p-0 mb-3 d-flex w-100 d-none d-lg-none d-sm-flex">
        <div style={{ width: "50%" }} className="hidden-xs">
          <p className="text-divider-grey mb-1 ">inspire</p>
          <hr className="divider-orange" />
        </div>
        <div
          className="d-flex justify-content-end "
          style={{ width: "50%", marginLeft: "5px" }}
        >
          <div style={{ width: "fit-content" }}>
            <p className="text-divider-grey mb-1">{dividerCaption}</p>
            <hr className="divider-orange" />
          </div>
        </div>
      
      </div> */}
      {/* <div className=" mb-2 d-block d-lg-none d-sm-none">
        <div style={{ width: "100%" }}>
          <p className="text-divider-grey mb-1">{dividerCaption}</p>
          <hr className="divider-orange w-100" />
          </div>
        </div> */}
      <div
        style={{ height: "95%" }}
        className=" overflow-auto overflow-x-hidden"
      >
        {children}
      </div>
    </div>
  );
}
