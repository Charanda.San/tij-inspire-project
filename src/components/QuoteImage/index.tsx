import React from "react";
import "./quote-image.css";
export interface iPropsQuoteImage {
  quote: any;
  subQuote: any;
  fontSub?: string;
}
export default function QuoteImage({ quote, subQuote ,fontSub}: iPropsQuoteImage) {
  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <hr className="divider-grey" />
      </div>

      <p style={{fontFamily: fontSub ?? 'SlimBach'}}  className="quote-text mt-0 mb-2">{quote}</p>

      <p style={{fontFamily: fontSub ?? 'bookman'}} className="quote-sub-text mt-2 mb-2">{subQuote}</p>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <hr className="divider-grey" />
      </div>
    </>
  );
}
