import React from "react";
import { Dropdown } from "react-bootstrap";
import "./dropdownMunus.css";

interface IProps {
  children: any;
}

const menuItem = [
  {
    page: "11",
    name: "Foreword",
    path: "/page-08-content",
  },
  {
    page: "12",
    name: "Introduction: Criminal Justice and the Rule of Law",
    path: "/page-09-chapter",
  },
  {
    page: "26",
    name: "Chapter 1: The Rights of Women",
    path: "/page-16-chapter",
  },
  {
    page: "64",
    name: "Chapter 2: The Rights of Children",
    path: "/page-35-chapter",
  },
  {
    page: "92",
    name: "Chapter 3: The Rule of Law and Sustainable Development",
    path: "/page-49-chapter",
  },
  {
    page: "120",
    name: "Afterword",
    path: "/page-63-content",
  },
  {
    page: "128",
    name: "Appendices",
    sub_path: [
      {
        page: "130",
        name: "Timeline of the Work of HRH Princess Bajrakitiyabha ",
        path: "/page-68-content",
      },
      {
        page: "136",
        name: "Glossary",
        path: "/page-71-content",
      },
      {
        page: "138",
        name: "TIJ Profile",
        path: "/page-72-content",
      },
    ],
  },
];

const DropdownMenus = ({ children }: IProps) => {
  return (
    <Dropdown align={{ lg: "start" }}>
      <Dropdown.Toggle
        // id="dropdown-basic"
        style={{
          backgroundColor: "transparent",
          color: "#54595F",
          border: "none",
        }}
      >
        {children}
      </Dropdown.Toggle>

      <Dropdown.Menu className="wrapper-dropdown-menu">
        {menuItem.map((item: any, index: number) => {
          return (
            <div key={index}>
              <Dropdown.Item
                href={item.path ?? ""}
                // style={{ backgroundColor: "#FCFBF6" }}
                className="item-dropdown"
              >
                <div className="d-flex justify-content-between menu-item mb-3">
                  <p className="text-paging">{item.name}</p>
                  <p className="text-paging">{item.page}</p>
                </div>
              </Dropdown.Item>
              {item.sub_path &&
                item.sub_path.map((_item: any, _index: number) => {
                  return (
                    <Dropdown.Item
                      key={_item.path}
                      href={_item.path ?? ""}
                      className="item-dropdown"
                    >
                      <div className="d-flex justify-content-between menu-item mb-3">
                        <p className="ps-4 text-paging">{_item.name}</p>
                        <p className="text-paging">{_item.page}</p>
                      </div>
                    </Dropdown.Item>
                  );
                })}
            </div>
          );
        })}
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default DropdownMenus;
