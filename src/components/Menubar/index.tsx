import React, {  useRef, useState } from "react";
import { Container, Navbar } from "react-bootstrap";
import "./menubar.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faSearch } from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";
import { DropdownMenus } from "src/components";

export default function Menubar() {
  const navigate = useNavigate();
  const inputRef = useRef(null);
  const [searchText, setSearchText] = useState<string>("");
  const [openSearch, setOpenSearch] = useState<boolean>(false);
  const menuItem = [
    {
      page: "11",
      name: "Foreword",
      path: "/page-08-content",
    },
    {
      page: "12",
      name: "Introduction: Criminal Justice and the Rule of Law",
      path: "/page-09-chapter",
    },
    {
      page: "26",
      name: "Chapter 1: The Rights of Women",
      path: "/page-16-chapter",
    },
    {
      page: "64",
      name: "Chapter 2: The Rights of Children",
      path: "/page-35-chapter",
    },
    {
      page: "92",
      name: "Chapter 3: The Rule of Law and Sustainable Development",
      path: "/page-49-chapter",
    },
    {
      page: "120",
      name: "Afterword",
      path: "/page-63-content",
    },
    {
      page: "128",
      name: "Appendices",
      sub_path: [
        {
          page: "130",
          name: "Timeline of the Work of HRH Princess Bajrakitiyabha ",
          path: "/page-68-content",
        },
        {
          page: "136",
          name: "Glossary",
          path: "/page-71-content",
        },
        {
          page: "138",
          name: "TIJ Profile",
          path: "/page-72-content",
        },
      ],
    },
  ];
  const handleClickSearch = () => {
    if (searchText !== "") {
      navigate(`/page-search/${searchText}`);
      setOpenSearch(false);
    }
  };

  // useEffect(() => {
  //   const handleClickOutside = (event: any) => {
  //     console.log(openSearch);
  //     if (
  //       inputRef.current &&
  //       !(inputRef.current as any).contains(event.target) &&
  //       openSearch
  //     ) {
  //       console.log("test3333");
  //       setOpenSearch(!openSearch);
  //     }else{
  //       setOpenSearch(false);
  //     }
  //   };

  //   window.addEventListener("click", handleClickOutside);
  //   return () => {
  //     window.removeEventListener("click", handleClickOutside);
  //   };
  // }, []);

  return (
    <Navbar variant="dark" sticky="top" style={{ backgroundColor: "#FFFFFF" }}>
      <Container>
        <div style={{ display: "flex", alignItems: "center" }}>
          <Navbar.Brand href="/" className="py-0">
            <img src="./images/logo.png" className="logo-img" />
          </Navbar.Brand>
          <div className="d-none d-lg-block">
            <DropdownMenus>
              <FontAwesomeIcon icon={faBars} />
              <span
                className="text-paragraph"
                style={{
                  marginLeft: "5px",
                  fontSize: 16,
                  color: "grey",
                  letterSpacing: 1,
                }}
              >
                CONTENTS
              </span>
            </DropdownMenus>
          </div>
        </div>
        <img src={"./images/inspire_1.svg"} className="navbar-img" />
        <input
          type={"text"}
          placeholder={"Search"}
          className="search-box d-none d-lg-block"
          onKeyPress={(e: any) => {
            console.log(e);
          }}
          onKeyDown={(e: any) => {
            if ((e.keyCode === 13 || e.keyCode === 10) && e.target.value) {
              navigate(`/page-search/${e.target.value}`);
            }
          }}
        />
        <div className="d-flex align-items-center flex-end d-lg-none">
          <DropdownMenus>
            <FontAwesomeIcon icon={faBars} />
          </DropdownMenus>
          <FontAwesomeIcon
            icon={faSearch}
            className="ps-3"
            onClick={() => {
              setOpenSearch(!openSearch);
            }}
          />
          <div
            className={`wrapper-input-search ${
              openSearch ? "d-block" : "d-none"
            } d-lg-none`}
          >
            <input
              type={"text"}
              placeholder={"Search"}
              className={`search-box`}
              ref={inputRef}
              onChange={(e: any) => {
                setSearchText(e.target.value);
              }}
              onKeyDown={(e: any) => {
                if ((e.keyCode === 13 || e.keyCode === 10) && e.target.value) {
                  navigate(`/page-search/${e.target.value}`);
                }
              }}
            />
            <FontAwesomeIcon
              icon={faSearch}
              className="ps-3 icon-search"
              onClick={handleClickSearch}
            />
          </div>
        </div>
      </Container>
    </Navbar>
  );
}
