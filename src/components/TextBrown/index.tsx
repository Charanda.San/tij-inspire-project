import React from "react";

interface IPropsTextBrown {
  year?: any | undefined;
  bullets: any[];
}
export default function TextBrown({ year, bullets }: IPropsTextBrown) {
  return (
    <div className="mb-0 mt-0">
      <h5
        style={{
          lineHeight: 1.4,
          fontWeight: 700,
          display: year === undefined ? 'none' : 'block'
        }}
        className="text-brown mt-0"
      >
        {year}
      </h5>
      <ul style={{paddingLeft: '1rem',marginBottom: '10px'}}>
        {bullets.map((item: string) => (
          <li style={{fontSize: 10}} className="text-brown">{item}</li>
        ))}
      </ul>
    </div>
  );
}
