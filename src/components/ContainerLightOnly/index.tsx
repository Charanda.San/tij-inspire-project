import React from "react";
import { Col, Row } from "react-bootstrap";

interface IPropsLayout {
  children: JSX.Element;
}
export default function ContainerLightOnly({ children }: IPropsLayout) {
  return (
    <div className="bg-gradient-style1 px-4 py-3">
      <Row className="gy-2 h-100">
        <Col sm={12} lg={12}>
          <p className="text-divider-grey mb-1 ">inspire</p>
          <hr className="divider-orange" />
        </Col>
        <Col sm={12} lg={12} className="h-100">
          {children}
        </Col>
      </Row>
    </div>
  );
}
