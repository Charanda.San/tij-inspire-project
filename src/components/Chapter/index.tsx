import React from "react";
import { Row, Col } from "react-bootstrap";
import "./chapter.css";
export interface IPropsChapter {
  src: string;
  chapterText: string;
  title: string;
  content: string;
  descriptionImg: any;
}
export default function Chapter(props: IPropsChapter) {
  const { src, title, chapterText, content, descriptionImg } = props;
  return (
    <Row className="chapterStyle">
      <Col
        className="cover"
        xxl={9}
        xs={12}
        sm={12}
        style={{ backgroundImage: `url(${src})` }}
      >
        {/* <img src={src} alt="" /> */}
        <div className="description-image">
          {descriptionImg}
        </div>
      </Col>
      <Col className="p-0" xxl={3} xs={12} sm={12}>
        <div className="content p-lg-3 p-4 p-sm-5">
          <p className="chapterText">{chapterText}</p>
          {/* // eslint-disable-line no-eval */}
          <hr className="divider" />
          <p className="titleText">{title}</p>
          <p className="contentText">
            {content}
            <hr className="dividerBottom" />
          </p>
        </div>
      </Col>
    </Row>
  );
}
