import React, { ReactNode } from "react";
import { Col, Row } from "react-bootstrap";

export interface IPropsImageParagraphPage {
  paragraphs: ReactNode[];
  image: string;
  caption: string;
  positionCaption: "left" | "right";
}
export default function ImageParagraphPage(props: IPropsImageParagraphPage) {
  const { paragraphs, image, caption, positionCaption} = props;

  const Caption = () => {
    return (
      <Col xs={12} sm={2} lg={2} className="order-1 order-sm-last order-lg-last">
        <p style={{ color: "grey",textAlign:'left' }} className="text-caption-image-nue">
          {caption}
        </p>
      </Col>
    );
  };
  const Paragraphs = () => (
    <Col xs={12} sm={10} lg={10} className="order-lg-1 order-last order-sm-1 mt-0 mt-lg-2 mt-sm-3">
      <Row className="gx-2">
        {paragraphs.map((paragraph: ReactNode) => (
          <Col xl={4} xs={12}>
            <p className="text-paragraph-nue">{paragraph}</p>
          </Col>
        ))}
      </Row>
    </Col>
  );
  return (
    <>
    
      <Row className="mt-2 mt-lg-0 gx-2 gy-3 " >
        <Col xs={12} lg={12} className="mt-2 mt-lg-0 order-lg-first order-1 order-sm-first">
          <img src={image} className="image-page"/>
        </Col>
        {positionCaption === "left" ? <Caption /> : <Paragraphs />}

        {positionCaption === "left" ? <Paragraphs /> : <Caption />}
      </Row>
    </>
  );
}
