import React from "react";

export default function Footer() {
  const pathname = window.location.pathname;
  return (
    <footer className="d-flex align-items-center">
      <div className="company-footer">
        <h5
          style={{
            fontSize: "12px",
          }}
        >
          © TIJ 2023. All rights reserved.
        </h5>
        <div
          style={{
            borderBottom: "1.5px solid darkgrey",
            width: "200px",
            display: "block",
            margin: "auto",
            marginBottom: "2px",
          }}
        />
        <p style={{ fontSize: "10px" }}>
          This website and its content is copyright of TIJ
        </p>
        <p style={{ fontSize: "10px" }}>
          No part of the content may be reproduced or transmitted in any form or
          by any means without express written permission from the author,
          except in the case of brief quotations embodied in critical articles
          and reviews. Please refer all pertinent questions to the Thailand
          Institute of Justice (TIJ). Reproduction, distribution, or use for any
          commercial benefit of the content is prohibited.​
        </p>
      </div>
    </footer>
  );
}
