import React from "react";
import {
  Home,
  Page01Home,
  Page05Chapter,
  Page06Content,
  Page08Content,
  Page09Chapter,
  Page10Content,
  Page11Content,
  Page12Content,
  Page13Content,
  Page14Content,
  Page15Content,
  Page16Chapter,
  Page17Content,
  Page18Content,
  Page19Content,
  Page20Content,
  Page21Content,
  Page22Content,
  Page23Content,
  Page24Content,
  Page25Content,
  Page26Content,
  Page27Content,
  Page28Content,
  Page29Content,
  Page30Content,
  Page31Content,
  Page32Content,
  Page33Content,
  Page34Content,
  Page35Chapter,
  Page36Content,
  Page37Content,
  Page38Content,
  Page39Content,
  Page40Content,
  Page41Content,
  Page42Content,
  Page43Content,
  Page44Content,
  Page45Content,
  Page46Content,
  Page47Content,
  Page48Content,
  Page49Chapter,
  Page50Content,
  Page51Content,
  Page52Content,
  Page53Content,
  Page54Content,
  Page55Content,
  Page56Content,
  Page57Content,
  Page58Content,
  Page59Content,
  Page60Content,
  Page61Content,
  Page62Content,
  Page63Content,
  Page64Content,
  Page66Content,
  Page65Content,
  Page68Content,
  Page69Content,
  Page70Content,
  Page71Content,
  Page72Content,
  PageSearch,
} from "src/pages";

export interface IRoutes {
  path: string;
  element: React.JSX.Element;
}

export const routes: IRoutes[] = [
  {
    path: "/",
    element: <Page01Home />,
  },
  {
    path: "/page-05-chapter",
    element: <Page05Chapter />,
  },
  {
    path: "/page-06-content",
    element: <Page06Content />,
  },
  {
    path: "/page-08-content",
    element: <Page08Content />,
  },
  {
    path: "/page-09-chapter",
    element: <Page09Chapter />,
  },
  {
    path: "/page-10-content",
    element: <Page10Content />,
  },
  {
    path: "/page-11-content",
    element: <Page11Content />,
  },
  {
    path: "/page-12-content",
    element: <Page12Content />,
  },
  {
    path: "/page-13-content",
    element: <Page13Content />,
  },
  {
    path: "/page-14-content",
    element: <Page14Content />,
  },
  {
    path: "/page-15-content",
    element: <Page15Content />,
  },

  {
    path: "/page-16-chapter",
    element: <Page16Chapter />,
  },
  {
    path: "/page-17-content",
    element: <Page17Content />,
  },
  {
    path: "/page-18-content",
    element: <Page18Content />,
  },
  {
    path: "/page-19-content",
    element: <Page19Content />,
  },
  {
    path: "/page-20-content",
    element: <Page20Content />,
  },
  {
    path: "/page-21-content",
    element: <Page21Content />,
  },
  {
    path: "/page-22-content",
    element: <Page22Content />,
  },
  {
    path: "/page-23-content",
    element: <Page23Content />,
  },
  {
    path: "/page-24-content",
    element: <Page24Content />,
  },
  {
    path: "/page-25-content",
    element: <Page25Content />,
  },
  {
    path: "/page-26-content",
    element: <Page26Content />,
  },
  {
    path: "/page-27-content",
    element: <Page27Content />,
  },
  {
    path: "/page-28-content",
    element: <Page28Content />,
  },
  {
    path: "/page-29-content",
    element: <Page29Content />,
  },
  {
    path: "/page-30-content",
    element: <Page30Content />,
  },
  {
    path: "/page-31-content",
    element: <Page31Content />,
  },
  {
    path: "/page-32-content",
    element: <Page32Content />,
  },
  {
    path: "/page-33-content",
    element: <Page33Content />,
  },
  {
    path: "/page-34-content",
    element: <Page34Content />,
  },
  {
    path: "/page-35-chapter",
    element: <Page35Chapter />,
  },
  {
    path: "/page-36-content",
    element: <Page36Content />,
  },
  {
    path: "/page-37-content",
    element: <Page37Content />,
  },
  {
    path: "/page-38-content",
    element: <Page38Content />,
  },
  {
    path: "/page-39-content",
    element: <Page39Content />,
  },
  {
    path: "/page-40-content",
    element: <Page40Content />,
  },
  {
    path: "/page-41-content",
    element: <Page41Content />,
  },
  {
    path: "/page-42-content",
    element: <Page42Content />,
  },
  {
    path: "/page-43-content",
    element: <Page43Content />,
  },
  {
    path: "/page-44-content",
    element: <Page44Content />,
  },
  {
    path: "/page-45-content",
    element: <Page45Content />,
  },
  {
    path: "/page-46-content",
    element: <Page46Content />,
  },
  {
    path: "/page-47-content",
    element: <Page47Content />,
  },
  {
    path: "/page-48-content",
    element: <Page48Content />,
  },
  {
    path: "/page-49-chapter",
    element: <Page49Chapter />,
  },
  {
    path: "/page-50-content",
    element: <Page50Content />,
  },
  {
    path: "/page-51-content",
    element: <Page51Content />,
  },
  {
    path: "/page-52-content",
    element: <Page52Content />,
  },
  {
    path: "/page-53-content",
    element: <Page53Content />,
  },
  {
    path: "/page-54-content",
    element: <Page54Content />,
  },
  {
    path: "/page-55-content",
    element: <Page55Content />,
  },
  {
    path: "/page-56-content",
    element: <Page56Content />,
  },
  {
    path: "/page-57-content",
    element: <Page57Content />,
  },
  {
    path: "/page-58-content",
    element: <Page58Content />,
  },
  {
    path: "/page-59-content",
    element: <Page59Content />,
  },
  {
    path: "/page-60-content",
    element: <Page60Content />,
  },
  {
    path: "/page-61-content",
    element: <Page61Content />,
  },
  {
    path: "/page-62-content",
    element: <Page62Content />,
  },
  {
    path: "/page-63-content",
    element: <Page63Content />,
  },
  {
    path: "/page-64-content",
    element: <Page64Content />,
  },
  {
    path: "/page-65-content",
    element: <Page65Content />,
  },
  {
    path: "/page-66-content",
    element: <Page66Content />,
  },

  {
    path: "/page-68-content",
    element: <Page68Content />,
  },
  {
    path: "/page-69-content",
    element: <Page69Content />,
  },
  {
    path: "/page-70-content",
    element: <Page70Content />,
  },
  {
    path: "/page-71-content",
    element: <Page71Content />,
  },
  {
    path: "/page-72-content",
    element: <Page72Content />,
  },
];
