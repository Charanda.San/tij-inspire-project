import React, { useCallback, useEffect, useState } from "react";
import "./App.css";
import { Route, Routes, useLocation, useNavigate } from "react-router-dom";
import { Menubar, Footer } from "src/components";
import "bootstrap/dist/css/bootstrap.min.css";
import { routes, IRoutes } from "src/routes";
import { ChangePageButton } from "./components/Widget";
import { PageSearch } from "src/pages";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowUp } from "@fortawesome/free-solid-svg-icons";

function App() {
  const routecurrent: IRoutes | any = routes.find(
    (item: IRoutes) => item.path === window.location.pathname
  );
  const [routeIndex, setRouteIndex] = useState(routes.indexOf(routecurrent));
  const location = useLocation();
  const navigate = useNavigate();
  const [y, setY] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", (e) => handleNavigation(e));
    return () => {
      window.removeEventListener("scroll", (e) => handleNavigation(e));
    };
  }, [y]);

  const handleNavigation = useCallback(
    (e: any) => {
      const window = e.currentTarget;
      setY(window.scrollY > getOffsetPage());
    },
    [y]
  );

  const getOffsetPage = (): number => {
    const pageEml: any = document.getElementsByClassName("page")[0];
    return pageEml?.offsetHeight - window.innerHeight;
  };

  const handleclickToTop = () => {
    scrollTo({
      top: 0,
    });
  };

  useEffect(() => {
    scrollTo({
      top: 0,
    });
    setY(false);
    if(routes[routeIndex]){
      navigate(routes[routeIndex].path);
    }
    
  }, [routeIndex]);
  useEffect(() => {
    const handleKeyPress = (e: any) => {
      const keyCode: number = e.keyCode;
      // 37 ArrowLeft
      // 39 Arrow Right
      if (keyCode === 37) {
        if (routeIndex > 0) {
          window.location.href = routes[routeIndex - 1].path;
        }
      }

      if (keyCode === 39) {
        if (routeIndex <= routes.length) {
          window.location.href = routes[routeIndex + 1].path;
        }
      }
    };
    window.addEventListener("keyup", (e) => handleKeyPress(e));
  }, [routeIndex]);

  useEffect(() => {
    const routeFind: IRoutes | any = routes.find(
      (item: IRoutes) => item.path === location.pathname
    );
    setRouteIndex(routes.indexOf(routeFind));
  }, []);

  return (
    <>
      <Menubar />
      <div className="page w-100">
        <div
          style={{ width: "10%" }}
          id="prev-arrow"
          className="d-none d-lg-flex"
        >
          <ChangePageButton
            type="prev"
            setRoute={setRouteIndex}
            route={routeIndex}
          />
        </div>
        <div
          id="content"
          className="w-100 h-100"
          style={
            location.pathname === "/" ||
            location.pathname === "/page-08-content"
              ? { background: "white", paddingBottom: 0 }
              : { background: "white" }
          }
        >
          <Routes>
            {routes.map((route: IRoutes) => (
              <Route
                key={route.path}
                path={route.path}
                element={route.element}
              />
            ))}
            <Route>
              <Route path={"/page-search/:search"} element={<PageSearch />} />
            </Route>
          </Routes>
        </div>
        <div
          style={{ width: "10%" }}
          id="next-arrow"
          className="d-none d-lg-flex justify-content-end "
        >
          <ChangePageButton
            type="next"
            setRoute={setRouteIndex}
            route={routeIndex}
          />
        </div>
      </div>
      {location.pathname === "/" && <Footer />}

      <div
        className={`back-to-top ${y ? "d-flex" : "d-none"} d-lg-none`}
        onClick={handleclickToTop}
      >
        <FontAwesomeIcon icon={faArrowUp} />
      </div>

      <footer className="sticky-footer d-lg-none  d-flex justify-content-between align-items-center">
        <ChangePageButton
          type="prev"
          setRoute={setRouteIndex}
          route={routeIndex}
        />
        <ChangePageButton
          type="next"
          setRoute={setRouteIndex}
          route={routeIndex}
        />
      </footer>
    </>
  );
}

export default App;
