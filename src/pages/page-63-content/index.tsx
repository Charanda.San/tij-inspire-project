import { CaptionImagePosition, ContainerLight, QuoteImage } from "@/components";
import { ICaptionPosition } from "@/components/CaptionImagePosition";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page63Content() {
  return (
    <ContainerLight dividerCaption="The Rule of Law and Sustainable Development">
      <Row className="py-2 px-2">
        <Col xs={12} sm={12} lg={6} className="scroll1">
          <Row className="p-0 gx-2 gy-2">
            <Col xs={12} sm={12} lg={6}>
              <p style={{ fontSize: 24 }} className="text-paragraph ">
                AFTERWORD
              </p>
              <p style={{ fontSize: 14 }} className="text-paragraph mb-2">
                by Dr. Eduardo Vetere
              </p>
              <p className="text-paragraph mb-1">
                Vice-President, International Association of Anti-Corruption
                Authorities (IAACA)
              </p>
              <p className="text-paragraph mb-1">
                Former Director of Treaty Affairs, UNODC
              </p>
              <p className="text-paragraph mb-4 mb-lg-3">
                Executive Secretary of the Eighth, Ninth and Eleventh UN Crime
                Prevention and Criminal Justice Congresses{" "}
              </p>
            </Col>
          </Row>
          <Row>
            <Col xs={12} sm={6} lg={6}>
              <p className="text-paragraph mb-2">
                It was early afternoon in Bangkok on 18 April, 2005. At the VIP
                entrance of the Queen Sirikit National Conference Center, I was
                joined by the Secretary General of the Eleventh United Nations
                Congress on Crime Prevention and Criminal Justice and Executive
                Director of UNODC, Antonio Maria Costa, and also the newly
                elected President of the Congress and Minister of Justice of
                Thailand Suwat Liptapanlop. It was a very hot day when, after
                many motorcycle guards, we saw a number of white Rolls Royces
                approaching. First, His Royal Highness Crown Prince Maha
                Vajiralongkorn – who was set to deliver an address on behalf of
                his father, His Majesty King Bhumibol Adulyadej – arrived, soon
                followed by his daughter, Her Royal Highness Princess
                Bajrakitiyabha Mahidol. After introducing ourselves, together we
                proceeded to the Plenary Hall, where all participants were
                silently and almost religiously waiting on our arrival.
              </p>
              <p className="text-paragraph mb-2">
                On the way to the hall, Antonio Maria Costa escorted His Royal
                Highness the Crown Prince, while I escorted Her Royal Highness.
                It was quite a long walk, which gave us the opportunity to have
                a short conversation. The first thing Her Royal Highness told me
                was that her name was too difficult for foreigners to pronounce
                and to simply call her “Pat”. She then told me that she was very
                much interested in the issues to be discussed at the Congress,
                being a lawyer herself. In addition, she mentioned she was
                completing her doctorate in Law at Cornell Law School in the
                United States and was also planning to go to New York to join
                the Permanent Mission of Thailand to the United Nations in order
                to gain some diplomatic experience (she added that she held a
                degree in International Relations).
              </p>
              <p className="text-paragraph mb-2 d-none d-lg-block">
                Being truly impressed by her background and even more by her
                friendly and frank way of speaking to me, I tried my best to
                inform her
              </p>
              <p className="text-paragraph mb-2 d-block d-lg-none">
                Being truly impressed by her background and even more by her
                friendly and frank way of speaking to me, I tried my best to
                inform her that, in addition to New York, Geneva might interest
                her for matters related to human rights and Vienna for issues
                concerning drug control and crime prevention. And then I
                expressed my sincere wish to meet again in Vienna in the near
                future.
              </p>
            </Col>
            <Col xs={12} sm={6} lg={6}>
              <p className="text-paragraph mb-2 d-none d-lg-block">
                that, in addition to New York, Geneva might interest her for
                matters related to human rights and Vienna for issues concerning
                drug control and crime prevention. And then I expressed my
                sincere wish to meet again in Vienna in the near future.
              </p>
              <p className="text-paragraph mb-2">
                That was my first encounter with a member of the Thai royal
                family. And a few days later I also had the honor of receiving
                an audience with Her Royal Highness’s grandfather, His Majesty
                the King of Thailand!
              </p>
              <p className="text-paragraph mb-2">
                After one week, the Congress – thanks to the exceptional
                leadership and support of the entire Thai government –
                successfully completed its work, with the consensual and
                unanimous decision to recommend to the General Assembly the
                adoption of the “Bangkok Declaration: Strategic Alliances in
                Crime Prevention and Criminal Justice.” This declaration, it
                should be noted, was the result of extremely intensive,
                extensive and protracted negotiations in order to agree on every
                single word of the text. Another equally successful and very
                productive session of the Crime Prevention and Criminal Justice
                Commission was held in Vienna the following month to consider
                and approve the outcome of the Eleventh Congress. In addition,
                the results of its proceedings had to be brought to the
                attention of the Economic and Social Council meeting in Geneva
                for the endorsement of its draft resolutions in July, just
                before my retirement at the end of that month, after more than
                37 years of service in the United Nations Secretariat.
              </p>
              <p className="text-paragraph mb-2 d-block d-sm-none d-lg-block">
                A few months later and just to satisfy my curiosity as a happy
                retiree, I checked the summary records of the Third Committee to
                see what was happening with the adoption of the “Bangkok
                Declaration” by the General Assembly. I discovered that Her
                Royal Highness, representing Thailand at its 60<sup>th</sup>{" "}
                session, had not only delivered a
              </p>
              <p className="text-paragraph mb-2 d-none d-sm-block d-lg-none">
                A few months later and just to satisfy my curiosity as a happy
                retiree, I checked the summary records of the Third Committee to
                see what was happening with the adoption of the “Bangkok
                Declaration” by the General Assembly. I discovered that Her
                Royal Highness, representing Thailand at its 60<sup>th</sup>{" "}
                session, had not only delivered a very powerful statement, but
                also lobbied in partnership with other countries’ delegations
                for the adoption of the relevant draft resolution endorsing the
                Declaration.
              </p>
            </Col>
          </Row>
        </Col>
        <Col xs={12} sm={12} lg={6} className="scroll1">
          <Row className="gy-2 gx-2">
            <Col xs={12} sm={12} lg={10}>
              <img
                src="./images/page-63-content/image-63-01.webp"
                className="image-page"
              />
            </Col>
            <Col xs={12} sm={10} lg={2} className="position-relative">
              <p className="text-caption-image-nue d-block d-sm-block d-lg-none">
                Her Royal Highness speaks to Dr. Eduardo Vetere at an exhibition
                to showcase the Kamlangjai project in Vienna in 2008.
              </p>
              <p className="text-caption-image-nue bottom-0 position-absolute d-none d-sm-none d-lg-block">
                Her Royal Highness speaks to Dr. Eduardo Vetere at an exhibition
                to showcase the Kamlangjai project in Vienna in 2008.
              </p>
            </Col>
            <Col xs={12} sm={6} lg={6}>
              <p className="text-paragraph mb-2 d-block d-sm-none d-lg-block">
                very powerful statement, but also lobbied in partnership with
                other countries’ delegations for the adoption of the relevant
                draft resolution endorsing the Declaration.
              </p>
              <p className="text-paragraph mb-2">
                As reported in the above-mentioned summary records, she noted
                that “the dynamic and substantive exchange of views at the
                Eleventh Congress – hosted by Thailand in April 2005 –
                demonstrated what the international community could achieve when
                all parties joined hands. Moreover, its outcome document – the
                Bangkok Declaration – could serve as a good basis for concerted
                efforts to improve cooperation and coordination among States,
                United Nations agencies, intergovernmental organizations and
                NGOs”.
              </p>
            </Col>
            <Col xs={12} sm={6} lg={6}>
              <p className="text-paragraph mb-2">
                Furthermore, she stated that “her delegation also intended to
                submit a draft resolution on the follow-up to the Congress,
                counting on the wide support of all Member States.” And, on the
                recommendations of the Third Committee, the Thai draft
                resolution on “Follow up to the Eleventh United Nations Congress
                on Crime Prevention and Criminal Justice” endorsing the Bangkok
                Declaration was unanimously adopted during the 64<sup>th</sup>{" "}
                plenary meeting of the General Assembly, on 16 December 2005, as
                General Assembly resolution 60/177.
              </p>
              <p className="text-paragraph mb-2">
                In April 2008, Her Royal Highness went to Vienna, not only to
                deliver a keynote speech at the opening of the seventeenth
                session of the Commission, but also to present an exhibition to
                showcase the
              </p>
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerLight>
  );
}
