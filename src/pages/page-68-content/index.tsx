import { ContainerAfterWorld, TextBrown } from "@/components";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page68Content() {
  const year2000: string[] = [
    `Receives her Bachelor degree in Laws (LL.B.) from Thammasat University and Bachelor degree of Art in International Relations (B.A.) from Sukhothai Thammathirat University`,
    `Becomes Judge Advocate General Officer, Royalty Security Command, Thai Royal Armed Forces
  `,
  ];
  const year2001: string[] = [
    `HRH visits Bangkok’s Central Women Correctional Institution for the first time`,
    `Works as a legal officer at the Judge Advocate General's Department`,
    `Applies to do her Master’s degree in Law at Cornell Law School`,
  ];
  const year2004: string[] = [
    `Studies to become Doctor of the Science of Law (J.S.D.) at Cornell University`,
    `Completes summer internship at the Washington office of Baker & McKenzie (2002)`,
  ];
  const year2005: any[] = [
    `Receives her Doctorate degree in Law (J.S.D.) from Cornell Law School`,
    ` Becomes a Barrister-at-Law in Thailand`,
    <>
      HRH represents Thailand at the 60<sup>th</sup> United Nations General
      Assembly
    </>,
    `Becomes First Secretary, Permanent Mission of Thailand to
the United Nations in New York`,
  ];
  const year2006: string[] = [
    `Works as Assistant Public Prosecutor, Public Prosecution Official Training Institute`,
    `Establishes the Kamlangjai Project`,
  ];
  const year2007: string[] = [
    `Works as Assistant Public Prosecutor, Public Prosecution Official Training Institute serving at the Executive Director’s Office of Criminal Litigation 3`,
    `Works as Divisional Public Prosecutor, Department of Narcotics Litigation`,
    `Works as Divisional Public Prosecutor, Executive Director’s Office of Narcotics Litigation 1`,
  ];
  const year2008: any[] = [
    `Works as Assistant Provincial Public Prosecutor, Office of Udon Thani Public Prosecution`,
    <>
      An exhibition about the Kamlangjai Project is presented at the 17
      <sup>th</sup>
      Session of the CCPCJ in Vienna, Austria
    </>,
    `Initiates the “Enhancing Lives of Female Inmates” (ELFI) advocacy campaign`,
    `Launches the “Say No to Violence Against Women” campaign`,
    `Appointed the United Nations Development Fund for Women
(UNIFEM) Goodwill Ambassador for Thailand`,
  ];

  return (
    <ContainerAfterWorld dividerCaption="Appendices">
      <Row s className="py-1 px-2 scroll1 timeline-responsive">
        <Col xs={12} lg={3} className="grid-divider">
          <h4
            style={{
              lineHeight: 1.4,
              fontFamily:'trajan',
              fontWeight:500
            }}
            className="text-brown"
          >
            TIMELINE OF THE WORK OF HRH PRINCESS BAJRAKITIYABHA
          </h4>
        </Col>
        <Col xs={12} lg={3} className="grid-divider">
          <TextBrown year="2000" bullets={year2000} />
          <TextBrown year="2001" bullets={year2001} />
          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-68-content/image-68-01.webp"
              className="image-page"
            />
          </div>
          <TextBrown year="2002-2004" bullets={year2004} />
        </Col>
        <Col xs={12} lg={3} className="grid-divider">
          <TextBrown year="2005" bullets={year2005} />
          <TextBrown year="2006" bullets={year2006} />
          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-68-content/image-68-02.webp"
              className="image-page"
            />
          </div>
          <TextBrown year="2007" bullets={year2007} />
        </Col>
        <Col xs={12} lg={3} className="grid-divider">
          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-68-content/image-68-03.webp"
              className="image-page"
            />
          </div>
          <TextBrown year="2008" bullets={year2008} />
          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-68-content/image-68-04.webp"
              className="image-page"
            />
          </div>
        </Col>
      </Row>
    </ContainerAfterWorld>
  );
}
