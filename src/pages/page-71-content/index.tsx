import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import "./page-71-content.css";
import { ContainerLight } from "@/components";

const Page71Content = () => {
  return (
    <ContainerLight dividerCaption="Appendices">
      <Row className="gx-2 flex-center">
        <Col lg={12}>
          <h4 className="text-brown">GLOSSARY</h4>
        </Col>
        <Col lg={6} className="mb-1 mb-lg-0">
          <Row className="gx-2">
            <Col lg={4} className="d-flex flex-column justify-content-end">
              <div className="text-content-group">
                <a href="/page-10-content">
                  <h5>ASSOCIATION OF SOUTHEAST ASIAN NATIONS (ASEAN)</h5>
                  <p>
                    A regional intergovernmental organization comprised of 10
                    nations, including Thailand.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="/page-15-content">
                  <h5>BANGKOK RULES</h5>
                  <p>
                    A set of 70 rules tailored towards enhancing the rights and
                    welfare of female offenders. Known officially as the United
                    Nations Rules for the Treatment of Women Prisoners and
                    Non-custodial Measures for Women Offenders, they were
                    drafted by experts in Bangkok in 2009 and adopted by the UN
                    General Assembly in 2010.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="/page-15-content">
                  <h5>
                    COMMISSION ON CRIME PREVENTION AND CRIMINAL JUSTICE (CCPCJ)
                  </h5>
                  <p>
                    The principal policymaking body of the United Nations in the
                    field of crime prevention and criminal justice.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="/page-22-content">
                  <h5>DHAMMA</h5>
                  <p>The teachings of the Buddha.</p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="/page-25-content">
                  <h5>DOMESTIC VIOLENCE PROTECTION ACT</h5>
                  <p>
                    Landmark legislation from 2007 aimed at preventing and
                    punishing family violence in Thailand.
                  </p>
                </a>
              </div>
            </Col>
            <Col lg={4} className="d-flex flex-column justify-content-end">
              <div className="text-content-group">
                <a href="page-71-content">
                  <h5>ENHANCING LIVES OF FEMALE INMATES (ELFI)</h5>
                  <p>
                    An international advocacy campaign, initiated in 2010, that
                    promoted the creation of gender-specific norms and standards
                    for the treatment of women offenders. It led directly to the
                    UN’s adoption of the so-called Bangkok Rules.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="page-71-content">
                  <h5>GENDER MAINSTREAMING</h5>
                  <p>
                    A public policy concept in which the different implications
                    for people of different genders are taken into consideration
                    for any planned policy action, legislation or program.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="/page-33-content">
                  <h5>HAPPY CENTRE</h5>
                  <p>
                    A mental health wellness center found within some Thai
                    prisons.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="page-71-content">
                  <h5>
                    INSTITUTES OF THE UN CRIME PREVENTION AND CRIMINAL JUSTICE
                    PROGRAMME NETWORK (PNI)
                  </h5>
                  <p>
                    A network of interregional and regional institutes, as well
                    as specialized centers, that works to strengthen
                    co-operation on crucial crime prevention and criminal
                    justice issues.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="/page-08-content">
                  <h5>KAMLANGJAI PROJECT</h5>
                  <p>
                    Founded by Princess Bajrakitiyabha in 2006, this
                    partnership-based project initially supplemented Thailand’s
                    Department of Corrections by offering support to female and
                    pregnant inmates, as well as their children. Today, it works
                    within the government to foster reintegration of the wider
                    prison population in society.
                  </p>
                </a>
              </div>
            </Col>
            <Col lg={4} className="d-flex flex-column justify-content-end">
              <div className="text-content-group">
                <a href="/page-66-content">
                  <h5>MANDELA RULES</h5>
                  <p>
                    The name of the revised United Nations Standard Minimum
                    Rules for the Treatment of Prisoners (SMRs), which offer
                    guidance on the management of prison facilities and the
                    humane treatment of prisoners.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="/page-26-content">
                  <h5>MODEL PRISONS</h5>
                  <p>
                    Initiated by the Thailand Institute of Justice, this
                    voluntary program invites female prisons around Thailand to
                    be evaluated using an ‘Index of Implementation’. Policies
                    and practices impacting treatment of women prisoners are
                    assessed in line with the Bangkok Rules.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="/page-35-chapter">
                  <h5>MODEL STRATEGIES</h5>
                  <p>
                    Refers to the United Nations Model Strategies and Practical
                    Measures on the Elimination of Violence against Children,
                    which were ratified in 2014 to address the need for
                    integrated strategies for child violence prevention and
                    protection.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="/page-11-content">
                  <h5>NON-CUSTODIAL MEASURES</h5>
                  <p>
                    An alternative to a prison sentence. For example, the
                    convicted person might be fined or complete community
                    service instead.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="/page-29-content">
                  <h5>
                    OFFICE FOR THE BANGKOK RULES AND TREATMENT OF OFFENDERS
                    (OBR)
                  </h5>
                  <p>
                    A department at the Thailand Institute of Justice that
                    conducts capacitybuilding training and research in pursuit
                    of Bangkok Rules implementation in prisons across Thailand
                    and overseas.
                  </p>
                </a>
              </div>
            </Col>
          </Row>
        </Col>
        <Col lg={6} className="mb-2 mb-lg-0">
          <Row className="">
            <Col lg={4} className="d-flex flex-column justify-content-end">
              <div className="text-content-group">
                <a href="/page-11-content">
                  <h5>RESTORATIVE JUSTICE</h5>
                  <p>
                    A response to criminal behavior in which victims, offenders
                    and the community are brought together in an attempt to
                    restore harmony and make amends.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="/page-11-content">
                  <h5>ROYAL PARDONS</h5>
                  <p>
                    A Thai tradition whereby prisoners are pardoned by His
                    Majesty the King, either in the form of an unconditional
                    release, a commutation or a reduction of punishment.
                    Dating back to the Sukhothai and Ayuthaya periods, royal
                    pardons were formally established as royal prerogative in
                    1932, when Thailand’s first Constitution was created. Two
                    types are practiced today. Covering select groups of
                    prisoners who fulfil certain criteria, collective royal
                    pardons are typically granted on auspicious occasions on the
                    recommendation of the government. Individual royal pardons,
                    applied for by submitting a petition through formal
                    channels, are subject to His Majesty’s discretion based on
                    recommendations by the Ministry of Justice.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="/page-08-content">
                  <h5>RULE OF LAW</h5>
                  <p>
                    A principle of governance in which all persons are
                    accountable to laws, equally enforced and independently
                    adjudicated, and in which the law is consistent with
                    international human rights norms and standards.
                  </p>
                </a>
              </div>
            </Col>
            <Col lg={4} className="d-flex flex-column justify-content-end">
              <div className="text-content-group">
                <a href="/page-08-content">
                  <h5>THAILAND INSTITUTE OF JUSTICE (TIJ)</h5>
                  <p>
                    A Bangkok-based research institute affiliated with the
                    United Nations Crime Prevention and Criminal Justice
                    Programme Network, or UN-PNI and focused on implementing the
                    Bangkok Rules, reforming the criminal justice system, and
                    promoting the rule of law.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
              <h5>UN CONVENTION ON THE RIGHTS OF THE CHILD (CRC)</h5>
                  <p>
                    A ratified UN treaty that enshrines the rights of the child
                    in terms of healthcare, education and social development and
                    also calls for their protection against all forms of
                    violence and exploitation.
                  </p>
              </div>
              <div className="text-content-group">
                <a href="page-71-content">
                  <h5>UN DEVELOPMENT FUND FOR WOMEN (UNIFEM)</h5>
                  <p>
                    A UN body that provides financial and technical assistance
                    to innovative programs and strategies to foster women’s
                    empowerment and gender equality.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="page-71-content">
                  <h5>UN OFFICE ON DRUGS AND CRIME (UNODC)</h5>
                  <p>
                    The UN body focused on making the world safer from drugs,
                    organized crime, corruption and terrorism.
                  </p>
                </a>
              </div>
            </Col>
            <Col lg={4} className="d-flex flex-column justify-content-end">
              <div className="text-content-group">
                <a href="page-71-content">
                  <h5>
                    UN STANDARD MINIMUM RULES FOR NON-CUSTODIAL MEASURES (THE
                    TOKYO RULES)
                  </h5>
                  <p>
                    A set of basic, soft law principles promoting alternatives
                    to imprisonment and greater community involvement in the
                    treatment of offenders.
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="page-71-content">
                  <h5>
                    UN STANDARD MINIMUM RULES FOR TREATMENT OF PRISONERS (SMRS)
                  </h5>
                  <p>
                    The UN-ratified minimum standards for the management of
                    prison facilities and the humane treatment of prisoners.
                    They were originally adopted in 1955, and revised, in
                    recognition of advances in international law and
                    correctional science, in 2015 (See the Mandela Rules).
                  </p>
                </a>
              </div>
              <div className="text-content-group">
                <a href="/page-10-content">
                  <h5>YABA</h5>
                  <p>
                    A combination of methamphetamine (a powerful and addictive
                    stimulant) and caffeine. Yaba, which means crazy medicine in
                    the Thai language, is produced by transnational crime
                    syndicates and widely abused across Thailand and Southeast
                    Asia.
                  </p>
                </a>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerLight>
  );
};

export default Page71Content;
