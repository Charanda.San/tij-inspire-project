import React from "react";
import { Chapter } from "src/components";
import { IPropsChapter } from "@/components/Chapter";
export default function Page49Chapter() {
  const data: IPropsChapter = {
    src: "./images/page-49-chapter/image-chapter.webp",
    title: "THE RULE OF LAW AND SUSTAINABLE DEVELOPMENT",
    chapterText: "CHAPTER 3",
    content:
      "The rule of law is the thread that binds Princess Bajrakitiyabha’s work together. In her view, strong institutions and a fair and equitable justice system are integral to a free and fully functioning society. Drawing inspiration from the work of her late grandfather, His Majesty King Bhumibol Adulyadej, she has sought to expand this notion in various contexts. Most no tably, she was, alongside the Thailand Institute of Justice, a key proponent of the campaign for the rule of law to be hardwired into the United Nations’ post-2015 international development agenda. Meanwhile, outside of the diplomatic arena, she has sought to put the concept of the rule of law into practice, namely through the promotion of new development-led approaches to crime prevention in Thailand.",
    descriptionImg: (
      <p style={{maxWidth:'230px'}}>
        An inmate collects vegetables at Khao Prik Agriculture Industrial
        Institution in Nakhon Ratchasima, Northeast Thailand.
      </p>
    ),
  };
  return <Chapter {...data} />;
}
