import { CaptionImagePosition } from "@/components";
import { ICaptionPosition } from "@/components/CaptionImagePosition";
import { ContentImage, WrapperContent } from "@/components/Widget";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const Page31Content = () => {
  const caption1 = `Indonesia’s Tangerang Women’s Prison welcomes Her Royal Highness and UNODC regional representative Jeremy Douglas in 2018`;
  const caption2 = `Dialogue as well as teaching is an important part of the program.`;
  const caption3 = `Bangkok Rules Training at TIJ, 2018.`;
  const caption4 = `Women’s corrections
expert Dr. Barbara Owen leads a training module.`;
  const caption5 = `Copies of the Bangkok Rules in different languages.`;

  const captionsDesktop: ICaptionPosition[] = [
    {
      position: "Opposite page top: ",
      caption: caption1,
    },
    {
      position: "Opposite page bottom: ",
      caption: caption2,
    },
    {
      position: "Top left: ",
      caption: caption3,
    },
    {
      position: "Top right: ",
      caption: caption4,
    },
    {
      position: "Bottom left: ",
      caption: caption5,
    },
  ];
  const captionMobile: ICaptionPosition[] = [
    {
      position: (
        <>
          1 <sup>st</sup>:&nbsp;&nbsp;
        </>
      ),
      caption: caption2,
    },
    {
      position: (
        <>
          2 <sup>nd</sup>:&nbsp;&nbsp;
        </>
      ),
      caption: caption1,
    },
    {
      position: (
        <>
          3 <sup>rd</sup>:&nbsp;&nbsp;
        </>
      ),
      caption: caption3,
    },
    {
      position: (
        <>
          4 <sup>th</sup>:&nbsp;&nbsp;
        </>
      ),
      caption: caption5,
    },
    {
      position: (
        <>
          5 <sup>th</sup>:&nbsp;&nbsp;
        </>
      ),
      caption: caption4,
    },
  ];
  return (
    <div
      style={{ backgroundColor: "#B49479", top: "7px" }}
      className="h-100 position-relative p-0"
    >
      <Row className="px-0 gy-2 gx-2  h-100 overflow-auto">
        <Col lg={6} className="scroll1">
          <ContentImage
            title="THE BANGKOK RULES TRAINING PROGRAMME"
            // img="./images/page-21-content/page-21-1.webp"
            bgColor="transparent"
            border={false}
            paragraph={[
              <div className="text-param">
                <p>
                  The leap from prison management theory to practice that the
                  Bangkok Rules represent is tackled head-on each year in the
                  Thailand Institute of Justice’s training course. Introduced in
                  2016, the two — week Programme equips groups of around 30 senior
                  correctional staff from across Southeast Asia with the skills
                  and knowledge necessary to enact the rules effectively within
                  their own context.
                </p>
                <p>
                  Interspersed with prison study visits, the course’s twelve
                  modules offer practical guidance on the context, rationale and
                  application of each of the 70 rules. For example, Module 3
                  (Hygiene and Healthcare) goes far beyond merely detailing
                  applicable rules and processes by offering an in-depth
                  discussion of all the relevant themes: the whole prison
                  approach to healthcare, the principles and components of
                  health screenings on admission, the nature of gender-specific
                  healthcare services, the need for medical confidentiality, and
                  the hygiene requirements of women.
                </p>
                <div
                  style={{ width: "80%", color: "#FFFFFF" }}
                  className="d-block mx-auto px-0 px-lg-3"
                >
                  <p
                    className="text-center"
                    style={{ fontSize: "17px", fontFamily: "Neue Haas Unica" }}
                  >
                    “The Bangkok Rules help people reimagine prison.”
                  </p>
                  <div className="d-block ms-auto" style={{ width: "60%" }}>
                    <p
                      className="text-center"
                      style={{
                        fontSize: "12px",
                        fontFamily: "Neue Haas Unica",
                        fontStyle: "italic",
                      }}
                    >
                      –Dr. Barbara Owen
                    </p>
                  </div>
                </div>

                <p>
                  With each new iteration of the modules, new statistics or best
                  practices from ASEAN members have been added. Because the gap
                  in practices varies widely between different parts of the
                  world, trainers have found that practitioners from the region
                  relate better to local examples than those from further
                  afield.
                </p>
                <p>
                  Also embedded in the training is a pragmatism that takes into
                  account common challenges in the region, such as acute
                  overcrowding and shortages of resources. Although couched in
                  human rights terminology, the Bangkok Rules were created with
                  the realities of operational practice in mind. Consequently,
                  prison staff who understand the spirit and objectives of the
                  rules are encouraged to prioritize the measures they can
                  implement in their circumstances and be creative. This could
                  be done by, for
                </p>
                <img
                  src="./images/page-31-content/page-31.webp"
                  alt=""
                  className="mb-3 mb-lg-0"
                  style={{ height: "auto", objectFit: "inherit" }}
                />
              </div>,
              <div>
                <div className="mb-3">
                  <img
                    src="./images/page-31-content/page-31-1.webp"
                    alt=""
                    className="mb-3 mb-lg-0"
                    style={{ height: "auto", objectFit: "inherit" }}
                  />
                </div>
                <p>
                  example, using certain spaces within the prison for multiple
                  purposes or trying to forge new collaborations with the
                  private sector. “One of the things I explain during the
                  training is reality and reach,” says Dr. Barbara Owen, an
                  American sociologist who specializes in female prison culture
                  and helped draft the Bangkok Rules. “We have to deal with
                  reality, but we have to reach at the same time.”
                </p>
                <p>
                  To get them thinking out of the box, each participant is
                  encouraged to draft and share an Action Plan towards the end
                  of their training. The goal they each come up with could be as
                  simple as making sure there are more menstrual hygiene
                  products available, or as complex as redesigning a whole
                  health-care system or creating a women’s services unit.
                </p>
                <p>
                  Regionally, the TIJ’s Implementation of the Bangkok Rules and
                  Treatment of Offenders (IBR) Programme has faced hurdles.
                  According to Dr. Nathee Chitsawang, TIJ acting Deputy
                  Executive Director and current advisor, the dichotomy between
                  the operational promise of the Bangkok Rules and the
                  operational reality within Thailand has been pointed out by
                  ASEAN members at times. “They said: ‘You’re telling us to do
                  this and that, but what about female prisoners in your own
                  country? Do you treat them in accordance with the Bangkok
                  Rules?’”
                </p>
                <p>
                  However, successive years of training have led to trust being
                  gained, relationships forged and initiatives launched. In
                  April 2018, Indonesia’s Tangerang Women’s Prison welcomed
                  Princess Bajrakitiyabha and a TIJ delegation, for example,
                  while the TIJ recently signed a memorandum of understanding to
                  adapt Thailand’s Model Prison concept for the Cambodian
                  context, beginning with the country’s largest women’s prison.
                  With, as of mid-2020, a total of 38 senior Thai correctional
                  staff and 77 from ASEAN member countries and further afield
                  having completed the training, the hope is that these sorts of
                  cross-border partnerships will only grow over time. “Each one
                  is a seed that we’ve planted in the prison system,” says Dr.
                  Barbara Owen.
                </p>
              </div>,
            ]}
          />
        </Col>
        <Col lg={6} className="scroll1 overflow-x-hidden">
          <Row className="gx-1 gy-0">
            <Col lg={6} xs={12}>
              <Row className="gy-3 gx-2 px-3 px-lg-0">
                <Col lg={12} sm={6} xs={12}>
                  <img
                    src="./images/page-31-content/image-31-03.webp"
                    alt=""
                    className="image-page"
                  />
                </Col>
                <Col lg={12} sm={6} xs={12}>
                  <img
                    src="./images/page-31-content/image-31-04.webp"
                    alt=""
                    className="image-page"
                  />
                </Col>
              </Row>
            </Col>
            <Col lg={6} xs={12} className="px-3" >
              <Row className="gy-3 gx-2">
                <Col lg={12} sm={12}>
                  <img
                    src="./images/page-31-content/image-31-05.webp"
                    alt=""
                    className="image-page"
                  />
                </Col>
                <Col lg={12} sm={6} className="pb-3">
                  <div className="d-none d-lg-block d-sm-none">
                    <CaptionImagePosition
                      style="monster"
                      captions={captionsDesktop}
                      position="top"
                    />
                  </div>
                  <div className="d-block d-lg-none d-sm-block">
                    <CaptionImagePosition
                      style="monster"
                      captions={captionMobile}
                      position="top"
                    />
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default Page31Content;
