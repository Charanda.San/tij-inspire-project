import { ContainerOrange } from "@/components";

import React from "react";
import { Col, Row } from "react-bootstrap";
import "./page-58-content.css";

export default function Page58Content() {
 
  const imagesTop = [
    "./images/page-58-content/image-58-02.webp",
    "./images/page-58-content/image-58-03.webp",
  ];

  return (
    <ContainerOrange>
      <Row className=" gy-2 gx-3 pt-lg-4 ">
        <Col xs={12} lg={6} sm={12} >
          <Row className="gx-3 gy-3 mb-2 ">
            <Col xs={12} sm={12} lg={12}>
              <img
                src="/images/page-58-content/image-58-01.webp"
                className="image-page"
              />
            </Col>
            <Col lg={12}>
              <div className="wrapper-image-group-58">
                {imagesTop.map((item) => (
                  <img src={item} className="image-page mb-3" />
                ))}
              </div>
            </Col>
          </Row>
        </Col>
        <Col xs={12} lg={6} sm={12}>
          <Row className="gx-3 gy-2 gy-lg-3">
            <Col xs={12} sm={12} lg={12} className=" mb-2 mb-lg-0">
              <img
                src="/images/page-58-content/image-58-04.webp"
                className="image-page"
              />
            </Col>
            <Col xs={12} lg={9} sm={9}>
              <img
                src="/images/page-58-content/image-58-05.webp"
                className="image-page h-auto"
              />
            </Col>
            <Col xs={12} lg={3} sm={3}>
              <p className="text-caption-image-nue">
                The alternative crop development strategies implemented by the
                Mae Fah Luang Foundation have, in recent years, given way to
                considerations of how the DoiTung brand can refine its
                production processes and supply chains with a view to making
                further inroads into international markets.
              </p>
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerOrange>
  );
}
