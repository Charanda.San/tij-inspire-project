import { CaptionImagePosition, ContainerLight, QuoteImage } from "@/components";
import { ICaptionPosition } from "@/components/CaptionImagePosition";
import React from "react";
import { Col, Row } from "react-bootstrap";
import "./page-51-content.css";

export default function Page51Content() {
  const caption1 = `the rule of law and sustainable
  development at the United
  Nations Conference
  Centre in Bangkok, 2017.`;
  const caption2 = `The King’s Science Exhibition
  in Vienna, 2017. Accompanying Princess Bajrakitiyabha is
  Dr. Kittipong Kittayarak, TIJ Executive Director, and Jeremy Douglas, the UNODC Representative for Southeast Asia and the Pacific.`;
  const caption3 = `Princess Bajrakitiyabha gives
a speech at the Asian Society of International Law Inter-Sessional Conference in Bangkok, 2015.`;
  const caption4 = `TIJ leadership and team members helped push for targets in relation
to the rule of law to be integrated in the United Nations’ Sustainable Development Goals, or SDGs.`;
  const captions: ICaptionPosition[] = [
    {
      position: "Top left: ",
      caption: caption1,
    },
    {
      position: "Bottom left: ",
      caption: caption2,
    },
    {
      position: "Top right: ",
      caption: caption3,
    },
    {
      position: "Bottom right: ",
      caption: caption4,
    },
  ];
  const captionsMobile: ICaptionPosition[] = [
    {
      position: (
        <>
          1 <sup>st</sup>:&nbsp;&nbsp;
        </>
      ),
      caption: caption1,
    },
    {
      position: (
        <>
          2 <sup>nd</sup>:&nbsp;&nbsp;
        </>
      ),
      caption: caption3,
    },
    {
      position: (
        <>
          3 <sup>rd</sup>:&nbsp;&nbsp;
        </>
      ),
      caption: caption2,
    },
    {
      position: (
        <>
          4 <sup>th</sup>:&nbsp;&nbsp;
        </>
      ),
      caption: caption4,
    },
  ];
  const ImageTop = ({ src }: { src: string }) => {
    return <img src={src} alt="" className="image-page" />;
  };
  const imagesTop = [
    "./images/page-51-content/image-51-01.webp",
    "./images/page-51-content/image-51-02.webp",
  ];
  const imagesBottom = [
    "./images/page-51-content/image-51-03.webp",
    "./images/page-51-content/image-51-04.webp",
  ];

  return (
    <ContainerLight dividerCaption="The Rule of Law and Sustainable Development">
      <Row className="p-0">
        <Col xl={6} sm={12} xs={12} className="scroll1">
          <Row className="gx-2 gy-2 ">
            <Col xs={12} sm={12} lg={4} className="mt-4 mt-lg-0">
              <QuoteImage
                fontSub="Neue Haas Unica"
                quote="“Strengthening the rule of law is our common responsibility. Let us all work to fulfill this goal by promoting the rule of law at the national and international levels under the UN umbrella
              to bring about a just world.”"
                subQuote="Her Royal Highness Princess Bajrakitiyabha at the High-Level Meeting on the Rule of Law at the National and International Levels, New York, September 2012
              "
              ></QuoteImage>

              <p className="text-paragraph-nue">
                academics – in the hope that consensus about the role that the
                rule of law, justice and strong institutions should play in the
                post-2015 development agenda could be reached.
              </p>
              <p className="text-paragraph-nue">
                Speaking as Chairperson of the TIJ’s advisory board, Princess
                Bajrakitiyabha carved out her position. “I strongly believe that
                development that excludes the rule of law, crime prevention and
                criminal justice is far from being sustainable,” she stated.
                “How can we be truly sustainable when inequality continues to
                create social tensions? When large segments of the population
                are still barred from accessing justice?”
              </p>
              <p className="text-paragraph-nue d-none d-lg-block">
                Drawing upon her years of experience as a public prosecutor, she
                also pointed out that transnational organized crime, corruption,
                drug trafficking and conflicts are impeding development efforts
                around the world. “Clearly, gaps in the justice system and
                ineffective rule of law are at the heart of the problem,” she
                reflected. And she was not alone in this belief. A similar
                sentiment was expressed in the debates, panel dialogues with
                academics and civil society leaders, and statements given by
                Indonesia’s Minister of
              </p>
              <p className="text-paragraph-nue d-block d-lg-none">
                Drawing upon her years of experience as a public prosecutor, she
                also pointed out that transnational organized crime, corruption,
                drug trafficking and conflicts are impeding development efforts
                around the world. “Clearly, gaps in the justice system and
                ineffective rule of law are at the heart of the problem,” she
                reflected. And she was not alone in this belief. A similar
                sentiment was expressed in the debates, panel dialogues with
                academics and civil society leaders, and statements given by
                Indonesia’s Minister of Foreign Affairs, the Prime Minister of
                Bhutan, and the then UN Secretary-General Ban Ki-moon, among
                others.
              </p>
            </Col>
            <Col xs={12} sm={12} lg={4}>
              <p className="text-paragraph-nue d-none d-lg-block">
                Foreign Affairs, the Prime Minister of Bhutan, and the then UN
                Secretary-General Ban Ki-moon, among others.
              </p>
              <p className="text-paragraph-nue">
                Two years later, the objective that Princess Bajrakitiyabha and
                others had so cogently argued for at the Bangkok Dialogue was
                achieved with the ratification of the United Nations Sustainable
                Development Goals, or SDGs. Unlike the MDGs, the SDGs
                acknowledge that the rule of law is a cornerstone of sustainable
                development, namely in Goal 16, which deals specifically with
                peace, justice and strong institutions. In essence, this goal
                recognizes that the rule of law is cross-cutting and
                instrumental to all nations – “essential for sustained and
                inclusive economic growth, sustainable development, the
                eradication of poverty and hunger and the full realization of
                all human rights and fundamental freedoms,” as the draft UN
                resolution that Her Royal Highness had tabled earlier, back in
                April 2013, stated.
              </p>
              <p className="text-paragraph-nue">
                The creation of SDG 16 was a collaborative effort that spanned
                the United Nations’ 193 Member States and a number of bodies.
                These included the UN-sponsored High-Level Panel of Eminent
                Persons on the Post-2015 Development Agenda, and the Member
                state-led Open Working Group on Sustainable Development Goals,
                to which the key highlights of the Bangkok Dialogue were
                presented. However, when it came to individual actors, Princess
                Bajrakitiyabha had emerged during the process as one of Asia’s
                most vocal and committed champions for the mainstreaming of the
                rule of law within the context of sustainable development. She
                had also helped “build political momentum for the post-2015
                development agenda”, as the United Nations Office on Drugs and
                Crime (UNODC) later stated.
              </p>
              <p className="text-paragraph-nue d-none d-lg-block">
                In recognition of her sustained and inspiring campaigning, the
                UNODC invited Princess Bajrakitiyabha to be its Goodwill
                Ambassador for the Rule of Law in Southeast Asia in February
                2017. Given the breadth of her criminal justice experience and
                intrepid work at both the diplomatic and community levels, not
                to mention her roles as a public prosecutor and at the TIJ, the
                appointment was a natural fit. Since taking up the role, she has
                worked
              </p>
            </Col>
            <Col xs={12} sm={12} lg={4}>
              <p className="text-paragraph-nue d-block d-lg-none">
                In recognition of her sustained and inspiring campaigning, the
                UNODC invited Princess Bajrakitiyabha to be its Goodwill
                Ambassador for the Rule of Law in Southeast Asia in February
                2017. Given the breadth of her criminal justice experience and
                intrepid work at both the diplomatic and community levels, not
                to mention her roles as a public prosecutor and at the TIJ, the
                appointment was a natural fit. Since taking up the role, she has
                worked hard to raise the visibility of, and support for,
                development efforts that address the impact of crime on society
                and contribute to justice reform. In this capacity, she has
                spoken at narcotics commissions, regional conferences, public
                forums, and General Assembly debates on transnational organized
                crime, among other high-level events. These speeches have tended
                to stress the need for firm and decisive action in lieu of
                exhaustive definitions of the rule of law, or universal
                agreement on its constituent, measurable components. As she said
                in 2016: “Agreeing on the importance of the rule of law in
                support of sustainable development is one thing, but taking the
                actual concrete steps to translate such goals and aspirations
                into reality for the people is quite another.”
              </p>
              <p className="text-paragraph-nue d-none d-lg-block">
                hard to raise the visibility of, and support for, development
                efforts that address the impact of crime on society and
                contribute to justice reform. In this capacity, she has spoken
                at narcotics commissions, regional conferences, public forums,
                and General Assembly debates on transnational organized crime,
                among other high-level events. These speeches have tended to
                stress the need for firm and decisive action in lieu of
                exhaustive definitions of the rule of law, or universal
                agreement on its constituent, measurable components. As she said
                in 2016: “Agreeing on the importance of the rule of law in
                support of sustainable development is one thing, but taking the
                actual concrete steps to translate such goals and aspirations
                into reality for the people is quite another.”
              </p>
              <p className="text-paragraph-nue">
                Alongside the TIJ, the UNODC and other organizations, she has
                also sought to make the rule of law more directly relatable and
                relevant to people’s lives. In addition to her ongoing drive to
                enhance the rights and future prospects of women and children in
                contact with the law, these have included the promotion of legal
                aid and community-based drug-control interventions. Through
                study visits on empowering vulnerable communities and women, and
                by facilitating workshops between prisoners and judges, she has
                also attempted to create more engagement between government
                officials and the public.
              </p>
              <p className="text-paragraph-nue">
                According to Jeremy Douglas, the UNODC’s Regional Representative
                Southeast Asia and the Pacific, Princess Bajrakitiyabha’s
                wide-ranging perspective and unique vantage point have made her
                an important catalyst for SDG 16 progress and awareness about
                the rule of law reform in the region. “She has a special place
                in Southeast Asian society,” he says. “And it’s really important
                to have a Southeast Asian speaking to Southeast Asians.” When it
                comes to confronting entrenched interests and advocating at the
                highest levels for crime and justice-related UN norms, goals and
                agendas, her multidisciplinary experience and steadily accrued
                diplomatic capital are a rare asset. “Without her, we simply
                can’t do it at that level,” he says. “She opens the door. She
                says the right words. She pushes policy forwards.”
              </p>
            </Col>
          </Row>
        </Col>
        <Col xl={6} sm={12} xs={12} className="scroll1">
          <Row className="gx-3 gy-2">
            {imagesTop.map((image: string) => (
              <Col lg={6} xs={12} sm={6}>
                <ImageTop src={image} />
              </Col>
            ))}
          </Row>
          <Row className="mt-1 gx-2 gy-2">
            <Col lg={9} xs={12} sm={7} className="hidden-xs">
              <img
                style={{ height: "auto" }}
                src="./images/page-51-content/image-page-51-fix.webp"
              />
            </Col>

            {imagesBottom.map((image: string) => (
              <Col xs={12} className="show-xs">
                <ImageTop src={image} />
              </Col>
            ))}

            <Col lg={3} xs={12} sm={5} className="position-relative">
              <div className="d-none d-sm-block d-lg-block">
                <CaptionImagePosition
                  style="monster"
                  captions={captions}
                  position="top"
                />
              </div>
              <div className="d-block d-sm-none d-lg-none">
                <CaptionImagePosition
                  style="monster"
                  captions={captionsMobile}
                  position="top"
                />
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerLight>
  );
}
