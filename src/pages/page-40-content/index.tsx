import { ContainerOrange } from "@/components";
import { Inpractice } from "@/components/Widget";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const Page40Content = () => {
  const textLgSection1 = `The boys also have the option to write diaries, which are then
  shared with their advisor, who reads it and writes a response
  before returning it. Initially there was some resistance to
  this form of therapeutic dialogue, but today everyone – both
  the boys and the staff – value the process. “I love it when I
  write down my feelings and someone comes to answer,” explains
  Book. “Sometimes I write down my problems on Facebook and`
  const textLgSection2 = `no one answers, but here the teacher gives me energy. Also,
  sometimes I like to look back at the first page I wrote –
  right away I can see the progress I’ve made.”`
  return (
    <ContainerOrange>
      <Row className="h-100">
        <Col lg={5}>
          <Row className="gx-2 gy-2">
            <Col lg={2} className="px-2">
              <p className="text-description-img">
                The atmosphere at Ban Kanchanapisek is open and flexible,
                encouraging the resident boys to take ownership of their past
                actions and contemplate their future potential as citizens.
              </p>
            </Col>
            <Col lg={10} className="pb-3 px-2">
              <img src="./images/page-40-content/page-40.webp" alt="" />
            </Col>

            <Col lg={6} className="px-2 pb-3 pb-lg-0">
              <img src="./images/page-40-content/page-40-1.webp" alt="" />
            </Col>
            <Col lg={6} className="px-2 pb-3 pb-lg-0">
              <img src="./images/page-40-content/page-40-2.webp" alt="" />
            </Col>
          </Row>
        </Col>
        <Col lg={7} className="h-100 scroll1">
          <Inpractice
            title="BAN KANCHANAPISEK, A PLACE OF EMPOWERMENT"
            paragraph={[
              <>
                <p>
                  <span className="first-letter-orange">O</span>n day one at Ban
                  Kanchanapisek, a leafy juvenile detention center in Thailand’s
                  Nakhon Pathom province, something unusual happens. Shortly
                  after the handcuffs of new arrivals have been removed, the
                  facility’s long-serving director, Ticha Na Nakorn, welcomes
                  them with a big hug. After that disarming gesture, she then
                  leads a blessing ritual during which the staff and other
                  children tie pieces of white string around their wrists.
                  “While tying the string,” this kindly matriarchal figure
                  explains, “I tell them: ‘Here we don’t believe you willingly
                  killed someone. If there’s a miracle that could bring you back
                  to that time, we believe you wouldn’t have done it. But there
                  are no miracles. Someone really died, you’ve been arrested and
                  brought here, the family of the victim is suffering, your
                  parents are suffering, and everything has happened. But we
                  believe that within you there is someone else in there that is
                  a weak person but a good person’.”
                </p>
                <p>
                  Life at Ban Kanchanapisek, which is home to around 100 wayward
                  boys guilty of serious crimes, including murder, starts as it
                  means to go on: compassionately. From the unorthodox welcome
                  to the no-uniforms policy, from the lack of high,
                  penitentiary-style walls to the use of restorative justice
                  to settle disputes, it is really a detention facility in name
                  only. Here the only real restrictions are internal – the
                  barrier that each boy comes to feel, and which stops him from
                  running away or returning to his miscreant ways. And the
                  statistics prove that this approach works. Years of successful
                  rehabilitation show that those boys who do arrive at Ban
                  Kanchanapisek with designs on escaping or committing crime
                  again change their minds. “We stop them at the very beginning,
                  not by prohibiting them, but by making them think,” she says.
                </p>
                <p>
                  Around a quarter of the boys’ schedule consists of regular
                  school subjects, and another quarter vocational training, but
                  the majority is devoted to life skills training. “What they
                  really need is life lessons,” argues Ticha. “We believe that
                  once their mindsets change, their behaviors change, they can
                  re-enter society and know how to act.”
                </p>
                <p>
                  Activities that get them to rethink the world and their place
                  in it include news analysis. Lining the walls in the open-air
                  corridors are the boys’ handwritten thoughts and opinions on
                  news stories recently in the headlines. These case-studies
                  don’t just span crime, but also civil society and even
                  celebrity culture. Once a week, a film handpicked for its
                  moral message by Ticha, or Pa Mon (Aunty Mon), as she is known
                  to the boys, is also screened. Afterwards, this, too, is
                  analyzed, both individually and as a group.
                </p>
                <p className="d-lg-block d-none d-sm-none">
                 {textLgSection1}
                </p>
                <p className="d-lg-none d-block d-sm-block">
                 {`${textLgSection1} ${textLgSection2}`}
                </p>
              </>,
              <>
                <p className="d-lg-block d-none d-sm-none">
                 {textLgSection2}
                </p>
                <p>
                  During home visits – boys are allowed home on the last weekend
                  of each month – parents are also encouraged to read and write
                  their own responses in these diaries. The results of them
                  doing this have surpassed expectations. “Before their crime
                  happened, there is usually a gap between the boys and their
                  families,” explains Ticha. “The diary helps fill that gap and
                  makes the parents understand their kids more.”
                </p>
                <p>
                  Parents are key to the rehabilitation process. “We work hard
                  with their parents, as they are the stakeholders, to change
                  their fixed mindsets into growth mindsets,” Ticha explains.
                  “Each family has to help their child change attitudes, change
                  behaviors.”
                </p>
                <p>
                  At the outset, parents are asked to come in for an orientation
                  during which plans are created and the advisor assigned to
                  their child introduced. Advisors talk regularly with them
                  about how their children are doing, typically via the LINE
                  social media app. “This is the foundation – parents have to
                  know about all the activities they do.”
                </p>
                <p>
                  Family empowerment workshops are also held six times a year;
                  and every two months, there is a day when staff leave the
                  grounds of Ban Kanchanapisek entirely to the whims of the
                  children and their parents. According to Ticha, this activity
                  helps the children “see they are valued, won’t be left
                  behind.” Book, 19, has personally felt the benefits. “At home
                  me and my parents didn’t spend much time together. Being here
                  is the first time in my life when we can all just sit down,
                  eating and talking to each other.”
                </p>
                <p>
                  On a system-wide level, it has, so far, proven difficult to
                  distill the working practices here and disseminate them to
                  other juvenile detention centers. “This,” Ticha explains, “is
                  because Ban Kanchanapisek works with kids on a horizontal
                  level. That means that the power relations that comes with
                  vertical hierarchies is gone; we don’t have a vertical
                  approach like the government.” And yet, it is her hope that
                  this will change if and when a ministerial rule allowing the
                  non-profit sector to run detention centers becomes law –
                  something she and former residents have requested from the
                  Ministry of Justice
                </p>
                <p>
                  Still today, sixteen years after it opened, there are those
                  who question whether the freedom, agency, forgiveness and love
                  given to the boys at Ban Kanchanapisek is – despite the many
                  success stories – punishment enough for the crimes they have
                  committed, who argue that ‘an eye for an eye’ is preferable.
                  “But we don’t believe that,” she says. “We want to bring out
                  their good side.” For her, every facet of life here boils down
                  to a simple truism: “The children who come to us were not born
                  to be criminals.”
                </p>
              </>,
            ]}
          />
        </Col>
      </Row>
    </ContainerOrange>
  );
};

export default Page40Content;
