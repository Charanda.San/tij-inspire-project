import React from "react";
import { ContentGreen, ImageBackground, QuoteImage } from "@/components";
import { IPropsContent } from "@/components/ContentGreen";
import CaptionImagePosition, {
  ICaptionPosition,
} from "@/components/CaptionImagePosition";
import { Col, Row } from "react-bootstrap";

export default function Page50Content() {
  const captions: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: `   Princess Bajrakitiyabha and Jeremy Douglas, UNODC Representative for Southeast Asia and the Pacific, pose with community members during a UNODC study visit to Doi Tung, Northern Thailand, 2018.
    `,
    },
    {
      position: "Bottom: ",
      caption: ` Hill-tribe people in remote rural Myanmar.
    `,
    },
  ];
  const propsContent: IPropsContent = {
    title: (
      <div>
        Promoting the Rule <br /> of Law
      </div>
    ),
    character: "h",
    textDividerLeft: "inspire",
    textDividerRight: "The Rule of Law and Sustainable Development",
    subTitle: `er Royal Highness Princess Bajrakitiyabha’s criminal justice and human rights reform work is underpinned and informed by a passionate belief in the paramount importance of the rule of law –
    and the need for a fair and accessible justice system that upholds it. For her, the rule of law is not something abstract or remote from everyday life, but something real, tangible and, above all, essential to a country’s sustainable socioeconomic progress and development. As she explained on a study visit to a prison back in 2018: “What I have sought to explain is that the rule of law is much more than court cases and lawyers. The rule of law is needed to make our educational system, our working life, our housing and our healthcare institutions, and of course, our justice system more effective, accountable, and inclusive.”`,
    paragraphs: [
      <>
        <p>
          This conviction has been directly informed by her work as a public
          prosecutor, a position she has held in provinces across Thailand,
          regularly placing her in contact with citizens whose problems have
          been aggravated – and sometimes even caused – by flaws within the
          justice system. Through such experiences she has come to see
          first-hand a concrete linkage between sustainable development, the
          rule of law and crime prevention.
        </p>
        <p className="d-none d-lg-block">
          Princess Bajrakitiyabha’s late grandfather, King Bhumibol Adulyadej,
          who studied law himself in Switzerland as a young man, has also acted
          as a role model. During his long reign (1946–2016), King Bhumibol
        </p>
        <p className="d-block d-lg-none">
          Princess Bajrakitiyabha’s late grandfather, King Bhumibol Adulyadej,
          who studied law himself in Switzerland as a young man, has also acted
          as a role model. During his long reign (1946–2016), King Bhumibol
          Adulyadej frequently instructed top officials on the necessity of
          upholding the law and combatting corruption, while also reflecting
          upon cases in which he thought the law had wronged – rather than
          served – the people. King Bhumibol also inspired more than 4,300 Royal
          Development projects across Thailand, encompassing water resource
          management, agriculture and public health. These initiatives often
          promoted the empowerment of the marginalized and disenfranchised, as
          well as people’s agency over their own lives. By doing so, they
          tacitly recognized the potential for the rule of law and a stronger
          civil society to create a virtuous circle – its ability to facilitate
          socioeconomic growth and the realization of certain human rights,
          which then further strengthen the rule of law.
        </p>
      </>,
      <p className="d-none d-lg-block">
        Adulyadej frequently instructed top officials on the necessity of
        upholding the law and combatting corruption, while also reflecting upon
        cases in which he thought the law had wronged – rather than served – the
        people. King Bhumibol also inspired more than 4,300 Royal Development
        projects across Thailand, encompassing water resource management,
        agriculture and public health. These initiatives often promoted the
        empowerment of the marginalized and disenfranchised, as well as people’s
        agency over their own lives. By doing so, they tacitly recognized the
        potential for the rule of law and a stronger civil society to create a
        virtuous circle – its ability to facilitate socioeconomic growth and the
        realization
      </p>,
      <>
        <p className="d-none d-lg-block">
          of certain human rights, which then further strengthen the rule of
          law.
        </p>
        <p>
          It was in the early 2010s that the mutually reinforcing relationship
          between the rule of law and sustainable development first became a
          central and recurring theme in Princess Bajrakitiyabha’s international
          advocacy work. Whilst serving as the Ambassador and Permanent
          Representative of Thailand to the United Nations in Vienna
          (2012-2014), she had come to the realization that an essential
          shortcoming of the Millennium Development Goals (MDGs) – the United
          Nations’ eight-goal blueprint for meeting the needs of the world’s
          poorest by 2015 – was the absence of any
        </p>
      </>,
    ],
    componentRightSide: (
      <>
        <Row className="gx-2 gy-2 gy-lg-2 gy-sm-3 ">
          <Col xs={12} sm={8} lg={8}>
            <img
              alt=""
              className="image-page"
              src="./images/page-50-content/image-50-01.webp"
            />
          </Col>
          <Col xs={12} sm={4} lg={4}>
            <QuoteImage
              fontSub="Neue Haas Unica"
              subQuote={
                <>
                  Her Royal Highness Princess Bajrakitiyabha at the 5
                  <sup>th</sup> Biennial Conference of the Asian Society of
                  International Law, November 2015
                </>
              }
              quote={`“My work in the Office of Public Prosecution has brought me
to experience on countless occasions the hard life of rural countrymen and women whose hardship has been exacerbated day by day
by the lack of access to justice, misallocation of natural resources and human rights abuses.”`}
            />
          </Col>
        </Row>
        <Row className="gx-2 gy-2 gy-lg-2 gy-sm-3 mt-sm-3 ">
          <Col xs={12} sm={12} lg={2} className="position-relative">
            <CaptionImagePosition
              style="monster"
              captions={captions}
              position="top"
            />
          </Col>
          <Col xs={12} sm={12} lg={4} className="h-50">
            <img
              alt=""
              className="image-page"
              src="./images/page-50-content/image-50-02.webp"
            />
          </Col>
          <Col xs={12} sm={6} lg={3} className="text-paragraph-nue p-0 p-md-1 mb-0">
            <p className="text-paragraph-nue">
              mention of the rule of law. So she began to lobby for this to be
              addressed in the post-2015 development framework that was being
              formulated.
            </p>
            <p className="text-paragraph-nue">
              For example, at a debate on drugs and crime at the 66<sup>th</sup>
              Session of the United Nations General Assembly, she gave her frank
              thoughts, in her capacity as the Chairperson of the 21
              <sup>st</sup> Session of the United Nations Commission on Crime
              Prevention and Criminal Justice (CCPCJ), on the mixed success of
              the MDGs. For her, it was not down to the “insufficiency of
              resources invested in achieving them, but rather due to the
              prevalence of crime and corruption in many countries that have
              undermined the rule of law and weakened the economy, hampering
              progress in this respect.”
            </p>
          </Col>
          <Col xs={12} sm={6} lg={3} className="text-paragraph-nue p-0 p-md-1 mb-0">
            <p>
              Later, at the 22<sup>nd</sup> session of the CCPCJ in April 2013,
              a Thai delegation led by Her Royal Highness presented a draft
              resolution that called for the post-2015 development agenda to “be
              guided by respect for and promotion of the rule of law.”
            </p>
            <p>
              Building upon these sorts of multilateral signals and gestures,
              the Thai government and recently formed Thailand Institute of
              Justice (TIJ) also hosted an important criminal justice themed
              discourse around the impending post-2015 development agenda. Held
              at Bangkok’s Siam Kempinski Hotel in November 2013, the Bangkok
              Dialogue on the Rule of Law conference presented diverse
              high-level perspectives – those of world leaders, United Nations
              officials, civil society actors and
            </p>
          </Col>
        </Row>
      </>
    ),
  };

  return (
    <>
      <ContentGreen {...propsContent}></ContentGreen>
    </>
  );
}
