import {
  ContainerLightOnly,
  ImageParagraphPage,
} from "@/components";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { ContentRules } from "src/components/Widget";

const Page30Content = () => {
  return (
    <>
      <Row className="h-100">
        <Col
          lg={6}
          // style={{ paddingTop: "80px" }}
          className="scroll2 h-100 p-0"
        >
          <ContainerLightOnly>
            <ImageParagraphPage
              positionCaption="right"
              caption="Princess Bajrakitiyabha talks about Bangkok Rules implementation with female wardens
              at Tangerang Women’s Prison on the outskirts of Jakarta, April 2018."
              image="./images/page-30-content/page-30.webp"
              paragraphs={[
                <>
                  <p>
                    deepening research, all in the hope of making each prison
                    within its purview a beacon of humane, gender-sensitive
                    penal practice. Her Royal Highness, meanwhile, maintains an
                    important role. In addition to working on the Kamlangjai
                    Project and maintaining a thorough grasp of the details of
                    individual prisons, which she visits regularly, her
                    long-term vision matches that of the TIJ.
                  </p>
                  <p>
                    Simply speaking, there are three dimensions to the issue of
                    prison and offenders – Inside Prison, Beyond Prison and
                    Besides Prison – that the Bangkok Rules encompass and the
                    TIJ is earnestly working to address. Inside prisons, the
                    Bangkok Rules encourage a gender-sensitive management
                    approach and a focus on preparing prisoners for life
                  </p>
                </>,
                <>
                  <p>
                    after release. They also deal with life beyond prisons by
                    promoting offenders’ reintegration into their communities
                    and calling for support to help them develop the personal
                    strength and skills needed to face a new life. Lastly, they
                    promote alternatives to prison such as non-custodial
                    measures.
                  </p>
                  <p>
                    In recent years, Her Royal Highness has consistently argued
                    for sustained effort on all these fronts – and, at the same
                    time, strategically placed them within a broader
                    constellation of ideas that revolve around one concept: rule
                    of law. “It is important to bring offenders to justice,” she
                    remarked in a video message for a TIJ public forum in July
                    2017, “but we should ensure that the justice system itself
                    follows the rule of law, by full
                  </p>
                </>,
                <>
                  <p>
                    application of such principles as gender sensitivity, and by
                    finding an outcome that best ensures reintegration of the
                    offender into society. In the longer term, we should tackle
                    the factors that may lead to crime.”
                  </p>
                  <p>
                    These are the precise parameters, the driving forces of the
                    TIJ as it strives, through its criminal justice and rule of
                    law reform work with regional partners and its unique status
                    as ASEAN’s only Programme Network Institute (PNI), to
                    protect vulnerable groups, to advocate for more equitable,
                    rights-oriented societies, and to extend the rights of women
                    by all means at its disposal, including the Bangkok Rules.
                  </p>
                </>,
              ]}
            ></ImageParagraphPage>
          </ContainerLightOnly>
        </Col>

        <Col
          lg={6}
          className="py-3 scroll2 h-100"
          style={{ backgroundColor: "#B49479" }}
        >
          <ContentRules
            title="EXAMPLES OF THE BANGKOK RULES"
            content={[
              {
                rule: 5,
                text: "The accommodation of women prisoners shall have facilities and materials required to meet women’s specific hygiene needs",
              },
              {
                rule: 10,
                text: "Gender-specific health-care services at least equivalent to those available in the community shall be provided to women prisoners",
              },
              {
                rule: 12,
                text: "Individualized, gender-sensitive, trauma-informed and comprehensive mental health care and rehabilitation programs shall be made available for women prisoners with mental health-care needs in prison or in non-custodial settings",
              },
              {
                rule: 18,
                text: "Preventive health-care measures of particular relevance to women, shall be offered to women prisoners on an equal basis with women of the same age in the community",
              },
              {
                rule: 19,
                text: "Effective measures shall be taken to ensure that women prisoners’ dignity and respect are protected during personal searches",
              },
              {
                rule: 20,
                text: "Alternative screening methods, such as scans, shall be developed to replace strip searches and invasive body searches",
              },
              {
                rule: 22,
                text: "Punishment by close confinement or disciplinary segregation shall not be applied to pregnant women, women with infants and breastfeeding mothers in prison",
              },
              {
                rule: 25,
                text: "Women prisoners who report abuse shall be provided immediate protection, support and counseling, and their claims shall be investigated by competent and independent authorities, with full respect for the principle of confidentiality",
              },
              {
                rule: 28,
                text: "Visits involving children shall take place in an environment that is conducive to a positive visiting experience, including with regard to staff attitudes, and shall allow open contact between mother and child",
              },
            ]}
          />
        </Col>
      </Row>
    </>
  );
};

export default Page30Content;
