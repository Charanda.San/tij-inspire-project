import { ContainerOrange } from "@/components";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page48Content() {
  return (
    <ContainerOrange>
      <Row>
        <Col xs={12} lg={6} sm={12}>
          <div style={{ height: 400 }}>
            <img
              src="./images/page-48-content/image-48-01.webp"
              className="image-page"
            />
          </div>
          <Row className="mt-2">
            <Col xs={12} sm={6} lg={4}>
              <div className="my-2">
                <hr className="divider-short-orange" />
              </div>
              <p className="text-caption-image-nue">
                Music club and group therapy are also offered. The daily
                schedule is designed to maintain health and instill discipline,
                integrity and a sense of moral responsibility.
              </p>
            </Col>
          </Row>
        </Col>
        <Col xs={12} lg={6} sm={12}>
          <Row className="p-0 gx-3 gy-3">
            <Col xs={12} lg={6} sm={12}>
              <Row className="gy-3">
                <Col xs={12} lg={12} sm={6}>
                    <img
                      src="./images/page-48-content/image-48-02.webp"
                      className="image-page"
                    />
                </Col>
                <Col xs={12} lg={12} sm={6}>
                    <img
                      src="./images/page-48-content/image-48-03.webp"
                      className="image-page"
                    />
                </Col>
              </Row>
            </Col>
            <Col xs={12} lg={6} sm={12}>
              <img
                src="./images/page-48-content/image-48-04.webp"
                className="image-page"
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerOrange>
  );
}
