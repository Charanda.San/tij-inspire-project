import { CaptionImagePosition, ContainerAfterWorld } from "@/components";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page66Content() {
  return (
    <ContainerAfterWorld dividerCaption="AFTERWORD">
      <Row className="py-1 px-2">
        <Col xs={12} sm={12} lg={6} className="scroll1">
          <Row>
            <Col xs={12} sm={6} lg={6}>
              <p className="text-paragraph mb-2">
                on “Taking action against gender-related killing of women and
                girls” (GA Res. 68/191). It is worth noting that Her Royal
                Highness was able to chair not one but three informal
                consultations, bringing the related difficult negotiations to
                their successful conclusion and managing to reach the required
                consensus on all three draft resolutions.
              </p>
              <p
                style={{
                  textAlign: "center",
                  color: "#4d4e4d",
                  fontSize:'14px',
                  lineHeight: 1.3,
                }}
                className="text-roboto-condensed py-3"
              >
                “This was a major event because it marked an important moment in
                the discourse on how the international community can contribute
                to promote not only the implementation of the Millennium
                Development Goals, but also the mainstreaming of the rule of
                law, justice and security in the post-2015 development agenda.”
              </p>
              <p className="text-paragraph mb-2">
                In addition, later on, she masterminded the planning of “The
                Bangkok Dialogue on the Rule of Law: Investing in the Rule of
                Law, Justice and Security for the Post-2015 Development Agenda”,
                a major event hosted by the government of Thailand and organized
                by the Thailand Institute of Justice in cooperation with UNODC.
                This event brought together heads of state, political leaders,
                academics and civil society stakeholders, including high-level
                participation from eleven countries, as well as contributions by
                the UN Secretary-General and several other senior UN officials.
                It was significant because it marked an important moment in the
                discourse on how the international community can contribute to
                promote not only the implementation of the Millennium
                Development Goals, but also the mainstreaming of the rule of
                law, justice and security in the post-2015 development agenda.
                Its report, which was widely distributed first in New York at
                the Open Working Group on Sustainable Development Goals in
                February 2014 and, a few months later, in Vienna at the
                twenty-third session of the Commission on Crime Prevention and
                Criminal Justice, was well received and highly appreciated. I
                still recall vividly the words of the UN Secretary-General Ban
                Ki-moon: “The rule of law is the bedrock of peace and good
                governance, justice and human rights. It provides the foundation
                for all our development goals: from building societies shattered
                by conflict; to reducing poverty; promoting gender equality; and
                addressing global challenges
              </p>
            </Col>
            <Col xs={12} sm={6} lg={6}>
              <p className="text-paragraph">
                such as climate change”; and those of Her Royal Highness
                Princess Bajrakitiyabha: “In conclusion, I wish to stress that
                investing in the rule of law, crime prevention and criminal
                justice is not only essential, but indispensable, to the path of
                sustainable development.” In 2014, as Head of the delegation of
                Thailand at the twenty-third session of the Commission on Crime
                Prevention and Criminal Justice, which co-sponsored the draft
                resolutions on the Thirteenth UN Congress and the Standard
                Minimum Rules, Her Royal Highness submitted two draft
                resolutions for the final approval of the General Assembly: the
                first one on the “Rule of law, crime prevention and criminal
                justice in the United Nations development agenda beyond 2015”
                (GA Res. 69/195); and the second one on the “United Nations
                Model Strategies and Practical Measures for the Elimination of
                Violence against Children in the Field of Crime Prevention and
                Criminal Justice” (GA Res. 69/194).
              </p>

              <p className="text-paragraph mb-2">
                When Her Royal Highness returned to Thailand to resume her
                functions in the Attorney General’s Office, no doubt the United
                Nations Office in Vienna, and UNODC in particular, lost one of
                its most energetic representatives and strongest supporters.
                However, her leadership and commitment could be felt even though
                she was thousands of miles away, as demonstrated by her presence
                in Doha during the last UN Congress where she was elected as one
                of its Vice-Presidents and in such capacity chaired the second
                meeting of its high-level segment. In May 2015, at the
                twenty-fourth session of the Commission on Crime Prevention and
                Criminal Justice, the three draft resolutions co-sponsored by
                Thailand together with several other countries for final
                adoption by the General Assembly were all approved by consensus
                as GA resolution 70/174 on the “Thirteenth United Nations
                Congress on Crime Prevention and Criminal Justice” endorsing the
                Doha Declaration; GA resolution 70/175 on the “United Nations
                Standard Minimum Rules for the Treatment of Prisoners (the
                Nelson Mandela Rules)” adopting the revised text of the Rules;
                and GA resolution 70/176 on “Taking action against
                gender-related killing of women and girls”, which notes with
                appreciation the recommendations of the open-ended expert group
                meeting held in Bangkok and urges Member States to adopt
                integrated and comprehensive responses.
              </p>
              <p className="text-paragraph mb-2 d-none d-lg-block">
                On 25 September 2015, the General Assembly adopted its historic
                resolution 70/1 on “Transforming our World: The 2030 Agenda for
              </p>

              <p className="text-paragraph mb-2 d-block d-lg-none">
                On 25 September 2015, the General Assembly adopted its historic
                resolution 70/1 on “Transforming our World: The 2030 Agenda for
                Sustainable Development.” With its 17 Sustainable Development
                Goals (SDGs) and 169 targets, the ambitious resolution presented
                a new universal agenda drawing together the strands of peace,
                rule of law, human rights, development and equality into a
                comprehensive and forward-looking framework that reduces
                conflict, crime, violence, and discrimination, and ensures
                inclusion and good governance become key elements of people’s
                well-being and essential elements for securing sustainable
                development. Now, my question is: would its Goal 16 on “Peace,
                Justice and Strong Institutions” have been included without the
                important contribution of forums like the Doha UN Congress or
                without the persistent and hard advocacy work of visionaries
                like Her Royal Highness?
              </p>
            </Col>
          </Row>
        </Col>
        <Col xs={12} sm={12} lg={6} className="scroll1">
          <Row>
            <Col xs={12} sm={6} lg={6}>
              <p className="text-paragraph mb-2 text-paragraph mb-2 d-none d-lg-block">
                Sustainable Development.” With its 17 Sustainable Development
                Goals (SDGs) and 169 targets, the ambitious resolution presented
                a new universal agenda drawing together the strands of peace,
                rule of law, human rights, development and equality into a
                comprehensive and forward-looking framework that reduces
                conflict, crime, violence, and discrimination, and ensures
                inclusion and good governance become key elements of people’s
                well-being and essential elements for securing sustainable
                development. Now, my question is: would its Goal 16 on “Peace,
                Justice and Strong Institutions” have been included without the
                important contribution of forums like the Doha UN Congress or
                without the persistent and hard advocacy work of visionaries
                like Her Royal Highness?
              </p>
              <p
                style={{
                  textAlign: "center",
                  color: "#4d4e4d",
                  fontSize:'14px',
                  lineHeight: 1.3,
                }}
                className="text-roboto-condensed py-3"
              >
                “Her Royal Highness is a true standard-bearer of the supremacy
                of the rule of law over the law of the jungle; a staunch and
                passionate defender of the rights of the most vulnerable,
                particularly women and children; and an enthusiastic supporter
                of multilateralism, deserving the gratitude of the professional
                community for her significant contributions to the progressive
                development of international criminal law.”
              </p>
              <p className="text-paragraph mb-2">
                During these last five years, the Commission on Crime Prevention
                and Criminal Justice still continued to see Thailand as one of
                its main actors and major protagonists. In 2016 Thailand
                submitted a draft resolution on “Mainstreaming holistic
                approaches in youth crime prevention” (ECOSOC Res. 2016/18); in
                2017 Thailand was behind a draft resolution on “Promoting and
                encouraging the implementation of alternatives to imprisonment
                as part of comprehensive crime prevention and criminal justice
                policies” (ECOSOC Res. 2017/19); and in 2018 a draft resolution
                was again sponsored by Thailand on “The rule of law, crime
                prevention and criminal justice in the context of the
                Sustainable Development Goals” (GA Res. 73/185).
              </p>
            </Col>
            <Col xs={12} sm={6} lg={6}>
              <p className="text-paragraph">
                Finally in 2019 Thailand proposed a draft resolution on
                “Integrating sport into youth crime prevention and criminal
                justice strategies” (GA Res. 74/170). While this expert group
                meeting took place in Bangkok in December 2019, with its opening
                ceremony presided over by Her Royal Highness, I should not fail
                to recall that she was very pleased to join the first meeting of
                the Friends of the Fourteenth UN Congress – convened by our
                Japanese colleagues at the beginning of 2017 in Tokyo – to start
                plotting and planning about the substantive preparations for the
                Kyoto Congress, when we started discussing about its main theme,
                the substantive items and the workshops’ topics. In this
                connection, it should be remembered that what she had suggested
                as the main theme — “Advancing crime prevention, criminal
                justice and the rule of law: towards the achievement of the 2030
                Agenda” — was finally retained and approved by the Commission
                for adoption by the General Assembly in its resolution 72/192,
                thus becoming the main theme of the Kyoto Congress!
              </p>

              <p className="text-paragraph mb-2">
                And, after having refreshed this as well as other memories, I
                should now conclude by emphasizing how truly privileged I have
                been for having enjoyed the confidence, trust and friendship of
                a person as extraordinary as Her Royal Highness Princess
                Bajrakitiyabha Narendiradebyavati Kromluang Rajasarinisiribajara
                Mahavajra rajadhita – to her farang friends simply “Pat” – and
                for having had the opportunity to witness the growing
                international stature she managed to gain through the years as a
                doer and as a mover of major criminal justice reforms, both
                domestically and globally, commanding always respect and
                admiration. A true standard-bearer of the supremacy of the rule
                of law over the law of the jungle; a staunch and passionate
                defender of the rights of the most vulnerable, particularly
                women and children; and an enthusiastic supporter of
                multilateralism, she deserves the gratitude of the professional
                community for her significant contributions to the progressive
                development of international criminal law, as well as to the
                further enhancement of the United Nations crime prevention and
                criminal justice Standards and Norms. While the Thailand
                Institute of Justice should be praised for having taken the
                initiative of finalizing this publication both as a tribute and
                as a sort of institutionalization or continuum of HRH’s legacy
                going forward, as a longtime friend of Thailand and TIJ, I am
                keen to see how the seeds that have been sown over the years can
                bear even more abundant and delicious fruits, not only for the
                Thai people but for all our fellow human beings, for “we the
                people”, all over the world.
              </p>
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerAfterWorld>
  );
}
