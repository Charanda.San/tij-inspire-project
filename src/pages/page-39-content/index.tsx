import { CaptionImagePosition, ContainerLight, QuoteImage } from "@/components";
import { ICaptionPosition } from "@/components/CaptionImagePosition";
import ImageParagraphPage, {
  IPropsImageParagraphPage,
} from "@/components/ImageParagraph";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page39Content() {
  const propsImageParagraph: IPropsImageParagraphPage = {
    image: "./images/page-39-content/image-39-01.webp",
    positionCaption: "right",
    paragraphs: [
      <>
        <p>
          As with the Bangkok Rules, both the Thailand Institute of Justice
          (TIJ) and Office of the Attorney-General of Thailand (OAG) have played
          an important role in their regional dissemination. At the 13
          <sup>th</sup> United Nations Congress on Crime Prevention and Criminal
          Justice, in Doha, Qatar in April 2015, Her Royal Highness articulated
          in a speech that the adoption of the Model Strategies was merely “the
          point of departure.” To translate them into reality, she argued, the
          international community should now engage “in their wide
          dissemination, in fostering the political willingness to implement
          them, and in reviewing legislation and building capacity where
          needed.”
        </p>
        <p>
          The TIJ has, in partnership with the UNODC, fulsomely supported her in
          these aims by producing a range of technical assistance tools – an
          ‘Introduction Booklet’, ‘Checklist’
        </p>
      </>,

      <>
        <p>
          and ‘Pocket Guide’ – geared towards mobilizing criminal justice and
          child protection professionals, among other stakeholders. These give
          guidance on how everyone from legislators and judges to prosecutors
          can make daily use of the Model Strategies, address violence against
          children in a more strategic and effective manner and identify their
          own reform and capacity building agenda. In that spirit, the TIJ and
          UNODC has also arranged regional workshops aimed at police,
          prosecutors, judges and social workers and disseminated the Model
          Strategies, which are broken down into 47 practical measures, through
          public channels and forums.
        </p>
        <p>
          Similarly, the OAG, which played a role in the negotiation of the
          Model Strategies, has advocated for their implementation through
          south-south cooperation, particularly with neighboring countries such
          as Laos PDR.
        </p>
      </>,
      <>
        <p>
          Within Thailand, it has also sponsored multi-disciplinary capacity
          building courses for police, prosecutors and other professionals
          working with children.
        </p>
        More recently, the TIJ commissioned a report highlighting the good
        practices to have emerged across the region since the Model Strategies
        were ratified. Penned by child rights academic Eileen Skinnider, Towards
        Child-Sensitive Criminal Justice in Southeast Asia for Child Victims of
        Violence: Exploring Good Practices Using the UN Model Strategies on the
        Elimination of Violence against Children singles out positive
        developments from police outreach projects aimed at sexually exploited
        children to the proliferation of 24-hour toll-free child helplines — while
        also stressing the threats and challenges that children still face and
        the high prevalence of underreporting.
      </>,
    ],
    caption: `TIJ acts as co-host
    of a regional training workshop on violence against children (VAC) at UNESCAP in Bangkok in 2014.`,
  };
  const captions: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption:
        "Her Royal Highness at the Crime Congress held in Doha, Qatar, in 2015",
    },
    {
      position: "Bottom: ",
      caption:
        "The TIJ has co-published a range of technical assistance tools for child welfare stakeholders looking to enact the Model Strategies.",
    },
  ];
  return (
    <ContainerLight dividerCaption="THE RIGHTS OF CHILDREN">
      <Row className="p-0 gy-sm-4 gy-3">
        <Col xl={6} sm={12} xs={12} className="scroll1">
          <ImageParagraphPage {...propsImageParagraph} />
        </Col>
        <Col xl={6} sm={12} xs={12} className="scroll1">
          <Row className="gx-2 gy-2 gy-sm-3">
            <Col lg={8} xs={12} className="order-1 order-lg-0 order-sm-1">
              <Row className="p-0 gx-2 gy-3">
                <Col xs={12} lg={12}>
                  <div>
                    <img
                      src="./images/page-39-content/image-39-02.webp"
                      className="image-page"
                    />
                  </div>
                </Col>
                <Col xs={12} sm={8} lg={10}>
                  <div>
                    <img
                      src="./images/page-39-content/image-39-03.webp"
                      className="image-page"
                    />
                  </div>
                </Col>
                <Col xs={12} sm={4} lg={2} className="position-relative">
                  <CaptionImagePosition style="monster" position="top" captions={captions} />
                </Col>
              </Row>
            </Col>
            <Col
              lg={4}
              xs={12}
              sm={12}
              className="order-0 order-lg-1 order-sm-0 "
            >
              <QuoteImage
              fontSub="Neue Haas Unica"
                quote="“The adoption of the Model Strategies by the UN General Assembly sent a strong and unequivocal political message that we, the international community, do not tolerate any form of violence against children, that we believe in change, and that we are ready to engage to make the world a better place for our children.”"
                subQuote="Excerpt from Her Royal Highness Princess Bajrakitiyabha’s Keynote Remarks at the High-Level Panel
Discussion on Violence against Children in the Field of Crime Prevention and Criminal Justice, United Nations Headquarters, New York, 21 November, 2014"
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerLight>
  );
}
