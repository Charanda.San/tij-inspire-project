import React from "react";

const Page01 = () => {
  return (
    < >
      <img
      
        src="./images/page-01-home/page-01-desktop.webp"
        alt=""
        className="d-none d-sm-none d-lg-flex image-page"
      />
      <img
        src="./images/page-01-home/page-01-tablet.webp"
        alt=""
        className="d-none d-sm-flex d-lg-none image-page"
      />
       <img
        src="./images/page-01-home/page-01-mobile.webp"
        alt=""
        className="d-flex d-sm-none d-lg-none image-page"
      />
    </>
  );
};

export default Page01;
