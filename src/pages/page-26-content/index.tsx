import { ContainerOrange } from "@/components";
import { Inpractice } from "@/components/Widget";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const Page22Content = () => {
  return (
    <ContainerOrange>
      <Row className="h-100 gy-sm-3 gy-3">
        <Col lg={5} className="h-100 overflow-auto">
          <Row className="gy-3 gx-sm-3 gx-lg-2">
            <Col lg={12} className="order-0 order-sm-0 order-lg-0">
              <img src="./images/page-22-content/image-22.webp" alt="" />
            </Col>
            <Col
              lg={3}
              xs={12}
              sm={12}
              className="order-3  order-sm-3 order-lg-1"
            >
              <div className="my-2">
                <hr className="divider-short-orange" />
              </div>
              <p className="text-caption-image-nue">
                The daily routine at the Mother-Baby Centre includes health
                check-ups, exercise, rest and vocational training. Pregnant
                mothers take a nap after lunch. Typically, children stay with
                their mothers here until they turn one, after which they are
                sent home to stay with family members.
              </p>
            </Col>
            <Col
              lg={9}
              sm={12}
              className="d-sm-none d-lg-block d-none  order-lg-2 "
            >
              <img
                src="./images/page-26-content/image-26-desktop.webp"
                className="image-page h-auto"
              />
            </Col>
            <Col lg={9} sm={6} className="d-sm-grid d-none d-lg-none">
              <img
                src="./images/page-22-content/image-22-1.webp"
                className="image-page"
              />
            </Col>
            <Col lg={9} sm={6} className="d-sm-grid d-none d-lg-none">
              <img
                src="./images/page-22-content/image-22-2.webp"
                alt=""
                className="image-page"
              />
            </Col>
            <Col
              lg={5}
              xs={12}
              sm={6}
              className="d-block d-sm-none d-lg-none order-lg-2 order-1"
            >
              <img src="./images/page-22-content/image-22-1.webp" alt="" />
            </Col>
            <Col
              lg={5}
              xs={12}
              sm={6}
              className="d-block d-sm-none d-lg-none order-lg-3 order-2"
            >
              <img src="./images/page-22-content/image-22-2.webp" alt="" />
            </Col>
          </Row>
        </Col>
        <Col lg={7} className="h-100 scroll1">
          <Inpractice
            title="MOTHER-BABY CENTRE AT CHIANG MAI WOMEN CORRECTIONAL INSTITUTION"
            paragraph={[
              <>
                <p>
                  <span className="first-letter-orange">I</span>n a spotless
                  Mother-Baby Centre at Chiang Mai Women Correctional
                  Institution, two women are nursing their babies to sleep after
                  feeding-time. Others – clearly pregnant – lie resting quietly
                  on the padded, child-friendly floor.
                </p>
                <p>
                  “I felt stressed when I arrived here,” says Lek, who is due to
                  give birth any day now, only a few months into a
                  three-year-and-three-months sentence for drug possession. “I
                  was wondering how I could raise him and if anyone would help
                  me. However, after being screened and given an orientation, I
                  soon relaxed and got used to the facility and my new routine.”
                </p>
                <p>
                  As Lek speaks, a four-month-old baby boy nearby begins to
                  stir. His mother, Bee, is awaiting sentencing for drug
                  possession – a crime she intends to plead not guilty to. But
                  while a question mark remains over her long-term future, for
                  now her daily routine affords her the maximum possible time
                  together with her child and a nursery environment comparable
                  with those found outside prison.
                </p>
                <p>
                  “When I got here, I was eight months pregnant and panicking,”
                  she explains softly, “but the officers assisted me and
                  instructed me about my rights as an expectant mother. And
                  since my boy was born in the local hospital, we’ve seen the
                  doctor regularly and he’s been given all the vaccines he needs
                  as scheduled.”
                </p>
                <p>
                  Formerly a male prison, Chiang Mai Women Correctional
                  Institution is a bustling and efficiently run high-security
                  facility where over 2,000 female inmates are serving
                  sentences. It is also one of 12 Model Prisons in Thailand: in
                  2015, a Thailand Institute of Justice and Department of
                  Corrections committee assessed the levels of Bangkok Rules
                  implementation using a 154-indicator checklist and found them
                  to be high enough that it now serves as a yardstick for other
                  prisons in the region.
                </p>
                <p>
                  While the staff strive to adhere to all the ‘Rules of General
                  Application’ (those rules covering admissions and
                  registration, hygiene and healthcare, safety and security),
                  the prison ranks especially highly for the care it affords a
                  special category of inmate: pregnant women, breastfeeding
                  mothers and mothers with children in prison.
                </p>
                <p>
                  According to Laor Sriswan, supervisor of the Mother-Baby
                  Centre – which has a capacity of 23 prisoners and 12 babies
                  and was purpose built with the support of Princess
                  Bajrakitiyabha’s Kamlangjai Project in 2015 – processes
                  involving pregnant inmates and mothers with newborns have
                  become much more clearly defined since the Bangkok Rules were
                  introduced.
                </p>
              </>,
              <>
                <p>
                  “Before we had specific protocols for treating children, but
                  they weren’t closely regulated. It was more instinctive,” she
                  explains. Officers receive regular capacity building training,
                  she adds, from local public health professionals about child
                  development and new and existing health issues for babies.
                  This is in line with Rule 51, which states that children shall
                  receive ongoing health-care services and have their
                  development monitored by specialists.
                </p>
                <p>
                  Thailand’s amended Penitentiary Act states that babies are
                  permitted to stay with their incarcerated mothers until the
                  age of three. In reality, though, most are sent home to live
                  with close family relatives, or put into foster care, when
                  they turn one year old.
                </p>
                <p>
                  But, according to the Bangkok Rules, a prison’s duty to
                  facilitate a mother-child bond doesn’t end once a child leaves
                  it. Rules 28 and 52 state that “women prisoners shall be given
                  the maximum possible opportunity and facilities to meet with
                  their children”, and that these visits should “take place in
                  an environment that is conducive to a positive 47 visiting
                  experience” and “allow open contact between mother and child.”
                </p>
                <div style={{ width: "90%" }} className="d-block mx-auto">
                  <p style={{ fontSize: 18 }} className="text-normal">
                    “After being screened and given an orientation, I soon
                    relaxed and got used to the facility and my new routine.”
                  </p>
                  <div className="d-block ms-auto" style={{ width: "80%" }}>
                    <p
                      className="text-normal text-center fst-italic fw-bolder"
                      style={{ fontSize: "12px" }}
                    >
                      – Lek, an expectant mother serving a sentence for drug
                      possession
                    </p>
                  </div>
                </div>
                <p>
                  Chiang Mai Women Correctional Institution satisfies these
                  requirements through a special program – known as ‘Kamlangjai:
                  From Children to Mother’ – that allows children to meet their
                  mothers for extended periods on around 10 days per year,
                  including Mother’s Day, Father’s Day, Songkran festival, and
                  other public holidays. Conducted in a special seating area
                  within the prison, these visits are offered in addition to the
                  three open visits already permitted per year. Similarly, staff
                  from foster homes can bring children to see their mothers on
                  these days.
                </p>
                <p>
                  With three kids already and a tight-knit family that visits
                  her every two weeks, Lek plans to send her baby home early,
                  after three months of breastfeeding. The reason: so that she
                  can begin a normal prison routine and earn a chance to be
                  dispatched outside the prison for vocational training –
                  another area the facility excels in. “I want to learn Thai
                  massage and get certified,” she says, “so that I can work in
                  the city center and earn an income once I get out. I think
                  it’ll be good for me.”
                </p>
              </>,
            ]}
          />
        </Col>
      </Row>
    </ContainerOrange>
  );
};

export default Page22Content;
