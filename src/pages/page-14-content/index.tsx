import { CaptionImagePosition, LayoutText, QuoteImage } from "@/components";
import { ICaptionPosition } from "@/components/CaptionImagePosition";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import './page-14-content.css'

const Page10Content = () => {
  const caption1 = `Her Royal Highness with United Nations Secretary-General Ban Ki-moon at the 67th session of the United Nations General Assembly (UNGA) in New York City in 2012.`;
  const caption2 = `Princess Bajrakitiyabha talks with Dr. Eduardo Vetere at the High-Level Conference on Sustainable Development, Crime Prevention and Safe Societies in Bangkok, March 2018.`;
  const caption3 = `Princess Bajrakitiyabha attending the 21st CCPCJ in Vienna, 2012.`;
  const captions: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: caption1,
    },
    {
      position: "Bottom left: ",
      caption: caption2,
    },
    {
      position: "Bottom right: ",
      caption: caption3,
    },
  ];
  const captionsMobile: ICaptionPosition[] = [
    {
      position: (
        <>
          1 <sup>st</sup>:&nbsp;&nbsp;
        </>
      ),      caption: caption1,
    },
    {
      position: (
        <>
          2 <sup>nd</sup>:&nbsp;&nbsp;
        </>
      ),      caption: caption2,
    },
    {
      position: (
        <>
          3 <sup>rd</sup>:&nbsp;&nbsp;
        </>
      ),      caption: caption3,
    },
  ];
  return (
    <LayoutText
      divider="The Criminal Justice Background of HRH Princess Bajrakitiyabha"
      leftComponent={
        <Row>
          <Col lg={6} xs={12} sm={6}>
            <div className="text-paragraph">
              <p>
                In 2001 — the same year as that encounter at Bangkok Central
                Women Correctional Institution — she applied to do her master's
                degree in law at Cornell Law School in New York. Among the top
                Ivy League law schools in the United States, Cornell was — and
                remains — internationally  renowned for its rigorous curriculum.
                A member of Her Royal Highness's dissertation reading
                committee during those years was Cornell alumnus, Dr. Kittipong
                Kittayarak, a scholar who has been instrumental in shaping
                judicial reform in Thailand. Ahead of her graduate legal studies
                abroad, he recalls her as a conscientious young woman. “She was
                quick-witted and highly intelligent. Her interest was not
                confined only to the law in the book. She was also keen to
                discuss issues on criminal justice reform of the day,” he says.
                “But at the time I didn't think she was going to become this
                devoted to the work she went on to do.”
              </p>
              <p>
                After completing her masters, Princess Bajrakitiyabha embarked
                upon a three-year doctorate degree in juridical science. Her
                2005 dissertation — <span className="fst-italic">Towards Equal Justice: Protection of the
                Rights of the Accused in the Thai Criminal Justice Process - A
                Comparison with France and the United States</span> - offers a
                revealing window into her beliefs and vision during that time.
                At its heart is the fundamental question of fairness in the
                criminal justice process in Thailand, or, as she writes,
                “whether criminal suspects and defendants in criminal cases,
                most of whom are poor and less educated, have adequate
                protection against powerful state law enforcement mechanisms,
                both in terms of a defendant's basic rights and the
                implementation of those rights in actual practice.”
              </p>
              <p>
                As well as discussing the strengths and weaknesses of Thailand's
                mixed system of law, particularly regarding the rights of the
                accused, the dissertation also expresses hope for a more just,
                rights-orientated future. Among her closing recommendations are
                some that would run like a thread through her subsequent work.
                “In my opinion, to be more effective at reducing crime, there
                should be a fundamental shift away from punitive measures
                towards prevention, treatment, and restoration,” she writes.
                “Criminal law and criminal process should be used only for real
                crime and criminals.”
              </p>
              <p>
                Another far-sighted, reform-minded recommendation calls for more
                human rights education among criminal justice
              </p>
            </div>
          </Col>
          <Col lg={6} xs={12} sm={6}>
            <div className="text-paragraph">
              <p>
                professionals. “Having a good criminal law and criminal
                procedure may not be sufficiently adequate to achieve due
                process and efficiency in law enforcement,” she wrote. “The
                attitudes of judicial and law enforcement officials are also of
                equal, or even more, importance.”
              </p>
              <p>
                Internships during her years at Cornell included a period as a
                trainee consultant at a leading law firm in Washington D.C.,
                where she advised on international trade law. And at the Office
                of the District Attorney in New York, she learnt about arrest
                and detention procedures, witness inquiry, investigation of
                evidences, preparation of court files and the conduct of court
                cases under American law. These gave her a strong grounding in
                the inner workings of criminal law.
              </p>
              <p>
                The years immediately following her 2005 graduation from both
                Cornell and as a Barrister-at-Law in Thailand (during her
                doctoral studies she was also studying remotely at the Bar
                Association of Thailand) were as productive as they were
                unpredictable. In particular, a one-year tenure at the Permanent
                Mission of Thailand in New York, from 2005-2006, gave her
                valuable lessons in foreign affairs and a skillset that would
                prove vital in the years to come. Under the close guidance of
                the formidable Ambassador Laxanachantorn Laohaphan, Her Royal
                Highness earnt her diplomatic stripes as First Secretary,
                handling everything from daily duties to the drafting and
                negotiating of United Nations resolutions. Among other
                achievements, she was instrumental in getting the 'Bangkok
                Declaration', a joint statement outlining UN Member States'
                alliances and commitments in regard to crime prevention and
                criminal justice, drawn up during a UN Crime Congress in
                Bangkok. It was later endorsed by the General Assembly.
              </p>
              <p>
                A full-time career in international multilateral diplomacy
                beckoned. Yet Her Royal Highness chose to return to Thailand in
                2006, her wish being — as her dissertation had hinted — to work
                in the field of Thai criminal justice. Doubly surprising to some
                observers was her decision to work as an Assistant Public
                Prosecutor within the Office of Attorney General. Many had
                expected her to pursue a career as a judge instead. From Dr.
                Kittipong's observation, she has always preferred interacting
                with people to sitting behind a desk or a bench,
              </p>
            </div>
          </Col>
        </Row>
      }
      rightComponent={
        <>
          <Row className="px-2 gy-2 ">
            <Col lg={6} xs={12} sm={6}>
              <QuoteImage
                subQuote="Excerpt from Chapter 5 (Evaluation and Recommendations) of Her Royal Highness Princess Bajrakitiyabha’s Dissertation, Towards Equal Justice: Protection of the Rights of the Accused in the Thai Criminal Justice Process — A Comparison with France and the United States (August 2005)"
                quote={`“By relying too heavily on criminal law as the only tool to maintain peace and order in the society, one risks the situation of over-criminalization, which may lessen the true purpose
of criminal law as well as strain the efficiency of criminal justice administration. The experience of Thailand in fighting drug problems is a good example.”`}
              ></QuoteImage>
            </Col>
            <Col lg={6} xs={12} sm={6}>
              <img
                src="./images/page-10-content/image-10.webp"
                className="image-page"
              />
            </Col>
            <Col lg={3} sm={12} className="order-3 order-lg-0">
              <div className="d-none d-lg-block d-sm-block">
              <CaptionImagePosition captions={captions} position="top" />

              </div>
              <div className="d-block d-lg-none d-sm-none">
              <CaptionImagePosition captions={captionsMobile} position="top" />

              </div>
            </Col>
            <Col lg={9} sm={12} className="d-sm-grid d-none">
              <div className="image-group-14">
                <img
                  src="./images/page-10-content/image-10-2.webp"
                  className="image-page h-auto"
                />
                <img
                  src="./images/page-10-content/image-10-3.webp"
                  className="image-page h-auto"
                />
              </div>
            </Col>
            <Col lg={5} sm={6} className="d-block d-sm-none">
              <img
                src="./images/page-10-content/image-10-2.webp"
                className="image-page h-auto"
              />
            </Col>
            <Col lg={3} sm={6} className="d-block d-sm-none">
              <img
                src="./images/page-10-content/image-10-3.webp"
                className="image-page h-auto"
              />
            </Col>
          </Row>
        </>
      }
    ></LayoutText>
  );
};

export default Page10Content;
