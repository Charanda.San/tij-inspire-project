import React from "react";
import { Col, Row } from "react-bootstrap";
import { WrapperContent } from "src/components/Widget";
import "./page-03.css";
import CaptionImagePosition, {
  ICaptionPosition,
} from "@/components/CaptionImagePosition";

const Page03 = () => {
  const captionText =
    "At Khao Prik Agriculture Industrial Institution, inmates gather to be assigned their work duties for the day. The inmates learn a wide variety of agricultural skills at the prison, so that they can have a better chance of earning a sufficient living on their release.";
  const captions: ICaptionPosition[] = [
    {
      position: "Page 9: ",
      caption:
        "Princess Bajrakitiyabha visits a class at Chiang Mai Women Correctional Institution, 2018.",
    },
    {
      position: "Page 10: ",
      caption: (
        <>
          Handcrafted bells, created as part of the Kamlangjai Project, are a
          symbol of awareness about violence against women. This bell was one of
          many hanging from fencing at Chiang Mai{" "}
          <p>Women Correctional Institution.</p>{" "}
        </>
      ),
    },
  ];
  const captionsDesktop: any[] = [
    {
      position: "Opposite page: ",
      caption: captionText,
    },
    // ...captions,
  ];
  const captionsTablet: any[] = [
    {
      position: "",
      caption: captionText,
    },
    // ...captions,
  ];
  const captionsMobile: any[] = [
    {
      position: "",
      caption: captionText,
    },
    // ...captions,
  ];
  const Content = () => (
    <>
      <h6 className="topic">
        <strong className="text-bookman-topic">© TIJ 2023. All rights reserved.</strong>
      </h6>
      <div className="text-param text-bookman">
        <p>This website and its content is copyright of TIJ</p>
        <p>
          No part of the content may be reproduced or transmitted in any form or
          by any means without express written permission from the author,
          except in the case of brief quotations embodied in critical articles
          and reviews.
          <br />
          <br />
          Please refer all pertinent questions to the Thailand Institute of
          Justice (TIJ). Reproduction, distribution, or use for any commercial
          benefit of the content is prohibited.
          <br />
          <br />
        </p>
      </div>
      <h6 className="topic">
        <strong className="text-bookman-topic">Acknowledgements</strong>
      </h6>
      <div className="text-param text-bookman">
        <p className="text-bookman-italic">
          The Thailand Institute of Justice (TIJ) would like to extend our
          sincere thanks to the following individuals for being interviewed or
          providing information, material or other forms of support to make this
          publication possible:
        </p>
        <p>
          Adisak Panupong, Anuwan Vongphichet, Aungkanung Lebnark, Chontit
          Chuenurah M.L. Dispanadda Diskul, Eduardo Vetere, Ekaphop
          Detkriangkraisorn, Jeremy Douglas, Kittipong Kittayarak, Nathee
          Chitsawang, Phiset Sa-ardyen, Santanee Ditsayabut, Sirithon
          Wairatpanij, Sommanat Juaseekoon, Ticha Na Nakorn, Vongthep
          Arthakaivalvatee and Wisit Wisitsora-at.
        </p>
        <p className="text-bookman-italic">
          The TIJ would also like to thank the following organizations for their
          generous support:
        </p>
      </div>
    </>
  );
  return (
    <WrapperContent>
      <Row className="chapterStyle">
        <Col
          className="h-100 position-relative d-none d-sm-none d-lg-block"
          lg={4}
          xs={12}
          sm={12}
          style={{ borderRight: "1.5px solid grey" }}
        >
          <div className="px-lg-4  position-text-image">
            <Content />
          </div>
        </Col>
        <Col
          className="h-100 d-block  d-sm-block d-lg-none"
          lg={4}
          xs={12}
          sm={12}
        >
          <div className="">
            <Content />
          </div>
        </Col>

        {/* for Tablet + Mobile */}
        <Col xs={12} sm={12} className="d-block d-lg-none">
          <img
            src="./images/page-03-content/image-page-06.webp"
            className="image-page w-100"
          />
        </Col>
        <Col xs={12} sm={12} className="d-sm-block d-none d-lg-none mt-2">
          <CaptionImagePosition captions={captionsTablet} position="top" />
        </Col>
        <Col xs={12} sm={12} className="d-block d-sm-none d-lg-none mt-2">
          <CaptionImagePosition captions={captionsMobile} position="top" />
        </Col>

        {/* for Desktop */}
        <Col
          lg={8}
          xs={12}
          sm={12}
          className="position-relative d-lg-flex justify-content-end d-none d-sm-none"
        >
          <Row className="position-text-image w-100">
            <Col lg={2} xs={12} className="position-relative">
              <CaptionImagePosition
                captions={captionsDesktop}
                position="bottom"
              />
            </Col>
            <Col lg={10} xs={12} className="h-100">
              <img
                src="./images/page-03-content/image-page-06.webp"
                className="image-page w-100"
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </WrapperContent>
  );
};

export default Page03;
