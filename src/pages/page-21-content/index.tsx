import { ContainerOrange } from "@/components";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Inpractice } from "src/components/Widget";

const Page17Conent = () => {
  return (
    <ContainerOrange>
      <Row className="h-100 m-0">
        <Col lg={5} xs={12} className="p-0">
          <Row className="gx-2 gy-2">
            <Col lg={12} sm={12} xs={12}>
              <div style={{ height: 300, marginBottom: "10px" }}>
                <img
                  src="./images/page-17-content/page-17.webp"
                  alt=""
                  className="image-page"
                />
              </div>
            </Col>
            <Col lg={2} sm={4} xs={12}>
              <div className="my-2">
                <hr className="divider-short-orange" />
              </div>
              <p className="text-caption-image-nue">
                Exercise and cleaning duties are part of the daily routine in
                the female wing of Chanthaburi Provincial Prison.
              </p>
            </Col>
            <Col lg={5} sm={4} xs={12}>
              <div style={{ height: 180, marginBottom: "10px" }}>
                <img
                  src="./images/page-17-content/page-17-1.webp"
                  alt=""
                  className="image-page"
                />
              </div>
            </Col>
            <Col lg={5} sm={4} xs={12}>
              <div style={{ height: 180, marginBottom: "10px" }}>
                <img
                  src="./images/page-17-content/page-17-2.webp"
                  alt=""
                  className="image-page"
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col lg={7} xs={12} className="scroll1 p-0 p-lg-2">
          <Inpractice
            title="HOW THE KAMLANGJAI PROJECT HELPS CHANTHABURI PRISON INMATES"
            paragraph={[
              <>
                <p className="d-none d-lg-block">
                  <span className="first-letter-orange">O</span>
                  um, a mother of two serving a 3-year, 9-month sentence in
                  Chanthaburi Provincial Prison, is feeling positive. “Next
                  month I am going home,” she says whilst preparing a cup of
                  latte in Inspire coffee shop, which serves a full menu of
                </p>
                <p className="d-block d-lg-none">
                  <span className="first-letter-orange">O</span>
                  um, a mother of two serving a 3-year, 9-month sentence in
                  Chanthaburi Provincial Prison, is feeling positive. “Next
                  month I am going home,” she says whilst preparing a cup of
                  latte in Inspire coffee shop, which serves a full menu of
                  prisoner-made drinks and cakes to the general public. “My plan
                  is to use the barista skills I've gained here to set up my own
                  coffee shop.”
                </p>
                <p className="d-none d-lg-block">
                  prisoner-made drinks and cakes to the general public. “My plan
                  is to use the barista skills I've gained here to set up my own
                  coffee shop.”
                </p>
                <p>
                  Located on the edge of Chanthaburi town, Chanthaburi
                  Provincial Prison is a microcosm of a strained Thai
                  corrections system. But while overcrowded and under resourced,
                  the prison strives to offer as many female inmates as possible
                  — around 190 of a total female prison population of 290 —
                  vocational classes and activities. Covering baking, fortune
                  telling and traditional local crafts such as mat weaving,
                  these vocational courses fulfill multiple functions, including
                  offering prisoners a source of income and, more importantly,
                  preparing them for their release back into society.
                </p>
                <p>
                  Judging by Oum's optimism, they can also raise the spirits
                  during bleak times. While in her late 20s, circumstances led
                  her into the lures of drug use and dealing. Although she
                  worked in one of the eastern province's many rubber tree
                  plantations, it was not enough to provide for her two
                  daughters, so she began selling cheap and highly addictive
                  methamphetamine, known as ya ba, on the side. “Financial need
                  led me to sell drugs,” she explains, “and I also had a new
                  partner who was a user.”
                </p>
                <p>
                  Shortly after she was sentenced for drug-related offences —
                  like 80% of Thai female inmates — Oum began to immerse herself
                  in the prison activities on offer, most of which are the
                  result of private sector collaborations facilitated by
                  Princess Bajrakitiyabha's Kamlangjai Project initiative.
                  Initially she joined the team making traditional Thai drinks,
                  but, in March 2019, she was selected to train under a barista
                  from the Thai café chain Inthanin Coffee.
                </p>
                <p>
                  “When I'm working here I feel acceptance from the community,”
                  she says of her job, which starts at 6.50 am each morning with
                  her being escorted the short distance from the prison gates to
                  a nearby mangrove area, above which the small, air-conditioned
                  wooden hut housing the Inspire coffee shop sits on stilts.
                  “Being outside you get to see and interact with people. I feel
                  there's no judgment. It gives me some hope for the future.”
                </p>
                <p className="d-none d-lg-block">
                  According to the Thailand Institute of Justice and Penal
                  Reform International, overcrowding is one of the main
                  impediments to developing prison-based rehabilitation
                  programs. In the case of Chanthaburi Provincial Prison, the
                  situation is such that Kamlangjai Project activities are
                  currently focused predominantly on the female population.
                  However, director Chan Wachiradech hopes to one day extend the
                  same opportunities for skill acquisition and outside
                  interaction to male
                </p>
                <p className="d-block d-lg-none">
                  According to the Thailand Institute of Justice and Penal
                  Reform International, overcrowding is one of the main
                  impediments to developing prison-based rehabilitation
                  programs. In the case of Chanthaburi Provincial Prison, the
                  situation is such that Kamlangjai Project activities are
                  currently focused predominantly on the female population.
                  However, director Chan Wachiradech hopes to one day extend the
                  same opportunities for skill acquisition and outside
                  interaction to male inmates. “If there's a sense of acceptance
                  at the very beginning, it's going to be much easier for the
                  social reintegration of the offenders in the future,” he says.
                  “I want to make that happen. And create more job opportunities
                  for them as well.”
                </p>
              </>,
              <>
                <p className="d-none d-lg-block">
                  inmates. “If there's a sense of acceptance at the very
                  beginning, it's going to be much easier for the social
                  reintegration of the offenders in the future,” he says. “I
                  want to make that happen. And create more job opportunities
                  for them as well.”
                </p>
                <p>
                  Under construction is a Kamlangjai Project tourist attraction:
                  a floating market where the public will be able to enjoy
                  traditional snacks and crafts. “I want to make the environment
                  around the prison look and feel more like a community,” he
                  says, “so that the public and inmates interact.” All the
                  services will be offered by prisoners, he adds, garbed in
                  traditional uniforms. “If they are outside but wearing attire
                  from the inside there will always be a sense of
                  stigmatization...I don't want them to feel that.” He also
                  points out this enterprise is as much about educating the
                  public as rehabilitating prisoners. “The purpose of doing this
                  is not only to create work for them, but also to make people
                  outside understand that people who have taken a wrong step in
                  life can still be useful to society. I want the public to see
                  that.”
                </p>
                <p>
                  As regulations stipulate that those selected to work outside
                  are first-time offenders and have less than 3 years and 6
                  months of their sentence remaining, many female prisoners at
                  Chanthaburi Provincial Prison are not eligible. However, the
                  Kamlangjai Project is also giving many of those prisoners
                  confined to life behind bars a sense of purpose.
                </p>
                <p>
                  “I consider the Kamlangjai Project a form of moral support,”
                  says Gift, who is serving a 25-year sentence for drug offences
                  but has taken full advantage of the fortune telling classes it
                  offered her. Since being taught how to read tarot cards a few
                  years ago, she has passed on her wisdom to 35 other inmates,
                  six of whom currently remain at the prison. “You feel a bit of
                  pride when you teach other people what you already know,” she
                  says. She also believes that it might be a source of income
                  one day. “I dream of becoming a teacher when I'm released,”
                  she says. “It's a useful skill as you don't need much capital
                  or equipment. There's no need for investment. All you need is
                  knowledge.”
                </p>
                <p>
                  A further 23 female prisoners have been trained in sua
                  chantabhoon, or reed mat weaving. Through an agreement with a
                  local vocational college, this unit has been trained in every
                  step of this age-old craft unique to the province. Other
                  inmates, meanwhile, are employed in the Kamlangjai Project's
                  smock making, embroidery, drink making and bakery sections.
                  All units work five days a week and share any profits from the
                  sale of their products.
                </p>
                <p>
                  “I feel very grateful for the work of Her Royal Highness,”
                  says Oum, expressing a sentiment widely held among prisoners
                  involved in the Kamlangjai Project. “She has helped develop
                  prison policy that takes into account the importance of giving
                  another chance to prisoners and that really matters for my
                  future. This is a big opportunity for me.”
                </p>
              </>,
            ]}
          />
        </Col>
      </Row>
    </ContainerOrange>
  );
};

export default Page17Conent;
