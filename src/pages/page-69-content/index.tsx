import {
  CaptionImagePosition,
  ContainerAfterWorld,
  TextBrown,
} from "@/components";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page69Content() {
  const year2009: string[] = [
    `Becomes Deputy Provincial Chief Public Prosecutor, Office of Udon Thani Public Prosecution`,
    `United Nations Rules for the Treatment of Women Offenders, or Bangkok Rules, are drafted at an expert roundtable meeting held in Bangkok`,
    `Receives an Award of Recognition medal from the UNODC`,
  ];
  const bullets1: any[] = [
    `Mahidol University confers upon HRH Princess Bajrakitiyabha an Honorary Doctorate Degree in Criminology, Justice Administration and Society`,
    <>
      The 18<sup>th</sup> session of the CCPCJ is held in Vienna
    </>,
    `Works as visiting expert at the John Jay College of Criminal
Justice, New York`,
  ];
  const year2010: string[] = [
    `Becomes Deputy Provincial Chief Public Prosecutor, Office of Pattaya Public Prosecution`,
    `Thammasat University presents an Honorary Doctorate degree in the field of Justice Administration and an Honorary Doctorate degree in Law to
    HRH Princess Bajrakitiyabha`,
    `The United Nations General Assembly adopts the Bangkok Rules`,
    `Initiates Kamlangjai Project to apply His Majesty’s Sufficiency Economy Philosophy to assist inmates on the verge of release`,
  ];
  const year2011: string[] = [
    `Becomes Deputy Provincial Chief Public Prosecutor, Office of Nong Bua Lam Phu Public Prosecution`,
    `Works as Provincial Chief Public Prosecutor attached to the Office of the Attorney General, Department of Civil Rights Protection and Legal Aid`,
    `Receives the President’s Award from the International Corrections and Prisons Association (ICPA) and UNIFEM (now UN Women) Award for her work as Goodwill Ambassador and for her leading role in advocating for the end of violence against women`,
    `The Thailand Institute of Justice (TIJ) is established`,
  ];

  const year2012: any[] = [
    `Receives an Honorary Doctorate degree in Law from Illinois Institute of Technology, USA`,
    <>
      Becomes an Ambassador and Alternate Representative of Thailand on the
      Commission on Crime Prevention and Criminal Justice (CCPCJ), serving as
      its Chairperson for the 21<sup>st</sup> Session
    </>,
  ];
  const year2013: any[] = [
    <>
      Represents Thailand at the 68<sup>th</sup> UNGA attending the High-Level
      Meeting on the Rule of Law at the National and International Levels
    </>,
    <>
      Visits Qatar at the invitation of the Qatari government to share
      experiences with and support the national focal point (Qatari Ministry of
      Interior) on the roles of the host country in preparation for the 13
      <sup>th</sup> UN Crime Congress
    </>,
    `Presides over the opening of the Bangkok Dialogue on
the Rule of Law: Investing in the Rule of Law, Justice and Security for the Post-2015 Development Agenda`,
  ];
  const year2014: any[] = [
    `Works as Provincial Chief Public Prosecutor attached to the Office of the Attorney General, Office of
Nong Bua Lam Phu Public Prosecution`,
    <>
      The 25<sup>th</sup> anniversary of the UN Convention on the Rights of the
      Child (CRC) is celebrated
    </>,
    `The United Nations adopts the Model Strategies and Practical Measures on the Elimination of Violence against Children in the Field of Crime Prevention and
Criminal Justice`,
  ];
  const year2015: string[] = [
    `Works as Provincial Chief Public Prosecutor attached to the Office of the Attorney General, Department of Narcotics Litigation`,
  ];

  return (
    <ContainerAfterWorld dividerCaption="Appendices">
      <Row s className="py-1 px-2 scroll1 timeline-responsive">
        <Col xs={12} lg={3} className="grid-divider">
          <TextBrown year="2009" bullets={year2009} />
          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-69-content/image-69-01.webp"
              className="image-page"
            />
          </div>
          <TextBrown bullets={bullets1} />
          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-69-content/image-69-02.webp"
              className="image-page"
            />
          </div>
        </Col>
        <Col xs={12} lg={3} className="grid-divider">
          <TextBrown year="2010" bullets={year2010} />
          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-69-content/image-69-03.webp"
              className="image-page"
            />
          </div>
          <TextBrown year="2011" bullets={year2011} />
        </Col>
        <Col xs={12} lg={3} className="grid-divider">
          <TextBrown
            bullets={[
              "Becomes Permanent Representative of the Kingdom of Thailand to the United Nations in Vienna",
            ]}
          />
          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-69-content/image-69-04.webp"
              className="image-page"
            />
          </div>
          <TextBrown year="2012" bullets={year2012} />
          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-69-content/image-69-05.webp"
              className="image-page"
            />
          </div>
        </Col>
        <Col xs={12} lg={3} className="grid-divider">
          <TextBrown year="2013" bullets={year2013} />
          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-69-content/image-69-06.webp"
              className="image-page"
            />
          </div>
          <TextBrown year="2014" bullets={year2014} />
          <TextBrown year="2015" bullets={year2015} />
        </Col>
      </Row>
    </ContainerAfterWorld>
  );
}
