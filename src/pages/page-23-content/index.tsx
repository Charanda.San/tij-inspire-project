import React from "react";
import { ContentGreen, ImageBackground } from "@/components";
import { IPropsContent } from "@/components/ContentGreen";
import { Col, Row } from "react-bootstrap";
import CaptionImagePosition, {
  ICaptionPosition,
} from "@/components/CaptionImagePosition";
import "./page-23-content.css";

export default function Page23Content() {
  const captions: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: (
        <>
          Princess Bajrakitiyabha cuts the ribbon at the Kamlangjai Project
          exhibition held on the sidelines of the 17<sup>th</sup> Session of the
          Commission on Crime Prevention and Criminal Justice in Vienna,
          Austria, April 2008.
        </>
      ),
    },
    {
      position: "Bottom: ",
      caption: (
        <>
          A copy of the draft resolution under the consideration of the 65
          <sup>th</sup>
          session of UN General Assembly, Third Committee, Agenda Item 105, in
          which the Bangkok Rules were annexed and subsequently adopted. The
          signatures are those of the Thai delegation, including Princess
          Bajrakitiyabha.
        </>
      ),
    },
  ];
  const propsContent: IPropsContent = {
    title: (
      <div>
        The Build-up to <br /> the Bangkok Rules
      </div>
    ),
    textDividerLeft: "inspire",
    textDividerRight: "THE RIGHTS OF WOMEN",
    character: "o",
    subTitle: `ver the years, Her Royal Highness Princess Bajrakitiyabha’s promotion of the rights of the vulnerable has consistently been advanced on both the domestic and diplomatic fronts. The
    multipolar yet complementary nature of her work first became apparent in early 2008, when she decided to present the progress of her small-scale yet impactful Kamlangjai Project at a United Nations meeting on crime prevention and criminal justice. The positive reactions garnered here spurred her to start a campaign to rally support for the first ever set of global standards for the treatment of female offenders. Inextricably linked with her hands-on humanitarian work at home, yet also part of a much bigger, global effort involving many international bodies and stakeholders, this two-year campaign culminated with the UN’s adoption of what are today known as the Bangkok Rules.`,
    paragraphs: [
      <p>
        The UN event at which Her Royal Highness showcased the Kamlangjai
        Project in April 2008 – the 17<sup>th</sup> Session of the Commission on
        Crime Prevention and Criminal Justice (CCPCJ) in Vienna – did not mark
        her first foray into the challenging and complex world of
        multilateralism. Previously, while serving as First Secretary at the
        Permanent Mission of Thailand in New York in 2005, she had represented
        Thailand at the 60<sup>th</sup>
        United Nations General Assembly. She also played a key role in drafting
        and negotiating an important follow-up resolution to the Eleventh United
        Nations Congress on Crime Prevention and Criminal
      </p>,
      <>
        <p>
          Justice in Bangkok in April 2005. Known as the Bangkok Declaration,
          this resolution (60/177) – which built on the momentum of the congress
          by calling for concerted action on a range of criminal justice policy
          areas, including ‘the adequacy of standards and norms in relation to
          prison management and prisoners’ – marked the symbolic moment when
          Thailand began taking more initiative on criminal justice-related UN
          matters.
        </p>
        <p className="d-none d-lg-block">
          These were notable diplomatic achievements on the part of Princess
          Bajrakitiyabha. However, something even more striking happened at
        </p>
        <p className="d-block d-lg-none">
          These were notable diplomatic achievements on the part of Princess
          Bajrakitiyabha. However, something even more striking happened at the
          17<sup>th</sup> CCPCJ session: her hands-on work in Thai prisons began
          to trigger global reform. On the sidelines of this five-day event, Her
          Royal Highness hosted an exhibition introducing the Kamlangjai Project
          as a case study. In both her opening statement and keynote speech, she
          also spoke passionately about its achievements to date, namely the
          tangible headway made vis-à-vis mainstreaming gender sensitivity
          within the Thai prison system. The feedback from those present was
          encouraging. While the Kamlangjai Project had been created out of her
          belief that there was an anachronistic gender gap in the Thai penal
          system, the
        </p>
      </>,
      <p className="d-none d-lg-block">
        the 17<sup>th</sup> CCPCJ session: her hands-on work in Thai prisons
        began to trigger global reform. On the sidelines of this five-day event,
        Her Royal Highness hosted an exhibition introducing the Kamlangjai
        Project as a case study. In both her opening statement and keynote
        speech, she also spoke passionately about its achievements to date,
        namely the tangible headway made vis-à-vis mainstreaming gender
        sensitivity within the Thai prison system. The feedback from those
        present was encouraging. While the Kamlangjai Project had been created
        out of her belief that there was an anachronistic gender gap in the Thai
        penal system, the
      </p>,
      // <p className="d-block d-lg-none">
      //   the 17<sup>th</sup> CCPCJ session: her hands-on work in Thai prisons
      //   began to trigger global reform. On the sidelines of this five-day event,
      //   Her Royal Highness hosted an exhibition introducing the Kamlangjai
      //   Project as a case study. In both her opening statement and keynote
      //   speech, she also spoke passionately about its achievements to date,
      //   namely the tangible headway made vis-à-vis mainstreaming gender
      //   sensitivity within the Thai prison system. The feedback from those
      //   present was encouraging. While the Kamlangjai Project had been created
      //   out of her belief that there was an anachronistic gender gap in the Thai
      //   penal system, the empathetic reactions and alliances forged in Vienna
      //   indicated that the issue was widespread.
      // </p>,
    ],
    componentRightSide: (
      <>
        <Row className="gx-2 gy-2 gy-lg-2 gy-sm-3 mb-3">
          <Col xs={12} sm={12} lg={8} className="d-grid mb-2">
            <img
              alt=""
              src="./images/page-23-content/image-23-01.webp"
              className="image-page h-auto"
            />
          </Col>
          <Col xs={12} sm={12} lg={4} className="position-relative">
            <CaptionImagePosition style="monster"  captions={captions} position="top" />
          </Col>
          <Col xl={4} sm={12}>
            <img
              src="./images/page-23-content/image-23-02.webp"
              className="image-page h-auto"
            />
          </Col>
          <Col xl={4} sm={12}>
            <div className="text-paragraph-nue">
              <p className="mb-2 d-none d-lg-block">
                empathetic reactions and alliances forged in Vienna indicated
                that the issue was widespread.
              </p>
              <p className="mb-2">
                Emboldened by the success at the 17<sup>th</sup> Session, Her
                Royal Highness went on to spearhead a two-year advocacy
                campaign, known as ‘Enhancing the Lives of Female Inmates’, or
                ELFI. This aimed to rally international support for the
                development of the first set of UN standards for the treatment
                of women offenders – often by linking them to broad human rights
                arguments. Steadily, meeting by meeting, speech by speech, this
                tactical approach gained traction. As Vongthep Arthakaivalvatee,
                a Special Advisor to the Thailand Institute of Justice and
                Former Deputy Secretary-General of ASEAN who was then serving at
                the Royal Thai Embassy in Vienna, recounts: “After the 17
                <sup>th</sup> Session in Vienna, things snowballed to the point
                where Thailand was now the one moving the agenda. The stars were
                aligning.”
              </p>
              <p className="mt-0 d-none d-lg-block">
                Essential to the gestation of what became known as the Bangkok
                Rules was an expert
              </p>
            </div>
          </Col>
          <Col xl={4} sm={12}>
            <div className="text-paragraph-nue ">
              <p className="d-block d-lg-none">
                Essential to the gestation of what became known as the Bangkok
                Rules was an expert roundtable meeting held at Bangkok’s Golden
                Tulip Hotel from February 2-4, 2009. Instigated by the Thai
                Ministry of Justice in collaboration with the UNODC, and led by
                international prison reform consultant Tomris Atabay, this
                meeting saw academics and corrections experts, including NGOs
                and handpicked delegations from the USA, UK, Africa, Latin
                America and Japan, striving to introduce a gender dimension to
                the 1955 UN Standard Minimum Rules (SMRs) for the treatment of
                prisoners. Topics discussed – and sometimes passionately debated
                – spanned from general prison management to the needs of special
                groups (such as victims of violence and pregnant inmates), the
                importance of non-custodial measures, and the role of research
                into offences and criminal behavior. The landmark outcome – 70
                rules covering the needs and rights of incarcerated women and
                built with the realities of operational practice in mind – was
                titled “Draft United Nations Rules for the Treatment of Women
                Prisoners and Non-Custodial Measures for Women Offenders”.
              </p>
              <p className="d-none d-lg-block">
                roundtable meeting held at Bangkok’s Golden Tulip Hotel from
                February 2-4, 2009. Instigated by the Thai Ministry of Justice
                in collaboration with the UNODC, and led by international prison
                reform consultant Tomris Atabay, this meeting saw academics and
                corrections experts, including NGOs and handpicked delegations
                from the USA, UK, Africa, Latin America and Japan, striving to
                introduce a gender dimension to the 1955 UN Standard Minimum
                Rules (SMRs) for the treatment of prisoners. Topics discussed –
                and sometimes passionately debated – spanned from general prison
                management to the needs of special groups (such as victims of
                violence and pregnant inmates), the importance of non-custodial
                measures, and the role of research into offences and criminal
                behavior. The landmark outcome – 70 rules covering the needs and
                rights of incarcerated women and built with the realities of
                operational practice in mind – was titled “Draft United Nations
                Rules for the Treatment of Women Prisoners and Non-Custodial
                Measures for Women Offenders”.
              </p>
            </div>
          </Col>
        </Row>
        <div></div>
      </>
    ),
  };

  return (
    <>
      <ContentGreen {...propsContent}></ContentGreen>
    </>
  );
}
