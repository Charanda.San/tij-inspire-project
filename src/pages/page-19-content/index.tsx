import { ImageWiteQuote } from "@/components";
import { IPropsImageWithQuote } from "@/components/ImageWithQuote";
import React from "react";

export default function Page19Content() {
  const dataProps: IPropsImageWithQuote = {
    quote: `“Thailand does not claim
    to be the model for a successful approach to the treatment of female inmates as there is still room for further improvement. The Kamlangjai Project is just one of the examples showing that we can promote practices that take into account the gender sensitivities of female inmates in Thailand, and I firmly believe in the value of demonstrating leadership by being a good model.”`,
    subQuote: (
      <>
        Excerpt from Her Royal Highness Princess Bajrakitiyabha’s speech at the
        61<sup>st</sup> Meeting of the American Society of Criminology, 5 November, 2009
      </>
    ),
    caption: `An inmate walks down a corridor at Chiang Mai Women Correctional Institution. Holistic in scope, the Kamlangjai Project aims to support the mental and physical health of inmates, as well as improve their future employability.`,
    src: "./images/page-19-content/image-19-01.webp",
  };
  return (
    <>
      <ImageWiteQuote {...dataProps} />
    </>
  );
}
