import { ContentImage } from "@/components/Widget";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const Page52Content = () => {
  return (
    <Container
      style={{
        height: "100%",
        backgroundImage: "linear-gradient(180deg, #F3EDDE, #FFFFFF)",
      }}
    >
      <Row className="h-100 px-3">
        <Col
          lg={6}
          className="scroll1 h-100"
          style={{
            backgroundColor: "#b49479",
          }}
        >
          <ContentImage
            title="ACHIEVING SUSTAINABLE DEVELOPMENT GOAL 16"
            paragraph={[
              <>
                <p>
                  Of the 17 Sustainable Development Goals (SDGs), SDG 16 is one
                  of the most cross-cutting and fundamental. Peace and
                  inclusiveness, access to justice for all and effective,
                  accountable and inclusive institutions – together these form
                  some of the most important pillars of modern societies.
                  Moreover, in addition to promoting hard-to-define and closely
                  interconnected concepts such as the rule of law and good
                  governance, SDG 16 also calls for the elimination of many
                  everyday threats, including all forms of violence, child abuse
                  and torture, and organized crime.
                </p>
                <div
                  style={{ color: "#FFFFFF" }}
                  className="d-block mx-auto px-0 px-lg-3"
                >
                  <p
                    className="text-paragraph-nue"
                    style={{
                      fontSize: "14px",
                      color: "white",
                      textAlign: "center",
                    }}
                  >
                    Promote peaceful and inclusive societies for sustainable
                    development, provide access to justice for all and build
                    effective, accountable and inclusive institutions at all
                    levels
                  </p>
                  <div className="d-block ms-auto" style={{ width: "90%" }}>
                    <p
                      className="text-paragraph-nue"
                      style={{
                        fontSize: "12px",
                        color: "white",
                        fontStyle: "italic",
                      }}
                    >
                      – Sustainable Development Goal 16
                    </p>
                  </div>
                </div>

                <p>
                  Various official and unofficial mechanisms have been devised
                  with the intention of quantitatively and periodically
                  reviewing SDG progress, including SDG 16. Most notably, a
                  high-level political forum on sustainable development is
                  convened annually by the United Nations Economic and Social
                  Council, or ECOSOC. Each year, this forum assesses advancement
                  on a number of the SDGs using official indicators established
                  by the Inter-Agency and Expert Group on SDG Indicators.
                </p>
                <p>
                  In its 2019 review, the United Nations high-level forum
                  concluded that “renewed efforts are essential” if SDG 16 is to
                  be achieved. “Advances in ending violence, promoting the rule
                  of law, strengthening institutions and increasing access to
                  justice are uneven and continue to deprive millions of their
                  security, rights and opportunities, and undermine the delivery
                  of public services and broader economic development,” the
                  report stated.
                </p>
                <p className="d-none d-lg-block">
                  The SDG 16 target most closely aligned with the mission of the
                  Thailand Institute of Justice – 16.3: ‘Promote the rule of law
                  at
                </p>
              </>,
              <>
                <p className="d-block d-lg-none">
                  The SDG 16 target most closely aligned with the mission of the
                  Thailand Institute of Justice – 16.3: ‘Promote the rule of law
                  at the national and international levels and ensure equal
                  access to justice for all’ – is measured by the United Nations
                  using two official indicators. The first indicator is the
                  proportion of victims of violence in the previous 12 months
                  who reported their victimization, and the second is the
                  proportion of the overall prison population who are
                  unsentenced detainees.
                </p>
                <p className="d-none d-lg-block">
                  the national and international levels and ensure equal access
                  to justice for all’ – is measured by the United Nations using
                  two official indicators. The first indicator is the proportion
                  of victims of violence in the previous 12 months who reported
                  their victimization, and the second is the proportion of the
                  overall prison population who are unsentenced detainees.
                </p>
                <p>
                  However, there are alternative yet complementary benchmarks
                  that draw upon broader nongovernmental data. In particular,
                  the World Justice Project’s Rule of Law index offers a more
                  holistic picture of the accessibility, affordability,
                  impartiality and effectiveness of civil justice systems, and
                  of the capacity of criminal justice systems to impartially
                  investigate and adjudicate criminal offenses. It uses more
                  than 130,000 household and expert surveys to measure how the
                  rule of law is experienced and perceived in practical,
                  everyday situations by the general public around the world.
                  Performance is assessed using 44 indicators across eight
                  categories: constraints on government powers, absence of
                  corruption, open government, fundamental rights, order and
                  security, regulatory enforcement, civil justice, and criminal
                  justice.
                </p>
                <p>
                  Thailand, which has been riven by more than a decade of
                  political conflict, lack of policy continuity and corruption,
                  ranks 71<sup>st</sup> out of 122 countries. Looking at the
                  2020 figures, its overall rule of law score has remained
                  steady over the past five years. However, it scores below the
                  global average across all eight categories and ranks tenth out
                  of 15 countries in the East Asia and Pacific region.
                </p>
                <p>
                  Fundamental freedoms, civil justice and criminal justice are,
                  according to the index, Thailand’s weak points. A lack of
                  freedom from arbitrary interference is one issue; unreasonable
                  delay and ineffective enforcement within the civil justice
                  system is another. And it fares even worse for criminal
                  justice, due in large part to a correctional system that ranks
                  86<sup>th</sup> out of 122 countries in terms of effectiveness
                  at reducing criminal behavior, and a ranking of 108th for
                  impartiality.
                </p>
                <p>
                  According to the UN’s 2019 ranking of SDG progress, Thailand’s
                  performance on SDG 16 falls in the “moderately improving”
                  category, despite “significant challenges.” However, the WJP’s
                  more forensic rule of law survey paints a somewhat starker
                  picture – of a country faltering in its attempts to update its
                  laws and legal institutions in accordance with international
                  norms and sustainability principles.
                </p>
              </>,
            ]}
          />
        </Col>
        <Col lg={6} className="h-100 pt-2 scroll1">
          <img
            src="./images/page-52-content/page-52.webp"
            alt=""
            style={{ height: "auto", objectFit: "inherit" }}
          />
        </Col>
      </Row>
    </Container>
  );
};

export default Page52Content;
