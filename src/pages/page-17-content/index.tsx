import React, { ReactNode } from "react";
import { ContentGreen } from "@/components";
import { IPropsContent } from "@/components/ContentGreen";
import { ContentImage } from "src/components/Widget";
import { Col, Row } from "react-bootstrap";
export default function Page17Content() {
  const Text = ({ children }: { children: ReactNode }) => (
    <p className="text-roboto-condensed">{children}</p>
  );

  const paragraphText = [
    <>
      <p>
        particularly with respect to prisoners' understanding of their
        legal rights and due process. She also expressed a desire to support
        them in the long-term. “Her Royal Highness's thinking at that time was
        that she doesn't want to only improve lives within the facility, but
        also to develop skills that will benefit prisoners when they leave it,”
        she says.
      </p>
      <p>
        Realizing these plans began with baby steps: the Kamlangjai Project's
        first initiative brought small yet significant improvements to the lives
        of incarcerated mothers and their newborn children. Shortly after the
        visit in October 2006, the Thai Red Cross College of Nursing was
        approached and asked to give training
      </p>
    </>,
    <p>
      courses to expectant mothers, new mothers and volunteer nannies from
      within the prison population. Launched in February 2007, these practical
      classes sought to foster healthy pregnancies and strengthen mother-child
      bonds, as well as give selected inmates the hands-on knowledge they needed
      to take care of infants. Similar courses were soon rolled out to other
      prisons in Ayutthaya, Chiang Mai and Phitsanulok provinces. Childcare
      centers wellstocked with milk, diapers and other baby supplies were
      also opened. In many cases, these centers offered, for the first time, a
      peaceful retreat from the regimes of prison life, a clean and comfortable
      space where pregnant women could rest and mothers could nurse their
      infants.
    </p>,
  ];
  const propsContent: IPropsContent = {
    title: (
      <div>
        The Kamlangjai <br /> Project
      </div>
    ),
    textDividerLeft: "inspire",
    textDividerRight: "THE RIGHTS OF WOMEN",
    character:'o',
    subTitle:
      "f all the social spheres in which women are vulnerable to abuse and neglect, prisons are among the most challenging. Hidden behind bars, away from public view, the plight of female inmates — the worldwide numbers of which are rising at a rate faster than male inmates — is easily ignored or forgotten. The stigma and fear surrounding crime and criminals helps to keep things that way. And only reinforcing this reality are the male-centered infrastructure and operational practices, the architecture and procedures that prevail in penal systems globally. Simply put, the vast majority of prisons around the world were originally designed to facilitate the secure and safe imprisonment of men, not women.",
    paragraphs: [
      <p>
        However, change has come over the last 15 years. And much of it has been
        instigated from within Thailand — a country with one of the highest
        rates of female incarceration in the world, largely on account of its
        strict drug laws. Occurring incrementally and across different strata of
        civil society and international contexts, this human rights-based
        evolution within the field of criminal justice was partially driven by
        the charity and diplomatic work of a Thai princess, Her Royal Highness
        Princess Bajrakitiyabha. However, it is not only a story about her but
        also a story about a collective effort that began with the Thai
        government taking the lead by bringing tangible improvements to the
        lives of its female prisoners.
      </p>,
      <p>
        Her personal involvement with the issue can be traced back to a visit to
        Bangkok Central Women Correctional Institution in July 2001. As her
        entourage was led on a tour through the utilitarian corridors of one of
        Thailand's biggest female prisons, an incarcerated mother tentatively
        approached. During the conversation that ensued, one fact stood out
        above the details of this prisoner's crime and sentence: her deep
        concern for her newborn baby. Basic childcare supplies, such as milk and
        nappies, were in short supply. And it was clear that mother and child
        lacked the love, warmth and opportunities afforded by a close family
        unit. Upon parting, Her Royal Highness left determined to help somehow.
      </p>,
      <p>
        Five years went by, during which she completed her studies at Cornell
        Law School, but upon returning to Thailand, she began offering that help
        through the auspices of a charity set up using a donation of her own
        money. The Kamlangjai — or Inspire — Project, as she named it, was
        officially established on 31<sup>st</sup> October 2006, on the occasion of her
        second visit to the Central Women Correctional Institution. As with her
        first visit, she had friendly interactions and frank conversations with
        both female prisoners and prison staff, and struck a tone of solidarity
        and support that served to encourage them. The facility's Executive
        Director at the time, Angkhaneung Lepnak, recalls her being especially
        eager to apply her knowledge in law,
      </p>,
    ],
    componentRightSide: (
      <>
        <Row className="gx-2">
          <Col xs={12} sm={7} lg={7}>
            <Row className="gx-2 gy-2">
              <Col xs={12} sm={4} lg={4} className="position-relative">
                <p
                  style={{ color: "#4D4E4D" }}
                  className="text-roboto-condensed position-text-image"
                >
                  Princess Bajrakitiyabha holds the baby of an inmate at
                  Phitsanulok Provincial Prison, 2008.
                </p>
              </Col>
              <Col xs={12} sm={8} lg={8}>
                <div
                  style={{
                    // height: 220,
                    // backgroundImage: `url('./images/page-17-content/image-17-01.webp')`,
                  }}
                  className="image-bg"
                >
                  <img src="/images/page-17-content/image-17-01.webp" alt="" />
                </div>
              </Col>
              {paragraphText.map((text) => (
                <Col xs={12} sm={6} lg={6}>
                  <Text children={text} />
                </Col>
              ))}
            </Row>
          </Col>
          <Col xs={12} sm={5} lg={5}>
            <div style={{ backgroundColor: "#B49479", }}>
              <ContentImage
                title="INSPIRED DESIGN"
                img="./images/page-17-content/image-17-3.webp"
                paragraph={[
                  {
                    col: 12,
                    content: (
                      <div
                        className="text-paragraph"
                        
                      >
                        <p>
                          When Princess Bajrakitiyabha established the
                          Kamlangjai Project — a charity that originally focused
                          on providing healthcare and health education to
                          pregnant inmates and incarcerated mothers — careful
                          consideration was given to its English title. The
                          obvious choice would have been a translation of the
                          Thai word kamlangjai: either the literal 'power'
                          (kamlang) of 'the heart' (jai), or a close equivalent,
                          such as 'support' or 'encouragement'. But eventually,
                          after consulting with a famous Thai writer, the more
                          rousing 'Inspire' was chosen.
                        </p>
                        <p>
                          Today, both titles articulate different facets of the
                          project's philosophy: 'Kamlangjai' reflects the care
                          and moral support it extends towards others, while
                          'Inspire' communicates its belief that everybody —
                          even people in prison — can be inspired and inspiring.
                        </p>
                        <p>
                          The Kamlangjai Project logo is a simple drawing of a
                          windborne flower complete with a flowing stalk that
                          forms a thick, curlicued heart shape. Originally hand
                          sketched on a tissue by a lady who worked in the
                          laundry room of a hotel, this graceful illustration
                          was selected by Her Royal Highness because it
                          effectively communicates something else the charity
                          tries to impress upon its many beneficiaries — the
                          resilience and strength of the heart amid tough
                          conditions, its capacity to weather all sorts of
                          proverbial storms.
                        </p>
                      </div>
                    ),
                  },
                ]}
              />
            </div>
          </Col>
        </Row>
      </>
    ),
  };

  return (
    <>
      <ContentGreen {...propsContent}></ContentGreen>
    </>
  );
}
