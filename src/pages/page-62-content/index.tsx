import { ContainerOrange } from "@/components";
import React from "react";
import { Col, Row } from "react-bootstrap";
import "./page-62-content.css";

export default function Page62Content() {
  return (
    <ContainerOrange>
      <Row className="gy-3">
        <Col xs={12} lg={6} sm={12}>
          <Row className="gy-3 gx-3">
            <Col lg={6} >
              <Row className="gy-3 gx-3">
                <Col lg={12} >
                  <img
                    src="./images/page-62-content/image-62-01.webp"
                    className="mb-3 mb-lg-3"
                  />
                </Col>
                <Col lg={12}>
                  <div>
                    <hr className="divider-short-orange" />
                    <p className="text-caption-image-nue">
                      The skills taught cover everything from modern farming
                      practices and hospitality to local traditions, such as tok
                      sen (hammer massage). In line with the Kamlangjai
                      Project’s aims, these activities inspire and bolster
                      prisoners as they approach the end of their incarceration.
                    </p>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col lg={6}>
              <Row className="gy-3 gx-2">
                <Col lg={12} sm={6}>
                  <img src="./images/page-62-content/image-62-02.webp" alt="" />
                </Col>
                <Col lg={12} sm={6}>
                  <img src="./images/page-62-content/image-62-03.webp" alt="" />
                </Col>
              </Row>
            </Col>
          </Row>
         
        </Col>
        <Col xs={12} lg={6} sm={12}>
        <img
              src="./images/page-62-content/image-62-04.webp"
              className="image-page h-auto"
            />
        </Col>
      </Row>
    </ContainerOrange>
  );
}
