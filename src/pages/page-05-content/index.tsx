import { CaptionImagePosition } from "@/components";
import React from "react";
import { Col,  Row } from "react-bootstrap";

const Page02 = () => {
  const captionsDesktop: any[] = [
    {
      position: "",
      caption:
        "Princess Bajrakitiyabha visits a class at Chiang Mai Women Correctional Institution, 2018.",
    },
  ];
  const captionsMobile: any[] = [
    {
      position: "",
      caption:
        "Princess Bajrakitiyabha visits a class at Chiang Mai Women Correctional Institution, 2018.",
    },
  ];
  return (
    <Row className="chapterStyle gx-3 gy-3">
      <Col className="h-100" lg={9} xs={12} sm={12}>
        <img src="./images/page-02-content/20150417.webp" alt="" />
      </Col>
      <Col xxl={3} xs={12} sm={12}>
        <div className="d-block d-lg-flex flex flex-lg-column-reverse h-100 px-lg-2 px-3">
         <div className="d-none d-lg-block d-sm-none">
         <CaptionImagePosition captions={captionsDesktop} position="top" />
         </div>
         <div className="d-block d-lg-none ">
         <CaptionImagePosition captions={captionsMobile} position="top" />
         </div>
          <p className="text-caption-image ">
            Her Royal Highness Princess Bajrakitiyabha <br />
            Narendiradebyavati Kromluang <br />
            Rajasarinisiribajara Mahavajrarajadhita.
          </p>
        </div>
       
      </Col>
    
    </Row>
  );
};

export default Page02;
