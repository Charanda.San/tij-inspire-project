import { ContainerLight, QuoteImage } from "@/components";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page42Content() {
  const Quote = () => (
    <QuoteImage
      fontSub="Neue Haas Unica"
      subQuote="
Keynote address by Her Royal Highness Princess Bajrakitiyabha at the United Nations Expert Group Meeting on Integrating Sport into Youth Crime Prevention
and Criminal Justice Strategies, Bangkok, 16 December, 2019"
      quote={`“Various pieces of evidence recognize the role of sport as a useful intervention tool to address the challenges of youth crime. Sport not only fulfills children’s right to development, leisure and play, but it can also play a significant role in boosting the development of cognitive and behavioral processes in children and youth.”`}
    ></QuoteImage>
  );
  return (
    <ContainerLight dividerCaption="THE RIGHTS OF CHILDREN">
      <Row className="p-0">
        <Col xl={6} sm={12} xs={12} className="scroll1">
          <Row className="gx-2 gy-2">
            <Col xs={12} sm={4} lg={2} className="d-none d-lg-block d-sm-block">
              <p className="text-caption-image-nue ">
                At Ban Pranee Juvenile Vocational Training Center for Girls,
                volleyball encourages teamwork and helps relieve stress. Sports
                are increasingly seen as an effective method of therapy and
                rehabilitation for young offenders.
              </p>
            </Col>
            <Col xs={12} sm={8} lg={10}>
              <div>
                <img
                  className="image-page"
                  src="./images/page-42-content/image-42-01.webp"
                />
              </div>
            </Col>
            <Col xs={12} sm={4} lg={2} className="d-block d-lg-none d-sm-none">
              <p className="text-caption-image-nue ">
                At Ban Pranee Juvenile Vocational Training Center for Girls,
                volleyball encourages teamwork and helps relieve stress. Sports
                are increasingly seen as an effective method of therapy and
                rehabilitation for young offenders.
              </p>
            </Col>
            <Col xs={12} sm={6} lg={4}>
              <p className="text-paragraph-nue">
                of commitment called the Doha Declaration. Coming at a time when
                the United Nations and Member States were trying to define the
                parameters of a “transformative post-2015 development agenda”,
                this action-oriented document emphasized – among other crime
                prevention and criminal justice-related themes – the profound
                importance of education for all children and the promotion of a
                culture of lawfulness. It also explicitly stressed “the
                fundamental role of youth participation in crime prevention
                efforts.”
              </p>
              <p className="text-paragraph-nue">
                In light of the Doha Declaration, the United Nations Office on
                Drugs and Crime (UNODC) – a UN body the Thai government and TIJ
                have forged a close working relationship with – went on to
                launch a global youth crime prevention initiative that “builds
                on the power of sports as a tool for peace.” Through a donation
                by the government of Qatar and partnerships with governments,
                sports organizations and civil society, it sought to conduct
                national and regional youth-oriented initiatives that “promote
                civic values and disseminate the benefits of sport in keeping
                youth from becoming involved in crime and violence.”
              </p>
              <p className="text-paragraph-nue">
                Inspired first and foremost by her first-hand experiences, but
                also conscious of the
              </p>
              <div className="d-block d-lg-none">
                <img
                  className="image-page"
                  src="./images/page-42-content/image-42-02.webp"
                />
                <p className="text-caption-image-nue mt-2">
                  Bounce Be Good offers hundreds of at-risk Thai children a
                  sense of purpose through table tennis, soccer and badminton
                  training.
                </p>
              </div>
            </Col>
            <Col xs={12} sm={6} lg={3} className="text-paragraph-nue ">
              <p>
                impetus provided by the Doha Declaration, Princess
                Bajrakitiyabha upgraded her pilot sports initiative to a
                full-fledged sports club in 2016. Since its launch under her
                Royal Patronage, Bounce Be Good (BBG), as it is known, has
                expanded countrywide and taken on many of the traits of
                professional sports clubs, including the harnessing of financial
                sponsorship from major corporations and competitive tournaments.
                Today, it offers hundreds of Thai children a sense of purpose
                through table tennis, soccer and badminton training, and
                scholarships to a handful of the most talented players from
                vulnerable backgrounds – not just child offenders but also
                at-risk youth from slums or villages known for drug trafficking.
                Those selected train under professional coaches and gain a
                school education at the same time, the aim being to have them
                compete whilst also equipping them with essential life skills
              </p>
              <p>
                The best practices of BBG have, meanwhile, been carefully
                leveraged by Her Royal Highness to promote yet another global
                campaign: the mainstreaming of sports within youth crime
                prevention efforts.
              </p>
              <p className=" d-block d-lg-none">
                In May 2019, a Thai diplomatic delegation drafted a resolution
                (2019/16) entitled ‘Integrating Sports into Youth Crime
                Prevention and Criminal Justice Strategies’. Paragraph 11 of
                this resolution, which was unanimously approved at the United
                Nations in Vienna, requested that the UNODC and Thai government
                convene an expert group meeting where relevant stakeholders
                could compile best practices on this nascent subject. This went
                ahead in December 2019 at Bangkok’s Plaza Athenee Hotel, where
                over 40 experts from government, academia, civil society, sports
                organizations and United Nations entities came together to
                exchange experiences and reflect on the use of sport and
                physical activity as a means of reducing youth crime recidivism.
              </p>
              <p className="text-paragraph-nue d-none d-lg-block">
                In May 2019, a Thai diplomatic delegation drafted a resolution
                (2019/16) entitled ‘Integra
              </p>
            </Col>
            <Col xs={12} sm={6} lg={5}>
              <Row className="gx-2 gy-2">
                <Col xs={12} lg={12}>
                  <div>
                    <img
                      className="image-page d-none d-lg-block"
                      src="./images/page-42-content/image-42-02.webp"
                    />
                  </div>
                </Col>
                <Col xs={12} lg={8}>
                  <p className="text-paragraph-nue d-none d-lg-block">
                    ting Sports into Youth Crime Prevention and Criminal Justice
                    Strategies’. Paragraph 11 of this resolution, which was
                    unanimously approved at the United Nations in Vienna,
                    requested that the UNODC and Thai government convene an
                    expert group meeting where relevant stakeholders could
                    compile best practices on this nascent subject. This went
                    ahead in December 2019 at Bangkok’s Plaza Athenee Hotel,
                    where over 40 experts from government, academia, civil
                    society, sports organizations and United Nations entities
                    came together to exchange experiences and reflect on the use
                    of sport and physical activity as a means of reducing youth
                    crime recidivism.
                  </p>
                </Col>
                <Col xs={12} lg={4}>
                  <p className="text-caption-image-nue d-none d-lg-block">
                    Bounce Be Good offers hundreds of at-risk Thai children a
                    sense of purpose through table tennis, soccer and badminton
                    training.
                  </p>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col xl={6} sm={12} xs={12} className="px-lg-4">
          {/* for tablet */}
          <Row className="gy-2 gx-2 d-flex d-lg-none">
            <Col sm={7}>
              <div>
                <img
                  className="image-page"
                  src="./images/page-42-content/image-42-03.webp"
                />
              </div>
              <p className="text-caption-image-nue mt-3">
                Sport is an important part of the curriculum at all of
                Thailand’s Juvenile Vocational Training Centres.
              </p>
            </Col>
            <Col sm={5}>
              <Quote />
            </Col>
          </Row>
          <Row className="gy-2 gx-2 d-none d-lg-flex">
            <Col xl={12} sm={12} xs={12}>
              <div>
                <img
                  className="image-page"
                  src="./images/page-42-content/image-42-03.webp"
                />
              </div>
            </Col>
            <Col xl={3} sm={6} xs={12}>
              <p className="text-caption-image-nue">
                Sport is an important part of the curriculum at all of
                Thailand’s Juvenile Vocational Training Centres.
              </p>
            </Col>
            <Col xl={9} sm={6} xs={12}>
              <Quote />
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerLight>
  );
}
