import { ContainerOrange } from "@/components";
import React from "react";
import { Col, Row } from "react-bootstrap";

const Page18Content = () => {
  return (
    <ContainerOrange>
      <Row className="gx-3 gy-3 h-100">
        <Col lg={6} sx={12} sm={12} className="h-100">
          <Row className="gx-2 gy-3">
            <Col lg={12} className="order-0 order-lg-0">
              <img src="./images/page-18-content/page-18.webp" alt="" className="image-page h-auto" />
            </Col>
            <Col lg={4} sm={6} xs={12} className="order-3 order-lg-1">
              <hr className="divider-short-orange" />
              <p className="text-caption-image-nue mt-2">
                The vocational training at Chanthaburi Provincial Prison covers
                barista skills, baking and traditional crafts such as reed mat
                weaving. Typically, the Kamlangjai Project facilitates training sessions by experts from private companies or institutions,
                and skills are then passed on between prisoners.
              </p>
            </Col>
            <Col lg={4} sm={6} xs={12} className="order-1 order-lg-2">
                <img
                  src="./images/page-18-content/page-18-1.webp"
                  alt=""
                  className="image-page "
                />
            </Col>
            <Col lg={4} sm={6} xs={12} className="order-1 order-lg-3">
                <img
                  src="./images/page-18-content/page-18-2.webp"
                  alt=""
                  className="image-page "
                />
            </Col>
          </Row>
        </Col>
        <Col lg={6} sm={12}>
          <Row>
            <Col lg={12} sx={12}>
              <div className="pb-3">
                <img src="./images/page-18-content/page-18-3.webp" alt="" />
              </div>
            </Col>
            <Col sx={12} sm={5} lg={4}>
              <hr className="divider-short-orange" />
              <p className="text-caption-image-nue  mt-2">
                Near to Chanthaburi, at the Rayong Central Prison, inmates are
                instructed in the teachings of the Buddha, or ‘dhamma’, by a
                monk. Mental health programs have been increasingly integrated
                into Thai prisons.
              </p>
            </Col>
            <Col sx={12} sm={7} lg={8} style={{display: 'flex',justifyContent:'flex-end'}}>
              <div style={{width: '60%',textAlign: 'end'}}>
              <p className="text-roboto-condensed" style={{textAlign: 'end',fontSize:'14px'}}>
                “I consider the Kamlangjai Project a form of moral support.”
              </p>
              <p className="text-roboto-condensed" style={{textAlign: 'end',fontStyle: 'italic'}}>
                – Gift, an inmate who is taking advantage of the offered classes
              </p>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerOrange>
  );
};

export default Page18Content;
