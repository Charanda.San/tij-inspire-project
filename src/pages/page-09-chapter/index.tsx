import React from "react";
import { Chapter } from "src/components";
import { IPropsChapter } from "@/components/Chapter";
export default function Page09Chapter() {
  const data: IPropsChapter = {
    src: "./images/page-09-content/cover-chapter-09.webp",
    title: "CRIMINAL JUSTICE AND THE RULE OF LAW",
    chapterText: "INTRODUCTION",
    content:
      "Thailand’s robust criminal justice system faces an intractable issue: overcrowded prisons. For years, many corrections facilities across the country have been operating in excess of their official operating capacity. It is against this backdrop that Princess Bajrakitiyabha’s diverse law education and broad criminal justice skills were acquired. All of the varied yet complementary strands of her work — her roles as a diplomat, a vocal human rights advocate and a Thai public prosecutor — have been directly informed by the realities of the Thai criminal justice system, as it strives to reform in the face of a growing prison population disproportionately comprised of the vulnerable.",
    descriptionImg: (
      <p>The central courtyard at Chiang Mai Women Correctional Institution.</p>
    ),
  };
  return <Chapter {...data} />;
}
