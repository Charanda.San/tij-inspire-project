import { LayoutText } from "@/components";
import React from "react";
import { Col, Row } from "react-bootstrap";

const Page11Content = () => {
  return (
    <>
      <LayoutText
        divider="On Thailand’s Criminal Justice System"
        leftComponent={
          <div className="h-100">
            <div className="text-divider-content">
              On Thailand’s Criminal Justice System
            </div>
            <div className="mt-2" style={{ width: "95%", height: "95%" }}>
              <img
                src="./images/page-07-content/image-07.webp"
                className="image-page"
              />
            </div>
          </div>
        }
        rightComponent={
          <Row>
            <Col lg={6} xs={12} sm={6}>
              <div className="text-paragraph">
                <p>
                  custodial sentences for drug crimes. This figure places
                  Thailand at the top of the list of countries that imprison
                  women for minor drug-related offences, and flies in the face
                  of research showing that women's pathways to prison are often
                  tied to cumulative structural and cultural disadvantages.
                </p>
                <p>
                  Before Her Royal Highness Princess Bajrakitiyabha took up the
                  cause of Thai criminal justice reform in the late 2000s some
                  drug control and prison initiatives had already achieved a
                  measure of success.
                </p>
                <p>
                  Internationally, Thailand has long been viewed as a model of
                  humane drug crop suppression, on account of its success at
                  stamping out opium cultivation. Back in 1971, the country's
                  Office of Narcotics Control Board was given a mandate to
                  tackle the heroin and opium production that was, at the time,
                  rife among certain hill tribe villages in the mountainous
                  border regions of Thailand's far north. Early crop
                  substitution efforts led, in subsequent decades, to
                  well-funded Alternative Development projects that built
                  positive relationships between farming communities and the
                  state, and gave the former legitimate new means of sustainable
                  income. This achievement boiled down to the establishment of
                  state authority in formerly isolated areas and the extension
                  of incentives to farmers, followed by the creation of a
                  high-risk environment for illicit drug crop cultivation and
                  production.
                </p>
                <p>
                  There had also been attempts to reduce and rehabilitate the
                  prison population. In the early 2000s, for example, strategies
                  and measures included — in addition to the use of Royal
                  Pardons, a tradition stretching back to the ancient kingdoms
                  of Sukhothai and Ayutthaya — the Narcotic Addicts
                  Rehabilitation Act (2002) and the introduction of a
                  correctional boot camp. The former introduced a drug diversion
                  program whereby suspects caught abusing drugs were treated as
                  'patients', not 'offenders' by the Department of Probation,
                  and offered a compulsory drug treatment program instead of
                  prosecution. And the latter, overseen by the Department of
                  Corrections and known as Vivat Polamaung Rachatan, was a
                  four-month rehabilitation program for drug-addicted prisoners,
                  after which they were offered early parole. However, both
                  these initiatives — one providing an alternative to prisons,
                  the other an experiment in prisoner reintegration —
                </p>
              </div>
            </Col>
            <Col lg={6} xs={12} sm={6}>
              <div className="text-paragraph">
                <p>
                  only succeeded at creating a momentary fall in the prison
                  population.
                </p>
                <p>
                  Since then, progress has been made. Under the impetus and
                  guidance of Her Royal Highness Princess Bajrakitiyabha,
                  Thailand has led campaigns for the introduction of
                  internationally accepted new criminal justice norms and
                  practices. Paradoxically, these have thrown into sharp relief
                  the flaws in Thailand's own criminal justice and corrections
                  system, as well as some of its evolving best practices. Many
                  prisons remain overcrowded, but more rights-based policies and
                  protocols have changed them for the better. Meanwhile, debates
                  about the rights of the accused, alternative sentencing,
                  restorative justice and other reforms with the potential to
                  further improve prisoner welfare, reduce overcrowding and
                  promote reintegration are well underway — part of a vociferous
                  national dialogue involving many stakeholders.
                </p>
                <p>
                  Above all, Thailand is paying more attention than ever before
                  to women, children and other vulnerable groups marginalized by
                  the Thai criminal justice system and society at large. These
                  efforts reflect its commitment to providing equal justice for
                  all and to the United Nations 2030 Agenda for Sustainable
                  Development, which claims 'No one will be left behind'.
                </p>
                <p>
                  Standing in the way of the country fulfilling those
                  commitments, however, are the same structural challenges. One
                  issue in this regard is the stigma surrounding drug users. In
                  mainstream society, drug users are often still viewed as
                  criminals, rather than victims or patients. Also exacerbating
                  matters is an often-siloed criminal justice system. Many of
                  its components and sub-components have their own unique — and
                  sometimes conflicting — missions and performance targets, not
                  to mention different perspectives on what constitutes
                  offending behavior, punishment and rehabilitation. With no
                  unified vision, criminal justice practitioners often work at
                  cross-purposes in pursuit of their own notion of justice — be
                  it more arrests, more prosecutions, more leniency or more
                  safeguards. Amid such an environment, change can easily fall
                  by the wayside in favor of the status quo. This is the
                  situation the country finds itself in, even as the arguments
                  for further criminal justice system reforms — especially the
                  more extensive use of non-custodial measures — grow ever
                  louder and clearer.
                </p>
              </div>
            </Col>
          </Row>
        }
      ></LayoutText>
    </>
  );
};

export default Page11Content;
