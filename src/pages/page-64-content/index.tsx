import { CaptionImagePosition, ContainerAfterWorld } from "@/components";
import { ICaptionPosition } from "@/components/CaptionImagePosition";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page64Content() {
  const captions: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: `Her Royal Highness, accompanied by Thai officials, at the CCPCJ in 2009.`,
    },
    {
      caption: (
        <>
          Her Royal Highness attends the 65<sup>th</sup> Session of the United
          Nations General Assembly to officially open the ELFI exhibition
          titled, “Standard Minimum Rules: A New Horizon for Women Prisoners“
          with UN Secretary-General Ban Ki-moon in 2010.
        </>
      ),
      position: "Bottom: ",
    },
  ];

  return (
    <ContainerAfterWorld dividerCaption="AFTERWORD">
      <Row className="py-2 px-2">
        <Col xs={12} sm={12} lg={6} className="scroll1">
          <Row className="text-paragraph">
            <Col xs={12} sm={6} lg={6}>
              <p>
                “Kamlangjai” or “Inspire” project, a program she had launched in
                partnership with the private sector to improve the treatment and
                rehabilitation processes of female prisoners in several Thai
                prisons. In addition, she demonstrated her support for the
                adoption of the Commission’s decision on “Strengthening crime
                prevention and criminal justice responses to violence against
                women and girls”, mandating UNODC to convene an
                intergovernmental group of experts to review and update the
                “Model Strategies and Practical Measures on the Elimination of
                Violence against Women in the Field of Crime Prevention and
                Criminal Justice”, ten years after their adoption by the General
                Assembly, with Thailand acting as its host (CPCJC Dec. 17/1).
              </p>
              <p>
                Encouraged by the positive response on the part of the
                international community to her leadership and activities in
                mainstreaming gender sensitivity into the domestic prison
                system, Her Royal Highness became even more active in 2009 by
                initiating a new project under her patronage called “Enhancing
                Lives of Female Inmates” or “ELFI”. Aimed at developing a new
                set of UN rules on the treatment of women prisoners, the ELFI
                project was featured at a number of important events, such as
                the expert roundtable meeting organized by the Thai government
                in cooperation with UNODC, held in Bangkok at the beginning of
                February, in order to work on the draft rules. Then at the
                Twelfth UN Congress in Salvador de Bahia, the draft updated
                Model Strategies on violence against women were “noted with the
                appreciation”.... for...”further consideration by the
                Commission” in paragraph 6 of the Salvador Declaration which, in
                its paragraph 50, also “welcomed” the draft Bangkok Rules... and
                “recommended their consideration by the Commission as a matter
                of priority for appropriate action”.
              </p>
              <p>
                To complete the process, at the nineteenth session of the
                Commission – held in Vienna in May 2010 – two draft resolutions
                submitted by Thailand and containing in their annexes the two
                new sets of Norms were recommended for action by the General
                Assembly, which formally adopted them on 21 December 1010 as GA
                resolutions 65/228 and 65/229. All in all, at that point, it
                became abundantly clear to me that the crucial role Thailand had
                played in these forums would continue to have an international
                impact in the years to come.
              </p>
              <p className="d-none d-lg-block">
                When the Commission on Crime Prevention and Criminal Justice was
                preparing for the Extended Bureau of its twenty-first session to
                be
              </p>
            </Col>
            <Col xs={12} sm={6} lg={6}>
              <p className="d-block d-lg-none">
                When the Commission on Crime Prevention and Criminal Justice was
                preparing for the Extended Bureau of its twenty-first session to
                be held in April 2012, the Chairmanship of the Commission rotated
                to the Asian Group. With its active leadership role, Thailand
                decided to nominate Her Royal Highness Princess Bajrakitiyabha
                as the Chair of the Commission. The Thai proposal was endorsed
                by the Asian Group, thus resulting in Her Royal Highness being
                elected to the post. From my first encounter with “Princess Pat”
                at the Eleventh Crime Congress in Bangkok back in 2005, little
                did I know that she would one day be at the helm of it all in
                Vienna! In addition to being the Chair, she also had a
                concurrent title of Ambassador to Austria and Permanent
                Representative of Thailand to the United Nations – a post she
                held until 2014.
              </p>
              <p className="d-none d-lg-block">
                held in April 2012, the Chairmanship of the Commission rotated
                to the Asian Group. With its active leadership role, Thailand
                decided to nominate Her Royal Highness Princess Bajrakitiyabha
                as the Chair of the Commission. The Thai proposal was endorsed
                by the Asian Group, thus resulting in Her Royal Highness being
                elected to the post. From my first encounter with “Princess Pat”
                at the Eleventh Crime Congress in Bangkok back in 2005, little
                did I know that she would one day be at the helm of it all in
                Vienna! In addition to being the Chair, she also had a
                concurrent title of Ambassador to Austria and Permanent
                Representative of Thailand to the United Nations – a post she
                held until 2014.
              </p>

              <p
                style={{
                  textAlign: "center",
                  color: "#4d4e4d",
                  lineHeight: 1.3,
                  fontSize: "14px",
                  fontFamily: "Neue Haas Unica",
                }}
                className="text-paragraph py-3 py-lg-1"
              >
                “From my first encounter with ‘Princess Pat’ at the Eleventh
                Crime Congress in Bangkok back in 2005, little did I know that
                she would one day be at the helm of it all in Vienna!”
              </p>
              <p>
                It was during these years in which Her Royal Highness remained
                in Vienna that I had the honor and privilege of interacting more
                often with her and of realizing what a unique and exceptional
                person she is. Always open to dialogue, ready to take up any
                challenge with passion and dedication, while conjugating in an
                admirable way thinking and action, she is extremely generous
                and, above all, absolutely devoted to the UN causes. In a sense,
                she was the most ideal and appropriate Chairperson for the
                twenty-first session of the Crime Prevention and Criminal
                Justice Commission, in addition being the youngest in its
                twenty-one-year history!
              </p>
              <p>
                Surely her chairmanship will be remembered for her stamina and
                diplomatic skills in the two-track negotiations of the draft
                resolution for adoption by the General Assembly on the
                preparations for the Thirteenth Congress to be held in Doha in
                2015, originally submitted by Thailand, Canada and Finland,
                setting up informal consultations and canvassing with all the
                regional groups, in order to reach agreement on its overall
                theme, the substantive agenda items and the workshop topics (GA
                Res. 67/184).
              </p>
              <p>
                Furthermore, the Thai delegation was able to see two other draft
                resolutions co-sponsored with other countries formally approved,
                also
              </p>
            </Col>
          </Row>
        </Col>

        <Col xs={12} sm={12} lg={6} className="scroll1">
          <Row className="gy-2 gx-2">
            <Col xs={12} sm={10} lg={9}>
              <Row className="gy-2 gx-2">
                <Col xs={12} sm={12} lg={12}>
                  <img
                    src="./images/page-64-content/image-64-01.webp"
                    className="image-page"
                  />
                </Col>
                <Col xs={12} className="d-sm-none d-lg-none d-block">
                  <CaptionImagePosition
                    style="monster"
                    captions={captions}
                    position="top"
                  />
                </Col>
                <Col xs={12} sm={6} lg={6}>
                  <img
                    src="./images/page-64-content/image-64-02.webp"
                    className="image-page"
                  />
                </Col>
                <Col xs={12} sm={6} lg={6}>
                  <img
                    src="./images/page-64-content/image-64-03.webp"
                    className="image-page"
                  />
                </Col>
              </Row>
            </Col>

            <Col
              xs={12}
              sm={2}
              lg={3}
              className="position-relative d-none d-lg-block d-sm-block "
            >
              <CaptionImagePosition
                style="monster"
                captions={captions}
                position="bottom"
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerAfterWorld>
  );
}
