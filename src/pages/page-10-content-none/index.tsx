import React from "react";
import { Col, Row } from "react-bootstrap";
import TitleBlockquote, {
  IPropsTitleBlockquote,
} from "@/components/TitleBlockquote";
import { LayoutText, ImageCaption } from "@/components";
import { IPropsImageCaption } from "@/components/ImageCaption";

const Page10Content = () => {
  const propsTitleBlockquote: IPropsTitleBlockquote = {
    title: (
      <>
        On Thailand's Criminal <br /> Justice System
      </>
    ),
    blockquote: `Thailand’s criminal justice system is, in many respects, robust. It
    has more resources in terms of budget and personnel than those in
    most other countries. Its major components – legislation, law
    enforcement, courts and corrections – are long-established and
    populated by skilled practitioners.`,
    paragraph1: (
      <>
        <p>
          In addition to a mature legal system based on elements of both common
          and civil law, the country has an independent judiciary and a
          Department of Corrections that, as the final agency in the criminal
          justice system, strives to operate in accordance with international
          norms and mechanisms, as well as the country’s penal code and
          government policies.
        </p>
        <p>
          Yet this comprehensive system is not without some flaws. In recent
          decades, the Thai courts have consistently received more cases than
          they can decide, leaving growing backlogs of pending cases. However,
          the most pressing and intractable issue regards the corrections system
          – a punitive approach to criminal justice has led to the country
          having the largest prison population and the highest incarceration
          rate among
        </p>
      </>
    ),
    paragraph2: (
      <>
        <p>
          all Association of Southeast Asian Nations (ASEAN) Member States.
          Internationally, it has the sixth largest prison pop- ulation and the
          fifth highest incarceration rate – and the numbers of people
          incarcerated are rising, not falling.
        </p>
        <p>
          Opinions differ between conservatives and progressives on whether this
          situation is desirable or regrettable. But the outcome is
          indisputable: chronic overcrowding is rife within Thailand’s prisons.
          Across the country, the inmate population in many facilities well
          exceeds the official carrying capacity, resulting in well-documented
          issues concerning prisoner health and human rights. Moreover, the vast
          majority of annual prison budgets are spent fulfilling the basic needs
          of prisoners, leaving only a fraction left for rehabilitation and drug
          treatment programs.
        </p>
      </>
    ),
  };
  const propsImageCaption: IPropsImageCaption = {
    image: "./images/page-10-content/image-10-01.webp",
    captionText: `Balancing punishment with
    the principles of justice, the Thai criminal justice system
    is actively exploring ways to reduce the country’s large prison population. At Khao Prik Agriculture Industrial Institution in Nakhon Sri Thammarat province, inmates are provided vocational training in many fields in order, in part,
    to reduce their chances of recidivism.`,
    positionX: "right",
    positionY: "bottom",
  };
  return (
    <>
      <LayoutText
        leftComponent={<TitleBlockquote {...propsTitleBlockquote} />}
        rightComponent={
          <>
            <Row>
              <Col lg={12} xs={12}>
                <ImageCaption {...propsImageCaption} />
              </Col>
            </Row>
            <Row className="mt-3">
              <Col lg={6} sm={6} xs={12}>
                <div className="text-paragraph">
                  This quandary is deep-rooted. Driving these figures is a
                  long-standing ‘zero tolerance’ approach to drug control that
                  centers upon the imposition of harsh sentences for drug
                  producers and suppliers (those possessing above a designated
                  amount), from compulsory detention to life imprisonment and
                  the death penalty. This ongoing war on drugs – launched in
                  2003, with the aim of achieving a still elusive drug-free
                  Thailand – has led to a prison population that is overwhelm-
                  ingly made up of non-violent drug offenders. For decades, the
                  majority of these offenders have been apprehended in relation
                  to methamphetamine, either in pill (yaba) or crystal (yaa ice)
                  form. Typically, this cheap and highly addictive drug – a Cat-
                  egory 1 narcotic – is not produced in Thailand, but smuggled
                  by transnational crime networks, over remote mountainous
                </div>
              </Col>
              <Col lg={6} sm={6} xs={12}>
                <div className="text-paragraph">
                  terrains and porous borders, from neighbouring countries where
                  law enforcement agencies and countermeasures tend to be
                  weaker.
                  <br />
                  <br />
                  This is a situation that, despite growing reform-minded
                  rhetoric, mounting drug policy debate and the recent decriminalization of marijuana for medicinal use, shows no signs of
                  abating. About two out of every three people incarcerated in
                  Thai jails were arrested for drug-related offences, and about
                  a quarter of these are users or small-time dealers (i.e.
                  people who sell small amounts of drugs to fund their own drug
                  use), not large-scale drug producers or traffickers. And women
                  are disproportionately impacted by Thailand’s severe drug
                  policies. As of 2020, up to 84% of female prisoners were
                  serving
                </div>
              </Col>
            </Row>
          </>
        }
      />
    </>
  );
};

export default Page10Content;
