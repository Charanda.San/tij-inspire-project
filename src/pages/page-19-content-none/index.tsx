import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const Page19Content = () => {
  return (
    <Container
      style={{ backgroundColor: "#FFFFFF" }}
      className="h-100 position-relative"
    >
      <div className="w-100 d-flex justify-content-between position-absolute px-3 start-0">
        <h3
          className="text-letter-header mb-5 mt-3"
          style={{ width: "calc(100% - 200px)", color: "#000000" }}
        >
          inspire
        </h3>
        <h3 className="text-letter-header mb-5 mt-3 d-inline-block">
          THE RIGHTS OF WOMEN
        </h3>
      </div>
      {/* <Row>
        <Col lg={10}>
          <h3 className="text-letter-header mb-5 mt-3">inspire</h3>
        </Col>
        <Col lg={"auto"}>
          <h3 className="text-letter-header mb-5 mt-3">THE RIGHTS OF WOMEN</h3>
        </Col>
      </Row> */}
      <Row className="h-100">
        <Col
          lg={6}
          className="h-100"
          style={{ paddingTop: "100px", backgroundColor: "#BBBBB0" }}
        >
          <h1>The Build-up to the Bangkok Rules</h1>
          <Row className="px-3">
            <Col xs={12} xxl={8}>
              <p className="text-param">
                Over the years, Her Royal Highness Princess Bajrakitiyabha's
                promotion of the rights of the vulnerable has consistently been
                advanced on both the domestic and diplomatic fronts. The
                multipolar yet complementary nature of her work first became
                apparent in early 2008, when she decided to present the progress
                of her small-scale yet impactful Kamlangjai Project at a United
                Nations meeting on crime prevention and criminal justice. The
                positive reactions garnered here spurred her to start a campaign
                to rally support for the first ever set of global standards for
                the treatment of female offenders. Inextricably linked with her
                hands-on humanitarian work at home, yet also part of a much
                bigger, global effort involving many international bodies and
                stakeholders, this two-year campaign culminated with the UN's
                adoption of what are today known as the Bangkok Rules.
              </p>
            </Col>
          </Row>
          <Row
            className="px-3 overflow-auto"
            style={{ height: "calc(100vh - 670px)" }}
          >
            <Col lg={4}>
              <p className="text-param">
                The UN event at which Her Royal Highness showcased the
                Kamlangjai Project in April 2008 - the 17<sup>th</sup> Session of the
                Commission on Crime Prevention and Criminal Justice (CCPCJ) in
                Vienna - did not mark her first foray into the challenging and
                complex world of multilateralism. Previously, while serving as
                First Secretary at the Permanent Mission of Thailand in New York
                in 2005, she had represented Thailand at the 60<sup>th</sup> United Nations
                General Assembly. She also played a key role in drafting and
                negotiating an important follow-up resolution to the Eleventh
                United Nations Congress on Crime Prevention and Criminal
              </p>
            </Col>
            <Col lg={4}>
              <div className="text-param">
                <p>
                  Justice in Bangkok in April 2005. Known as the Bangkok
                  Declaration, this resolution (60/177) - which built on the
                  momentum of the congress by calling for concerted action on a
                  range of criminal justice policy areas, including 'the
                  adequacy of standards and norms in relation to prison
                  management and prisoners' - marked the symbolic moment when
                  Thailand began taking more initiative on criminal
                  justice-related UN matters.
                </p>
                <p>
                  These were notable diplomatic achievements on the part of
                  Princess Bajrakitiyabha. However, something even more striking
                  happened at
                </p>
              </div>
            </Col>
            <Col lg={4}>
              <div className="text-param">
                <p>
                  the 17<sup>th</sup> CCPCJ session: her hands-on work in Thai prisons
                  began to trigger global reform. On the sidelines of this
                  five-day event, Her Royal Highness hosted an exhibition
                  introducing the Kamlangjai Project as a case study. In both
                  her opening statement and keynote speech, she also spoke
                  passionately about its achievements to date, namely the
                  tangible headway made vis-à-vis mainstreaming gender
                  sensitivity within the Thai prison system. The feedback from
                  those present was encouraging. While the Kamlangjai Project
                  had been created out of her belief that there was an
                  anachronistic gender gap in the Thai penal system, the
                </p>
              </div>
            </Col>
          </Row>
        </Col>
        <Col lg={6} className="h-100 px-3" style={{ paddingTop: "100px" }}>
          <Row className="pb-3">
            <Col lg={9} className="pb-3 pb-lg-0">
              <img src="./images/page-19-content/page-19.webp" alt="" />
            </Col>
            <Col lg={3} className="ps-lg-0 d-flex flex-lg-column-reverse">
              <div className="text-side-img">
                <p>
                  <strong>Top:</strong> Princess Bajrakitiyabha cuts the ribbon
                  at the Kamlangjai Project exhibition held on the sidelines of
                  the 17<sup>th</sup> Session of the Commission on Crime Prevention and
                  Criminal Justice in Vienna, Austria, April 2008.
                </p>
                <p className="mb-0">
                  <strong>Bottom:</strong> A copy of the draft resolution under
                  the consideration of the 65<sup>th</sup> session of UN General Assembly,
                  Third Committee, Agenda Item 105, in which the Bangkok Rules
                  were annexed and subsequently adopted. The signatures are
                  those of the Thai delegation, including Princess
                  Bajrakitiyabha.
                </p>
              </div>
            </Col>
          </Row>
          <Row
            style={{ height: "calc(100vh - 660px)" }}
            className="overflow-auto"
          >
            <Col lg={4}>
              <img src="./images/page-19-content/page-19-1.webp" alt="" style={{
                height:'auto',
                objectFit:'inherit'
              }}/>
            </Col>
            <Col lg={4}>
              <div className="text-param">
                <p>
                  empathetic reactions and alliances forged in Vienna indicated
                  that the issue was widespread.
                </p>
                <p>
                  Emboldened by the success at the 17<sup>th</sup> Session, Her Royal
                  Highness went on to spearhead a two-year advocacy campaign,
                  known as 'Enhancing the Lives of Female Inmates', or ELFI.
                  This aimed to rally international support for the development
                  of the first set of UN standards for the treatment of women
                  offenders - often by linking them to broad human rights
                  arguments. Steadily, meeting by meeting, speech by speech,
                  this tactical approach gained traction. As Vongthep
                  Arthakaivalvatee, a Special Advisor to the Thailand Institute
                  of Justice and Former Deputy Secretary-General of ASEAN who
                  was then serving at the Royal Thai Embassy in Vienna,
                  recounts: “After the 17<sup>th</sup> Session in Vienna, things snowballed
                  to the point where Thailand was now the one moving the agenda.
                  The stars were aligning.”
                </p>
                <p>
                  Essential to the gestation of what became known as the Bangkok
                  Rules was an expert
                </p>
              </div>
            </Col>
            <Col lg={4}>
              <div className="text-param">
                <p>
                  roundtable meeting held at Bangkok's Golden Tulip Hotel from
                  February 2-4, 2009. Instigated by the Thai Ministry of Justice
                  in collaboration with the UNODC, and led by international
                  prison reform consultant Tomris Atabay, this meeting saw
                  academics and corrections experts, including NGOs and
                  handpicked delegations from the USA, UK, Africa, Latin America
                  and Japan, striving to introduce a gender dimension to the
                  1955 UN Standard Minimum Rules (SMRs) for the treatment of
                  prisoners. Topics discussed - and sometimes passionately
                  debated - spanned from general prison management to the needs
                  of special groups (such as victims of violence and pregnant
                  inmates), the importance of non-custodial measures, and the
                  role of research into offences and criminal behavior. The
                  landmark outcome - 70 rules covering the needs and rights of
                  incarcerated women and built with the realities of operational
                  practice in mind - was titled “Draft United Nations Rules for
                  the Treatment of Women Prisoners and Non-Cus- todial Measures
                  for Women Offenders”.
                </p>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
};

export default Page19Content;
