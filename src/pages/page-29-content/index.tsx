import { ContainerAfterWorld, QuoteImage } from "@/components";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import "./page-29-content.css";

const Page29Content = () => {
  return (
    <ContainerAfterWorld
      dividerCaption="THE RIGHTS OF WOMEN"
      captionBottom="Putting The Bangkok Rules Into Practice"
    >
      <Row className="gx-2 gy-2 gy-sm-3  ">
        <Col lg={6} className="scroll1">
          <Row className="gx-2 gy-2">
            <Col lg={12}>
              <div>
                <h3
                  style={{
                    fontWeight: 500,
                    color: "#A7755A",
                    fontSize: "16px",
                    fontFamily: "ThaiBev",
                  }}
                >
                  THAILAND'S MODEL PRISON PROGRAM
                </h3>
                <p className="text-paragraph-nue">
                  The Thailand Institute of Justice and Thai Department of
                  Corrections encourage prisons to adopt their policies and
                  practices through its ‘Model Prison’ program, which was
                  introduced in 2015. This voluntary program invites prisons
                  around Thailand to be evaluated using an ‘Index of
                  Implementation’ developed by Penal Reform International. As of
                  2020, 12 Thai prisons – from women’s correctional institutes
                  to large and small women’s units within male prisons – have
                  received the 'Model Prison' designation.
                </p>
              </div>
            </Col>
            <Col lg={12} className="flex-center">
              <img
                src="./images/page-29-content/page-29.webp"
                alt=""
                className="image-page"
              />
            </Col>
            <Col
              lg={12}
              sm={12}
              xs={12}
              className="position-relative mt-3 mt-lg-5 mt-sm-5 d-none d-lg-block"
            >
              <p className="text-divider-bottom position-text-image ">
                Putting The Bangkok Rules Into Practice
              </p>
            </Col>
          </Row>
        </Col>

        <Col lg={6} className="scroll1">
          <Row className="gx-2 gy-2 d-sm-flex align-items-center ">
            <Col lg={6} sm={7} xs={12}>
              <img
                src="./images/page-29-content/page-29-1.webp"
                alt=""
                className="image-page"
              />
            </Col>
            <Col lg={2} sm={6} xs={12} className=" d-block d-sm-none d-lg-none">
              <p
                style={{ textAlign: "left" }}
                className="text-caption-image-nue "
              >
                Chontit Chuenurah, Director of Office for the Bangkok Rules and
                Treatment of Offenders (OBR), talks to wardens at Ayutthaya
                Provincial Prison, 2019.
              </p>
            </Col>
            <Col lg={6} sm={5} xs={12}>
              <QuoteImage
                fontSub="Neue Haas Unica"
                quote={
                  "“The UN Bangkok Rules do not provide guidance on all issues surrounding women in the criminal justice system. However, they are a truly comprehensive set of standards covering not only conditions in detention but also non‐custodial measures and gender‐specific considerations in sentencing.”"
                }
                subQuote={
                  "Excerpt from Penal Reform International’s 2015 briefing ‘Women in criminal justice systems and the added value of the UN Bangkok Rules’"
                }
              />
            </Col>
          </Row>
          <Row className="gx-2 gy-2 d-sm-flex mt-2 ">
            <Col
              lg={10}
              sm={12}
              xs={12}
              className="oder-sm-1 order-1 order-lg-0"
            >
              <Row className="gx-2 text-paragraph-nue ">
                <Col lg={4} sm={12} xs={12}>
                  <p>
                    efforts to absorb them. Most notably, the Kingdom’s 1936
                    Penitentiary Act was, in February 2017, amended so that it
                    is “more consistent with international standards.” It now
                    includes specific clauses on pregnant prisoners and women
                    prisoners with children, among other vulnerable groups.
                  </p>
                  <p>
                    Meanwhile, the TIJ has promoted compliance through regular
                    training of senior correctional staff from across the
                    region. Held annually in Bangkok, its two-week training
                    course focuses on re-educating them through personal
                    stories, practical examples and guest speakers – not merely
                    disseminating key principles. “What we try to introduce”,
                    explains Chontit Chuenurah, Director of Office for the
                    Bangkok Rules and Treatment of Offenders (OBR), “is the
                    notion that most women who come to prison are non-violent
                    and have only committed drugrelated crimes. We try to make
                    them understand these backgrounds and pathways, and to then
                    identify their inmates’ rehabilitative needs.”
                  </p>
                </Col>
                <Col lg={4} sm={12} xs={12}>
                  <p>
                    Through this capacity building drive, relationships with
                    departments of corrections in several countries – including
                    Cambodia and Indonesia – have been built and trust grown.
                    New initiatives have also been launched. “More important
                    than knowledge exchange, we have found, is encouraging
                    correctional staff to have ownership of the Bangkok Rules by
                    creating projects or programs in their own facilities,” she
                    adds.
                  </p>
                  <p>
                    Another way the TIJ encourages prisons to adapt their
                    policies and practices is through ‘Model Prisons’.
                    Introduced in 2015 by the TIJ and Thai Department of
                    Corrections, this voluntary program invites prisons around
                    the Kingdom to be evaluated using an ‘Index of
                    Implementation’ developed by Penal Reform International. The
                    one-year assessment timetable consists of a preliminary
                    visit, a roughly three- to-four-month gap for the facility
                    to enact improvements and collate relevant documentation,
                    and an assessment by a committee made up of TIJ and external
                    agency
                  </p>
                </Col>
                <Col lg={4} sm={12} xs={12}>
                  <p>
                    experts, as well as Department of Corrections
                    representatives. As of writing, 12 Thai prisons – from
                    women’s correctional institutes to large and small women’s
                    units within male prisons – have been designated ‘Model
                    Prisons’.
                  </p>
                  <p>
                    Its effects are both psychological and practical. On a
                    mindset level, the ‘Model Prison’ project allows department
                    of corrections staff from other countries to see how
                    Thailand is practicing what it preaches. Additionally, Model
                    Prisons show corrections staff within Thailand that the
                    Bangkok Rules offer a certain amount of latitude – that they
                    can be put into practice in spite of limitations such as
                    overcrowding or lack of resources. More practically, staff
                    from Model Prisons get access to wider networks, skills
                    building and specialist advisory support, as well as certain
                    capacity building incentives, such as study visits to other
                    prisons both in Thailand and abroad.
                  </p>
                  <p>
                    Today, the program is focused on refining training, forging
                    new relationships and
                  </p>
                </Col>
                <Col lg={12} sm={12} xs={12} className=" d-block d-lg-none d-sm-block">
                  <p  className="text-divider-bottom mt-3 w-100 ">
                    Putting The Bangkok Rules Into Practice
                  </p>
                </Col>
              </Row>
            </Col>
            <Col
              lg={2}
              sm={6}
              xs={12}
              className="oder-sm-0 order-0 order-lg-1 d-none d-sm-block d-lg-block"
            >
              <p
                style={{ textAlign: "left" }}
                className="text-caption-image-nue "
              >
                Chontit Chuenurah, Director of Office for the Bangkok Rules and
                Treatment of Offenders (OBR), talks to wardens at Ayutthaya
                Provincial Prison, 2019.
              </p>
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerAfterWorld>
  );
};

export default Page29Content;
