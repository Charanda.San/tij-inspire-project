import React from "react";
import { ContentGreen, ImageParagraphPage } from "@/components";
import { IPropsContent } from "@/components/ContentGreen";
import { IPropsImageParagraphPage } from "@/components/ImageParagraph";
export default function Page27Content() {
  const propsImageparagraph: IPropsImageParagraphPage = {
    image: "./images/page-27-content/image-27-01.webp",
    paragraphs: [
      <>
        <p>
          Abhisit Vejjajiva and the advocacy of reform minded members of the
          Ministry of Justice led to the latter option being pursued as part of
          a larger vision.
        </p>
        <p className="d-none d-lg-block">
          In 2011, Dr. Kittipong Kittayarak — the current TIJ Executive Director
          — was the permanent secretary of the Ministry of Justice. A reform
          minded law scholar who had extensive experience working with the
          United Nations on crime prevention and criminal justice, he had long
          envisioned to one day create a criminal justice think-tank with a
        </p>
        <p className="d-block d-lg-none">
          In 2011, Dr. Kittipong Kittayarak — the current TIJ Executive Director
          — was the permanent secretary of the Ministry of Justice. A reform
          minded law scholar who had extensive experience working with the
          United Nations on crime prevention and criminal justice, he had long
          envisioned to one day create a criminal justice think-tank with a
          region-wide vision. His aim was for this organization to become a
          member of the United Nations Programme Network of Institutes (UN-PNI).
          After close consultation with the government, it was decided that the
          opportunity afforded by the arrival of the Bangkok Rules should be
          used to create exactly that: a semi-autonomous public organization
          promoting justice reform across Association of Southeast Asian Nations
          (ASEAN) Member States, which were inching towards closer economic
          integration at the time.
        </p>
      </>,
      <p className="d-none d-lg-block">
        region-wide vision. His aim was for this organization to become a member
        of the United Nations Programme Network of Institutes (UN-PNI). After
        close consultation with the government, it was decided that the
        opportunity afforded by the arrival of the Bangkok Rules should be used
        to create exactly that: a semi-autonomous public organization promoting
        justice reform across Association of Southeast Asian Nations (ASEAN)
        Member States, which were inching towards closer economic integration at
        the time.
      </p>,
      <>
        <p>
          Founded on 13 June, 2011, the TIJ began life with a broad, reform
          minded remit that respectfully channeled the spirit of Her Royal
          Highness’s holistic criminal justice thinking. With her full
          consultation and blessing, it commenced exploring wider enhancements
          to the justice system while at the same time proceeding at pace with
          the promotion and implementation of the Bangkok Rules.
        </p>
        <p>
          To kickstart that process, regional expert group meetings and prison
          evaluations were conducted in the early years. These allowed
        </p>
      </>,
    ],
    caption: `Prison staff from different countries visit Rayong Central Prison, as part of the TIJ’s Bangkok Rules Training Programme, 2018.`,
  };
  const propsContent: IPropsContent = {
    title: (
      <div>
        PUTTING THE BANGKOK <br /> RULES INTO PRACTICE
      </div>
    ),
    textDividerLeft: "inspire",
    textDividerRight: "THE RIGHTS OF WOMEN",
    character: "w",
    subTitle: `hen the Bangkok Rules – the first international standards for the treatment of female offenders – were adopted by the United Nations General Assembly in December 2010, it was a proud moment for
    the Thai Kingdom. And understandably so: while much of the substance of the Bangkok Rules had been drafted by an international team of corrections experts, the campaign that led to their acceptance was headed by a Thai delegation that included Her Royal Highness Princess Bajrakitiyabha. Not only that, the momentum of the Bangkok Rules had, as Her Royal Highness said a few years later, a “catalytic impact on subsequent movement in the UN”. Within Thailand, meanwhile, just as prisons worldwide were beginning to grapple with the day-to-day ramifications of the Bangkok Rules, that same momentum also triggered the creation of the Thailand Institute of Justice (TIJ) – a regional bulwark against structural injustices operating at the nexus of the rule of law and sustainable development.`,
    paragraphs: [
      <p>
        Back in mid-2010, just as the recently drafted Bangkok Rules were on
        their legislative path to the General Assembly, the international
        community began to tackle something that many working within the
        criminal corrections field believed was also long overdue: the review of
        the general prison population regulations the Bangkok Rules were
        designed to supplement. Known as the 1955 UN Standard Minimum Rules for
        the Treatment of Prisoners, these were revised between 2010 and 2014 and
        renamed – with much fanfare – the Nelson Mandela Rules in 2015.
      </p>,
      <p>
        During that period, the momentum of the Bangkok Rules was also harnessed
        within Thailand. For the Kingdom’s Ministry of Justice and Ministry of
        Foreign Affairs, pride about Thailand’s key role in their drafting,
        review and adoption quickly gave way to the realization that the real
        work had just begun. Collectively, they were aware that the Bangkok
        Rules – a piece of soft law – did not guarantee that the situation on
        the ground in women’s prisons would actually improve. An intensification
        of efforts – some kind of promotion or capacity building – was needed.
      </p>,
      <>
        <p>
          “Success was not just having the Bangkok Rules. Success was having
          them implemented,” as Wisit Wisitsora-at, the current Permanent
          Secretary of Thailand’s Ministry of Justice, puts it. Equally critical
          was the question of Thailand’s future role. Should it now focus on
          introducing and implementing the Bangkok Rules within the Kingdom? Or
          should it be aiming bigger, pushing for their uptake regionally as
          well as domestically?
        </p>
        <p>
          Eventually, the combination of a receptive Thai government headed by
          then Prime Minister
        </p>
      </>,
    ],
    componentRightSide: <ImageParagraphPage {...propsImageparagraph} />,
  };

  return (
    <>
      <ContentGreen {...propsContent}></ContentGreen>
    </>
  );
}
