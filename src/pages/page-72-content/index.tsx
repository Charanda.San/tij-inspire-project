import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import "./page-72-content.css";
import { ContainerLight } from "@/components";

const Page72Content = () => {
  return (
    <ContainerLight dividerCaption="Appendices">
      <>
        <Row className="h-100 px-0 px-lg-3">
          <Col lg={6} className="scroll3">
            <Row>
              <Col lg={6}>
                <div className="d-flex flex-column justify-content-between h-100">
                  <img
                    src="./images/page-72-content/TIJ Logo 2018-01.png"
                    alt=""
                    style={{
                      width: "70%",
                      height: "auto",
                      objectFit: "inherit",
                    }}
                    className="d-block mx-auto"
                  />
                  <p
                    className="text-normal text-center"
                    style={{ fontFamily: "bookman" }}
                  >
                    www.tijthailand.org
                  </p>
                </div>
              </Col>
              <Col lg={6}>
                <div className="header-about">
                  <h5>ABOUT THE TIJ</h5>
                  <p>“Justice is a matter that concerns everyone.”</p>
                </div>
                <div className="text-param">
                  <p>
                    The Thailand Institute of Justice (TIJ) is a relatively new
                    player in the field of crime prevention and criminal
                    justice. Established in 2011 with funding from the
                    government of Thailand, the TIJ supports policymakers and
                    practitioners in Thailand and beyond to ensure a fair and
                    effective response to crime and the proper treatment of
                    offenders.
                  </p>
                  <p>
                    Despite on-going reform efforts by criminal justice systems
                    worldwide, meeting the specific needs of the vulnerable –
                    mostly women and children – remains a challenge for many
                    countries. The TIJ, therefore, places a strong emphasis on
                    capacity building for the effective implementation of the
                    international standards that specifically address the
                    vulnerabilities faced by women and children in criminal
                    justice settings.
                  </p>
                  <p>
                    One example is the 2010 United Nations Rules for the
                    Treatment of Women Prisoners and Non-custodial Measures for
                    Women Offenders or the Bangkok Rules. The rules seek to
                    address the gender-specific needs of women who are
                    imprisoned or subject to non-custodial measures.
                    International standards and norms like the
                  </p>
                </div>
              </Col>
            </Row>
          </Col>
          <Col lg={6} className="scroll3">
            <Row>
              <Col lg={6}>
                <div className="text-param">
                  <p>
                    Bangkok Rules not only provide policy guidance to member
                    states, but also serve as a rich repository of good
                    practices in the field of crime and justice. The TIJ works
                    closely with Member States to collectively enhance capacity
                    and make effective use of these standards and norms.
                    Strategically, most of the TIJ’s programs are in alignment
                    with the United Nations Crime Prevention and Criminal
                    Justice Programme.
                  </p>
                  <p>
                    In this context, the TIJ contributes to, and benefits from,
                    the large pool of knowledge and expertise shared among our
                    international partners within the United Nations Programme.
                    These include the United Nations Office on Drugs and Crime
                    (UNODC) and regional and national organizations comprising
                    the United Nations Programme Network of Institutes or
                    UN-PNI. Currently, the TIJ is an active member of the PNIs
                    and the only one in Southeast Asia – an honor enjoyed since
                    2016.
                  </p>
                  <p>
                    Five years after the adoption of the Bangkok Rules came
                    another milestone that characterizes the trajectory of the
                    TIJ as we fulfill our mandates. The adoption of the 2030
                    Agenda for Sustainable Development in 2015 represents such a
                    pivotal moment. Underlying the new framework is a
                    realization that all sectors responsible for the sustainable
                    development of society are deeply interrelated.
                  </p>
                  <p className="d-none d-lg-block">
                    In this sense, our justice systems cannot operate in
                    isolation. We need to pursue multi-sectoral cooperation and
                    cross-sectoral linkages. When justice works with public
                    health,
                  </p>
                  <p className="d-block d-lg-none">
                    In this sense, our justice systems cannot operate in
                    isolation. We need to pursue multi-sectoral cooperation and
                    cross-sectoral linkages. When justice works with public
                    health, education, poverty reduction, or job creation, for
                    instance, we stand a better chance to succeed, not only in
                    what we do but also in helping others achieve their goals.
                  </p>
                </div>
              </Col>
              <Col lg={6}>
                <div className="text-param">
                  <p className="d-none d-lg-block">
                    education, poverty reduction, or job creation, for instance,
                    we stand a better chance to succeed, not only in what we do
                    but also in helping others achieve their goals.
                  </p>
                  <p>
                    The TIJ, therefore, began to expand our work beyond
                    traditional areas of justice and the rule of law. Our
                    partnership network now includes more players such as
                    private enterprises, civil society, academia and youth. We
                    invite and encourage our partners to seek “justice” in
                    everyday life themselves.
                  </p>
                  <p>
                    The new direction rests on the premise that diversity is
                    essential for finding the best solutions to societal
                    problems. We hope to bring together criminal justice
                    practitioners and those working in other fields so that – as
                    a whole – we benefit from distinct yet complementary
                    perspectives.
                  </p>
                  <p>
                    The aim is to ensure that justice and the justice sector
                    remain relevant in the contemporary era of technological
                    disruption. Under the new agenda, the TIJ strives to create,
                    nurture, and expand the “justice-minded networks” in
                    society. We believe these networks are the key to securing a
                    sustainable and inclusive path towards a more innovative and
                    peoplecentered solution to persisting challenges in the
                    fields of crime and justice.
                  </p>
                  <p>
                    The new chapter for the TIJ is upon us. We commit ourselves
                    to the principles and values of justice but remain open to
                    new possibilities. We embark upon a journey where we embrace
                    change and translate new ideas into actions and solutions.
                  </p>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </>
    </ContainerLight>
  );
};

export default Page72Content;
