import { ContainerOrange, QuoteParagraph } from "@/components";
import { Inpractice } from "@/components/Widget";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const Page44Content = () => {
  return (
    <ContainerOrange>
      <Row className="h-100 gy-3">
        <Col lg={5}>
          <Row className="gx-2 gy-2 gy-lg-2">
            <Col lg={12} className="">
              <img src="./images/page-44-content/page-44.webp"className="image-page" alt="" />
            </Col>
            <Col lg={6} sm={6} xs={12} >
              <img src="./images/page-44-content/page-44-1.webp" alt="" />
            </Col>
            <Col lg={6} sm={6} xs={12} >
              <img src="./images/page-44-content/page-44-2.webp" alt="" />
            </Col>
          </Row>
        </Col>
        <Col lg={7} className="h-100 scroll1">
          <Inpractice
            title="BOUNCING BACK"
            paragraph={[
              <>
                <p>
                  <span className="first-letter-orange">I</span>n a crowded
                  corner of the BBG Princess Cup 2020 tournament, a 20-year-old
                  boy has an epic table tennis battle on his hands. After a fast
                  serve by his opponent, Somkid slams the ball back across the
                  net with a confident forehand stroke, his feet BOUNCING BACK
                  skipping beneath his slight frame as he does so. A long,
                  rapid-fire rally ensues, until he sees his chance and blasts
                  the ball into the top left corner, just beyond his opponent’s
                  reach. Onlookers burst into applause, but there is no
                  celebration on Somkid’s part – only a determined look in his
                  eyes.
                </p>
                <p>
                  “I feel every kind of emotion when I’m out there playing table
                  tennis: frustration, anger, elation,” he says later, on the
                  sidelines of this highly competitive two-day tournament held
                  in the event halls at Bangkok’s Samyan Mitrtown shopping mall.
                  “Sport helps me control my feelings and challenges me to be
                  better.”
                </p>
                <p>
                  Conceived and created by HRH Princess Bajrakitiyabha, the
                  Bounce Be Good (BBG) sports club initiative began in June 2015
                  when an agreement between the private office handling her many
                  initiatives and two state-run juvenile centers was completed.
                  The ‘Young Table Tennis Way Of Change’ project, as it was then
                  known, offered sports training to delinquent boys and girls
                  held at the Ban Muthita and Ban Pranee facilities, but soon
                  grew to become something bigger.
                </p>
                <p>
                  “BBG’s aims are two-fold,” explains BBG President Ekaphop
                  Detkriangkraisorn, a friend of HRH who himself got into
                  trouble when he was younger, and feels a sense of solidarity
                  with young offenders because of his experiences. “We recruit
                  potential athletes to compete in competitions. And secondly,
                  we provide a home outside of juvenile centers for a small
                  number of children – give them a new environment that nurtures
                  their potential by changing their attitude and training them.”
                </p>
                <p>
                  Since its inception, BBG’s crime prevention through sports
                  philosophy has expanded to include child victims of crime, and
                  children from impoverished areas with high crime rates.
                  However, most of its players – 474 and rising – hail from
                  Thailand’s youth detention centers, which, as of 2019, housed
                  nearly 37,000 boys and girls for crimes ranging from attempted
                  murder and assault to theft and drug dealing.
                </p>
                <p className="d-none d-lg-block">
                  Sixteen of the most outstanding players – including Somkid, a
                  former detainee of a juvenile center in Nakhon Pathom province
                  – are currently
                </p>
                <p className="d-block d-lg-none">
                  Sixteen of the most outstanding players – including Somkid, a
                  former detainee of a juvenile center in Nakhon Pathom province
                  are currently in the BBG Academy located in Bangkok’s Thonburi
                  area. Here they follow an intensive, boarding school-like
                  routine that includes exercise and sports training not just
                  table tennis, but also badminton and football. “Some of them
                  didn’t start playing until they were 14 or 15 years old, so
                  the schedule needs to be quite intense,” explains Ekaphop.
                </p>
              </>,
              <>
                <p className="d-none d-lg-block">
                  in the BBG Academy located in Bangkok’s Thonburi area. Here
                  they follow an intensive, boarding school-like routine that
                  includes exercise and sports training – not just table tennis,
                  but also badminton and football. “Some of them didn’t start
                  playing until they were 14 or 15 years old, so the schedule
                  needs to be quite intense,” explains Ekaphop.
                </p>
                <p>
                  The club supplies their basic needs, including food, clothing
                  and medical care, and a standard education, so that players
                  can pursue other occupations if their sporting career doesn’t
                  take off. Coaches work a lot on attitude and psychology while
                  emphasizing the importance of discipline, self-esteem and
                  teamwork, not only technical skills, or winning. “We try to
                  provide them with a new family so they can grow properly.”
                </p>
                <QuoteParagraph
                  quote="“Sport helps me control my feelings and challenges me to
                    be better.”"
                  subQuote="– Somkid, a former child offender turned table tennis
                    player"
                />
                <p>
                  The hope is that BBG players will be competing at an
                  international level within five years. Yet the underlying
                  motivation is clearly broader. “We want to set an example to
                  other Thai children who are limited in terms of education,
                  economics and family background, and at high risk of making
                  mistakes,” adds Ekaphop. “We want to show them they can rise
                  to the top through sport. Instead of them feeling apathetic
                  about their lives and turning to crime, we want them to feel
                  proud of themselves.”
                </p>
                <p>
                  As for Somkid, he has his long-term sights set on something
                  even bigger than winning the BBG Princess Cup 2020: becoming a
                  valuable member of society. His BBG scholarship will support
                  him through university, where he plans to study sports
                  science, hone his table tennis skills and start sharing his
                  passion with others.
                </p>
                <p>
                  “I hope to play for the national team,” he says, “but if I
                  don’t, I want to be a coach and help produce other players.
                  Whatever happens, I’ve gained a future from this.”
                </p>
              </>,
            ]}
          />
        </Col>
      </Row>
    </ContainerOrange>
  );
};

export default Page44Content;
