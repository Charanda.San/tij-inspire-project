import { CaptionImagePosition, ContainerOrange } from "@/components";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import "./page-33-content.css";
import { ICaptionPosition } from "@/components/CaptionImagePosition";

const Page33Content = () => {
  const captions: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption:
        "Carcel sources natural and sustainable materials, including local Thai silk, and reinvests profits with a view to expanding its operations in the future.",
    },
    {
      position: "Bottom: ",
      caption:
        "As it requires years to master, silk weaving is taught to prisoners serving long sentences.",
    },
  ];
  
  return (
    <ContainerOrange>
      <Row className="h-100 px-lg-2 px-0">
        <Col lg={6} className="h-100 scroll1">
          <Row className="gx-3 gy-3">
            <Col lg={2} className="px-2 order-lg-1 order-2">
              <div className="d-flex flex-end flex-lg-column-reverse h-100">
                <p className="text-description-img mb-lg-0">
                  Danish fashion label Carcel has pioneered model employment
                  practices at the prison in partnership with the Department of
                  Corrections and Kamlangjai Project. The 14 employees work six
                  hours a day, five days a week, and are paid a fair wage in
                  line with international norms.
                </p>
              </div>
            </Col>
            <Col lg={10} className="order-lg-2 order-1">
              <img src="./images/page-33-content/page-33.webp" alt="" />
            </Col>
          </Row>
          <Row className="gx-3 gy-3 mt-lg-1">
            <Col lg={6}>
              <div className="text-param">
                <p>
                  Along a steel-barred corridor on an upper level, just beyond
                  the computer room and a library stocked with books about law,
                  Buddhism and the late King Bhumibol's Sufficiency Economy
                  Philosophy, sits the facility's ‘Happy Centre'. With its
                  off-white walls reminiscent of a Lisu hill-tribe hut, this
                  multi-purpose room is a calming escape where prisoners
                  suffering from stress or depression engage in therapeutic
                  daily activities. These range from dhamma teaching,
                  meditation, yoga and mindfulness classes to expressive art
                  therapies such as watercolor painting and poetry. Prison
                  wardens assess participants using a simple questionnaire,
                  while a counselor comes on weekends to meet those who need to
                  talk through their issue.
                </p>
                <p>
                  Back in the workshops, the routine offers a real chance at
                  long-term development. Prisoners gain purpose, structure,
                  income — and, in many cases, hope. “Working here, I feel I can
                  work outside in the future...I've gained that skill,” says Au,
                  who works at both the massage and cooking sections at the
                  tourist attraction in Chiang Mai's Old City. As her 32-year
                  sentence for drugs, which was commuted to 14 years by a Royal
                  Pardon, draws to a close, she feels emboldened by her
                  experiences.
                </p>
              </div>
            </Col>
            <Col lg={6} className="mt-0 mt-lg-3">
              <div className="text-param">
                <p>
                  “I have two plans,” she says, confidently. “The first is
                  working at Leela Thai: a massage shop chain run by a former
                  prison director. It's a good job with good income. The second
                  is going to work in a laundry shop with my cousin.” During the
                  early years of her sentence, which were served in Rayong
                  Provincial Prison, she felt demoralized and directionless.
                  “When I arrived, they didn't have this vocational program — 10
                  years ago there weren't many in female wings,” she says, “but
                  things are different now. I feel very good about the
                  situation, this time in my life. I have options now.”
                </p>
                <div style={{ width: "80%" }} className="d-block mx-auto">
                  <p className="text-center" style={{ fontSize: "18px",fontFamily:'Neue Haas Unica' }}>
                    “I feel very good about the situation, this time in my life.
                    I have options now.”
                  </p>
                  <div className="d-block ms-auto" style={{ width: "70%" }}>
                    <p className="text-normal fst-italic">
                      - Au, who is finishing a 14-year sentence for drug
                      offences
                    </p>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Col>
        <Col lg={6} className="h-100 scroll1">
          <Row className="gx-2 gy-2">
            <Col lg={12}>
              <img src="./images/page-33-content/page-33-1.webp" alt="" />
            </Col>
            <Col lg={12} className="order-2 order-lg-1 order-sm-1">
              <Row className="gx-2 gy-2">
                <Col xs={12} sm={6} lg={5} className="order-1 order-lg-first order-sm-first" >
                  <img src="./images/page-33-content/page-33-2.webp" alt=""  className="h-auto"/>
                </Col>
                <Col xs={12} sm={6} lg={5} className="order-2 order-lg-1 order-sm-1">
                  <img src="./images/page-33-content/page-33-3.webp" alt="" className="h-auto" />
                </Col>
                <Col xs={12} sm={8} lg={2} className="order-0 order-lg-last order-sm-last">
                  <hr className="divider-short-orange " />
                  <CaptionImagePosition style="monster" captions={captions} position="top" />
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerOrange>
  );
};

export default Page33Content;
