import { ContainerOrange } from "@/components";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page61Content() {
  return (
    <ContainerOrange>
      <Row className="gy-2">
        <Col xs={12} lg={6} sm={12}>
          <Row className="gy-2">
            <Col lg={12} sm={12} xs={12}>
              <img
                src="./images/page-61-content/image-61-01.webp"
                className="image-page"
              />
            </Col>
            <Col lg={12} sm={12} xs={12}>
              <div className="my-2">
                <hr className="divider-short-orange" />
              </div>
              <p className="text-caption-image-nue">
                A working farm and tourist attraction, Doi Hang Temporary Prison
                benefits both male inmates nearing the end of their sentences
                and the general public. Visitors’ prejudices and misconceptions
                are countered by close interactions across the different zones
                of the picturesque site. The prisoners, meanwhile, learn new
                skills and self-sufficiency.
              </p>
            </Col>
        
          </Row>
        </Col>
        <Col xs={12} lg={6} sm={12}>
              <Row className="gy-3">
                <Col lg={12} sm={12} xs={12}>
                  <Row className="gx-3 gy-3">
                    <Col lg={8} sm={8} xs={12}>
                      <img
                        src="./images/page-61-content/image-61-02.webp"
                        className="image-page"
                      />
                    </Col>
                    <Col lg={4} sm={4} xs={12}>
                      <img
                        src="./images/page-61-content/image-61-03.webp"
                        className="image-page"
                      />
                    </Col>
                  </Row>
                </Col>
                <Col lg={12} sm={12} xs={12}>
                  <img
                    src="./images/page-61-content/image-61-04.webp"
                    className="image-page"
                  />
                </Col>
              </Row>
            </Col>
  
      </Row>
    </ContainerOrange>
  );
}
