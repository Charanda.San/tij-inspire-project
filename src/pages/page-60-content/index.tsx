import { ContainerOrange, QuoteParagraph } from "@/components";
import { Inpractice } from "@/components/Widget";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import "./page-60-content.css";

const Page60Content = () => {
  return (
    <ContainerOrange>
      <Row className="gy-sm-3 gy-3">
        <Col lg={5} className="scroll1">
          <div className="wrapper-image-group-60 d-lg-grid d-block d-sm-none">
            <div className="img-1 ">
              <img
                src="./images/page-60-content/page-60.webp"
                className="mb-3 mb-lg-3"
                alt=""
              />
              <div className="d-none d-lg-block">
                <h3 className="divider-short-orange" />
                <p className="text-caption-image-nue">
                  Once they’ve been assigned to different zones of Doi Hang’s
                  grounds, inmates spend their days tending vegetables or
                  providing services to visitors.
                </p>
              </div>
            </div>
            <img
              src="./images/page-60-content/page-60-1.webp"
              className="img-2 mb-3 mb-lg-0"
              style={{ objectFit: "inherit", height: "auto" }}
              alt=""
            />
            <img
              src="./images/page-60-content/page-60-2.webp"
              className="img-3 mb-3 mb-lg-0"
              style={{ objectFit: "inherit", height: "auto" }}
              alt=""
            />
               <div className="d-block d-lg-none">
                <h3 className="divider-short-orange" />
                <p className="text-caption-image-nue">
                  Once they’ve been assigned to different zones of Doi Hang’s
                  grounds, inmates spend their days tending vegetables or
                  providing services to visitors.
                </p>
              </div>
          </div>
          <Row className="d-sm-flex d-none d-lg-none gy-sm-3">
            <Col sm={7}>
              <img
                src="./images/page-60-content/page-60.webp"
                className="mb-3 mb-lg-3"
                alt=""
              />
            </Col>
            <Col sm={5}>
              <Row className="gy-sm-3">
                <Col sm={12}>
                  <img
                    src="./images/page-60-content/page-60-1.webp"
                    className="img-2 mb-3 mb-lg-0"
                    alt=""
                  />
                </Col>
                <Col sm={12}>
                  <img
                    src="./images/page-60-content/page-60-2.webp"
                    className="img-3 mb-3 mb-lg-0"
                    alt=""
                  />
                </Col>
              </Row>
            </Col>
            <Col sm={6}>
              <div>
                <h3 className="divider-short-orange" />
                <p className="text-caption-image-nue">
                  Once they’ve been assigned to different zones of Doi Hang’s
                  grounds, inmates spend their days tending vegetables or
                  providing services to visitors.
                </p>
              </div>
            </Col>
          </Row>
        </Col>
        <Col lg={7} className="scroll1">
          <Inpractice
            title="FACING THE FUTURE AT DOI HANG TEMPORARY PRISON"
            paragraph={[
              <>
                <p>
                  <span className="first-letter-orange">I</span>t’s early
                  morning at Doi Hang Temporary Prison and, across the four
                  zones of the 22-rai facility, 100 men are hard at work. Some
                  scrape away the bark of trees in search of raw rubber sap,
                  which drips slowly into wooden cups, ready to be sold at the
                  local market. Others gather eggs from the chicken coops, check
                  on the shed filled with rows of mushroom spores, or feed the
                  resident ponies.
                </p>
                <p>
                  Golf, a 45-year-old father of five serving a five-year prison
                  sentence for drug and gun possession, is also up and about.
                  After a quick shower each morning, he gets dressed and then
                  sets off along a dirt track flanked by the rubber tree
                  plantation on one side, and fields filled with well-tended
                  rows of vegetables on the other. Then, after walking the short
                  distance to his destination, he starts his day by checking on
                  the progress of his grapes, then watering and feeding them
                  with natural fertilizer.
                </p>
                <p>
                  “It is not an easy job,” he explains. “Some of the three grape
                  varieties we grow are difficult to take care of, and I need to
                  make sure they grow all year round – no matter how harsh or
                  hot the climate.”
                </p>
                <p>
                  Located in northern Thailand’s Chiang Rai province, Doi Hang
                  is one of six open prisons across the country that offer farm
                  training and life coaching to male prisoners who are nearing
                  release, yet vulnerable to the lures of drug use and dealing.
                  Operated by Thailand’s Department of Corrections and Princess
                  Bajrakitiyabha’s Kamlangjai Project, its practices are an
                  experimental fusion of Alternative Development – an
                  internationally feted drug control approach that centers upon
                  reducing illicit crop cultivation among rural communities –
                  with the practical life lessons of the late King Bhumibol
                  Adulyadej’s Sufficiency Economy Philosophy (SEP). Through
                  group activities, counseling and collaborations with
                  community, commercial and governmental organizations, inmates
                  are taught to live self-sufficiently and happily amid nature
                  over several months, predominantly using SEP and the land and
                  water management principles of the late King.
                </p>
                <p>
                  Seven months ago, Golf was moved here from Chiang Rai Central
                  Prison, a barbed wire-ringed facility with a total population
                  of around 5,000 male and female inmates. Though only less than
                  two kilometers away, life here is profoundly different, he
                  explains. Since arriving, he’s not only been assigned grape
                  growing duties, but also learnt how to make his own natural
                  fertilizer, shampoo, liquid soap and insect repellent. “The
                  training I’ve been given has helped me learn how to rely on
                  myself more,” he says. He also now gets to see his family more
                  often – and on an informal, rather than strictly supervised,
                  basis. “My kids visit me every week,” he says
                </p>
                <p>
                  The benefits of Doi Hang are also enjoyed by the general
                  public. In addition to being an open prison-cum-working farm,
                  it is also a popular local tourist attraction complete with
                  neatly manicured paths and
                </p>
              </>,
              <>
                <p>
                  gardens, a coffee shop offering refreshments and a relaxing
                  treehouse area. During opening hours, visitors can take a tour
                  of the lush, expansive grounds and interact with prisoners,
                  then sit down to enjoy the views over a cup of fresh coffee or
                  slice of cake. They can even buy packets of tea made from
                  mixtures of dried northern herbs.
                </p>
                <p>
                  Much of the training is culturally specific to the region,
                  such as the rare forms of traditional Lanna Thai massage
                  available. Betel leaf, charcoal fire and hammer tapping
                  massages are given in a room flanked by signs advertising
                  their perceived benefits, both physical and mental. “We have
                  to pray to the spirits as we do it,” explains Dum, an inmate
                  trained in tok sen – a loud yet sacred form of massage that
                  entails gently tapping along the length of the receiver’s
                  ligaments using a small hammer and wooden pick. He and his
                  five co-workers were taught by a monk from Chiang Rai’s Doi
                  Ong temple, and now practice their skills on willing members
                  of the public. “I find it fun and it makes me proud and happy
                  when the customer tells me they feel better,” he adds. “Most
                  of all, I feel being here has made me calmer.”
                </p>
                <div className="my-5">
                  <QuoteParagraph
                    quote={
                      "“The objective is to have prisoners adapt themselves to a lifestyle that resembles that of the local community.”"
                    }
                    subQuote="– Pawat Polawat, Doi Hang Temporary Prison’s Director"
                  />
                </div>
                {/* <div style={{ width: "80%" }} className="d-block mx-auto">
                  <p className="text-center" style={{ fontSize: "14px" }}>
                    “The objective is to have prisoners adapt themselves to a
                    lifestyle that resembles that of the local community.”
                  </p>
                  <div className="d-block ms-auto" style={{ width: "60%" }}>
                    <p className="text-center fst-italic">
                      – Pawat Polawat, Doi Hang Temporary Prison’s Director
                    </p>
                  </div>
                </div> */}
                <p>
                  Back out in the fields, crops ranging from rice to morning
                  glory, sweetcorn, garlic, broccoli and giant pumpkins are
                  grown organically year-round. But Doi Hang’s overarching
                  purpose is to ensure that something else takes root among the
                  residents. “The objective is to have prisoners adapt
                  themselves to a lifestyle that resembles that of the local
                  community,” explains Doi Hang Temporary Prison Chief Pawat
                  Polawat. In his experience, many inmates feel ambivalent about
                  their impending release, as they are uncertain whether society
                  will accept them or not. By removing the barriers – both
                  physical and psychological – that stand between prisoners and
                  the public, Doi Hang strives to give them the confidence they
                  need to face the future positively. “Open prisons help them
                  understand that they can get along with outsiders and can
                  contribute something of value to the real world.” Currently,
                  data showing the recidivism rate of former Doi Hang inmates is
                  not yet available. However, several success stories, including
                  the opening of a local coffee bar and a noodle shop by a
                  couple of them, show anecdotally that the practices here –
                  teaching life skills and self-sufficiency – put lasting social
                  reintegration within reach.
                </p>
              </>,
            ]}
          />
        </Col>
      </Row>
    </ContainerOrange>
  );
};

export default Page60Content;
