import React from "react";
import { ContentGreen, ImageParagraphPage } from "@/components";
import { IPropsContent } from "@/components/ContentGreen";
import { IPropsImageParagraphPage } from "@/components/ImageParagraph";
export default function Page41Content() {
  const propsImageParagraph: IPropsImageParagraphPage = {
    image: "./images/page-41-content/image-41-01.webp",
    positionCaption: "right",
    paragraphs: [
      <p>
        facilities across Thailand. These include the Ban Kanchanapisek
        Vocational Juvenile Training Centre for Boys, which successfully
        practices a progressive philosophy that pivots around elements of
        restorative justice and a non-punitive regime that treats those in its
        care as equals, not criminals. On visits here, and elsewhere, she spoke
        with both children and staff, gleaned information about their day-to-day
        lives – including the important role that sports plays in it – and their
        thoughts on how things could be improved for them and the criminal
        justice system at large.
      </p>,
      <p>
        In 2014, these experiences motivated her to begin a sports-related Royal
        Project known as ‘Young Table Tennis Way of Change’. Underpinned by a
        belief in sports’ rehabilitative potential, this initiative began by
        focusing on upgrading the quality and availa bility of sports within
        Thailand’s Juvenile Training Centers. This was done by garnering support
        from sports associations and the private sector. Then, she hit upon the
        idea of providing support not only within the facilities but also
        downstream in society, by providing proper housing and counseling for
      </p>,
      <>
        <p>
          those child offenders who wanted to continue training upon their
          release. This bold step was triggered by her realization that many of
          the most promising sportsmen and women benefiting from the initiative
          had no homes to return to or were at high risk of reoffending.
        </p>
        <p>
          Unfolding in the background during this time were some dovetailing
          international developments. One of the outcomes of the 13<sup>th</sup> United
          Nations Congress on Crime Prevention and Criminal Justice, held in
          Doha, Qatar in April 2015, was a statement
        </p>
      </>,
    ],
    caption: `Boys practice badminton in the BBG Sports Club Training Centre at Ban Karuna Juvenile Centre.`,
  };
  const propsContent: IPropsContent = {
    title: (
      <div>
        Sports & Youth Crime <br /> Prevention
      </div>
    ),
    textDividerLeft: "inspire",
    textDividerRight: "THE RIGHTS OF CHILDREN",
    character: "i",
    subTitle: `
    n recent years, Princess Bajrakitiyabha has been actively promoting the use of sport as a means of preventing youth crime. What began in 2014 as a drive to improve sports facilities within Thailand’s Juvenile Training
    Centers has since evolved into a nationwide sports club, Bounce Be Good, that today offers sports training and scholarships to vulnerable Thai children. Meanwhile, she has been using her diplomatic acumen to spearhead the campaign for a new set of United Nations guidelines that will, if adopted, offer those working in child protection and corrections evidence-based strategies for how to use sport as a youth crime prevention tool.`,
    paragraphs: [
      <>
        <p>
          Underpinning this work is a stark reality: as ASEAN’s youth population
          grows, so, too, does the number of children in conflict with the law.
          And while the dynamics – the types of crime committed, the minimum age
          of criminal responsibility, the levels of incarceration and the use of
          non-custodial measures – vary between Southeast Asian nations, all
          governments harbor a desire to see youth offenders re-enter and make a
          contribution to society.
        </p>
        <p>
          This is often easier said than done. In addition to the many
          structural and socioeconomic drivers of youth crime, there are deep rooted issues surrounding reintegration and recidivism. “The
          perception these kids have of themselves is the main problem,”
          explains Marut Kaewin, Psychologist at the Juvenil
        </p>
      </>,
      <>
        <p>
          Vocational Training Centre in the northern Thai province of Chiang
          Mai. For many, the problem begins at a young age, with them having a
          hard time concentrating at school or getting bad grades and adopting a
          self-defeating mentality. “Many don’t have any belief in their
          capacity to achieve anything. They don’t want to get a normal job
          because they feel no one will accept them, and then they collapse into
          this mindset of ‘Why bother?’ After that it’s very, very easy to just
          turn to drugs or crime.”
        </p>
        <p>
          Wider society often perpetuates this vicious cycle. Negative
          stereotypes and attitudes about youth offenders, as well as mistrust
          in the effectiveness of the correctional system to rehabilitate them,
          means that they are often marginalized in spite of their best efforts.
        </p>
      </>,
      <>
        <p>
          And attempts by justice systems to practice positive reinforcement and
          build self-esteem among children are sometimes not enough. “Often,
          when they go back into a community or environment that is not
          understanding,” he says, “they will then again be faced with the same
          kind of rejection or misunderstanding, and so their self-esteem goes
          down once again.”
        </p>
        <p>
          It is within this context that another facet of Princess
          Bajrakitiyabha’s work has emerged: the use of sports as a tool for
          preventing youth crime and delinquency.
        </p>
        <p>
          She has long known the importance of building resilience among at-risk
          youth. Over the years, she has paid many visits to child corrections
        </p>
      </>,
    ],
    componentRightSide: <ImageParagraphPage {...propsImageParagraph} />,
  };

  return (
    <>
      <ContentGreen {...propsContent}></ContentGreen>
    </>
  );
}
