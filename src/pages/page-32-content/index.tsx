import { ContainerOrange } from "@/components";
import { Inpractice } from "@/components/Widget";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const Page32Content = () => {
  return (
    <ContainerOrange>
      <Row className="gx-3 gy-2 h-100">
        <Col lg={6} sm={12} xs={12}>
          <Row className="gx-2 gy-3 gx-sm-3">
            <Col lg={12} className="order-sm-first order-first order-lg-0">
              <img src="./images/page-32-content/page-32.webp" alt="" />
            </Col>
            <Col lg={2} sm={8} xs={12} className="order-sm-last order-last order-lg-1">
              <hr className="divider-short-orange" />
              <p className="text-caption-image-nue">
                Scenes from inside Chiang Mai Women Correctional Institution:
                inmates line up to buy goods, prepare daily meals and play
                volleyball in the central courtyard.
              </p>
            </Col>
            <Col lg={5} sm={6} xs={12} className="order-lg-2 order-1 order-sm-1">
              <img src="./images/page-32-content/page-32-1.webp" alt="" />
            </Col>
            <Col lg={5} sm={6} xs={12} className=" order-lg-3 order-2 order-sm-2">
              <img src="./images/page-32-content/page-32-2.webp" alt="" />
            </Col>
          </Row>
        </Col>
        <Col lg={6} sm={12} xs={12} >
          <div className="scroll1">
            <Inpractice
              title="VOCATIONAL TRAINING AT CHIANG MAI WOMEN CORRECTIONAL INSTITUTION"
              paragraph={[
                <>
                  <p>
                    <span className="first-letter-orange">A</span>t 6 am, Chiang
                    Mai Women Correctional Institution comes to life. After
                    breakfast, hundreds of female inmates in freshly pressed
                    uniforms file along the semi open-air corridors and to their
                    workstations on the upper levels of this high-security
                    complex. Others follow an officer towards the facility’s
                    main gates and out into the civilian world, where they will
                    do a day’s work before returning in the early evening.
                  </p>
                  <p>
                    Rule 46 of the Bangkok Rules states: “Prison authorities
                    shall design and implement comprehensive pre- and
                    post-release reintegration programs which take into account
                    the gender-specific needs of women.” And this ‘Model Prison’
                    – one of 12 prisons in Thailand that implements the Bangkok
                    Rules to a high standard – closely observes this rule with a
                    vocational training program that strives to offer over 2,000
                    inmates financial remuneration and practical skills.
                  </p>
                  <p>
                    At the prison’s traditional Thai massage zone, groups of
                    tourists who have undergone the security protocols –
                    deposited their belongings in lockers and had a body frisk
                    search – arrive twice a day. Priced at 500 baht, the
                    two-hour session they enjoy at the hands of trainee
                    masseuses, who keep 50% of the income, takes place in a
                    softly lit and soothingly-scented room with rows of massage
                    mats.
                  </p>
                  <p>
                    Further along the same corridor,14 inmates equipped with
                    tape measures and scissors, or seated at sewing machines,
                    are busy working wonders with lengths of pure Thai silk.
                    Selected from over 100 skilled applicants, they are all
                    salaried employees of Carcel, a Danish fashion label that
                    creates long-lasting and sustainable luxury clothing for
                    sale online and in Europe’s fashion capitals – and in
                    accordance with International Labour Organization standards.
                  </p>
                  <p>
                    “We pay them a wage equal to that of workers on the
                    outside,” explains Carcel’s on-site production manager,
                    whose father was incarcerated at this facility – previously
                    a men’s prison – when he was a child. “The point is to help
                    them save up money so that when they get out they can start
                    a business by themselves, or can send it home to their
                    children.” In addition, ten percent of their total combined
                    salary is donated to the prison’s vocational training
                    activities each month.
                  </p>
                  <p>
                    Also helping the prison adhere to Rule 46 are the silk
                    weaving rooms nearby. On more than 100 clattering wooden
                    looms, beneath whirring ceiling fans, women serving long
                    sentences for crimes ranging from drug dealing to homicide
                    diligently work six hours a day, five days a week, creating
                    Northern Thai, Lanna-style products.
                  </p>
                  <p>
                    The training takes years, but the benefits are more than
                    just financial. “It helps a lot with mindfulness, with
                    learning to be just in the moment
                  </p>
                </>,
                <>
                  <p>
                    in front of you,” says Nong, who is seven years into a
                    25-year sentence. “I’m also surprised at how my creativity
                    has developed. As the years have gone by, I’ve learnt to
                    create my own patterns. I didn’t know I had that skill.”
                  </p>
                  <p>
                    Their dresses, scarves and other handwoven creations are
                    sold at government craft fairs, and also at a shop located a
                    few kilometers away, in one of Chiang Mai city’s most
                    popular tourist attractions: a massage center and restaurant
                    complex run by the Department of Corrections. Tourists flock
                    to this acclaimed social enterprise to lunch on fresh bowls
                    of khao soi noodle soup in the tree-shaded courtyard, or
                    enjoy a traditional Thai massage within a century-old wooden
                    house, all delivered by prisoners who are nearing the end of
                    their sentences and so qualify for supervised day release.
                    Some of them earn 9,000 baht (about US$280) a month.
                  </p>
                  <p>
                    Resulting from collaborations between Thailand’s Ministry of
                    Justice, the Department of Corrections, the Kamlangjai
                    Project, businesses and 59 non-governmental organizations,
                    these training projects fulfil many of the key principles of
                    gender-sensitive rehabilitation programs, as outlined in the
                    Thailand Institute of Justice and Penal Reform
                    International’s guidelines. However, while the activities
                    offered in Chiang Mai are based on market needs,
                    sustainable, good quality and both community and prisoner
                    driven, there remains room for improvement.
                  </p>
                  <p>
                    “One thing we need to develop further is individualized
                    sentence plans,” says prison director Archaree Srisonakhuam.
                    “While there are a lot of vocational and rehabilitative
                    programs in this prison, I feel they could, if managed well,
                    be more substantively incorporated into each person’s plan
                    in a way that more directly addresses their specific
                    problems or needs.”
                  </p>
                  <p>
                    She is confident that this can be done – not least because
                    the private sector is more open to collaborations than a
                    decade ago. “There are a lot of companies that now openly
                    accept ex-prisoners or people with criminal records, and a
                    lot of external agencies that are willing to come in and
                    help out with programs or with classes.” The latter include
                    hotel associations, elderly care centers and chef
                    associations.
                  </p>
                  <p>
                    Mental healthcare at Chiang Mai Women Correctional
                    Institution has also improved since the advent of the
                    Bangkok Rules, which call for individualized,
                    trauma-informed mental health programs, and suicide and
                    self-harm prevention strategies, among other measures. As at
                    many Thai prisons, there is no qualified psychologist or
                    psychiatrist on-site to help inmates reduce stress or ensure
                    mental well-being, so activities are typically used as a
                    form of support instead.
                  </p>
                </>,
              ]}
            />
          </div>
        </Col>
      </Row>
    </ContainerOrange>
  );
};

export default Page32Content;
