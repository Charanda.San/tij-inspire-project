import { CaptionImagePosition, ContainerLight } from "@/components";
import { ICaptionPosition } from "@/components/CaptionImagePosition";
import ImageParagraphPage, {
  IPropsImageParagraphPage,
} from "@/components/ImageParagraph";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page43Content() {
  const propsImageParagraph: IPropsImageParagraphPage = {
    image: "./images/page-43-content/image-43-01.webp",
    positionCaption: "left",
    paragraphs: [
      <>
        <p>
          In her keynote address, Princess Bajrakitiyabha stressed the
          “importance of preventing the involvement of children and youth in
          criminal activities, supporting their development, reducing their
          anti-social and delinquent behavior, and supporting their
          rehabilitation and reintegration into the society through sport.” An
          exhibition displaying the achievements of Bounce Be Good was also
          presented on the expert group meeting’s sidelines.
        </p>
        <p>
          As of writing, the best practices compiled at this meeting are due to
          be reviewed by the United Nations’ various criminal justice forums and
          mechanisms in the near future. These include the 29<sup>th</sup>{" "}
          session of the Commission
        </p>
      </>,

      <>
        <p>
          on Crime Prevention and Criminal Justice in Vienna, and the fourteenth
          Congress on Crime Prevention and Criminal Justice in Kyoto. But if
          successfully endorsed, these new standards will offer actionable
          guidelines and proven strategies for policymakers and practitioners
          looking to use sports as a tool for youth crime prevention. This will
          be in line with the 2030 Agenda for Sustainable Development, which, as
          the UNODC states, “recognizes the contributions of sport to the
          empowerment of young people.”
        </p>
        <p>
          Domestically, youth sports is now embedded within the human capital
          development strand of the government’s 20-year National
        </p>
      </>,
      <>
        <p>
          Strategy. And, meanwhile, BBG is, according to President Ekaphop
          Detkriangkraisorn, already bearing fruit by successfully improving
          participants’ sense of self-worth. “Our kids have begun giving back to
          vulnerable and underprivileged children living in remote areas,” he
          stated at a TIJ panel discussion in December 2019. “I hope that when
          society looks at these children, there will be no question as to why
          we have to help them. Many of our children are university athletes now
          who also do well in school.”
        </p>
      </>,
    ],
    caption: `Princess Bajrakitiyabha delivers her keynote address at the United Nations Expert Group Meeting on Integrating Sport into Youth Crime Prevention and Criminal Justice Strategies at the Plaza Athenee Hotel, Bangkok, December 2019.`,
  };
  const propsImageParagraphMobile: IPropsImageParagraphPage = {
    image: "./images/page-43-content/image-43-01.webp",
    positionCaption: "left",
    paragraphs: [
      <>
        <p>
          In her keynote address, Princess Bajrakitiyabha stressed the
          “importance of preventing the involvement of children and youth in
          criminal activities, supporting their development, reducing their
          anti-social and delinquent behavior, and supporting their
          rehabilitation and reintegration into the society through sport.” An
          exhibition displaying the achievements of Bounce Be Good was also
          presented on the expert group meeting’s sidelines.
        </p>
        <p>
          As of writing, the best practices compiled at this meeting are due to
          be reviewed by the United Nations’ various criminal justice forums and
          mechanisms in the near future. These include the 29<sup>th</sup>{" "}
          session of the Commission on Crime Prevention and Criminal Justice in
          Vienna, and the fourteenth Congress on Crime Prevention and Criminal
          Justice in Kyoto. But if successfully endorsed, these new standards
          will offer actionable guidelines and proven strategies for
          policymakers and practitioners looking to use sports as a tool for
          youth crime prevention. This will be in line with the 2030 Agenda for
          Sustainable Development, which, as the UNODC states, “recognizes the
          contributions of sport to the empowerment of young people.”
        </p>
      </>,

      <>
        <p>
          Domestically, youth sports is now embedded within the human capital
          development strand of the government’s 20-year National Strategy. And,
          meanwhile, BBG is, according to President Ekaphop Detkriangkraisorn,
          already bearing fruit by successfully improving participants’ sense of
          self-worth. “Our kids have begun giving back to vulnerable and
          underprivileged children living in remote areas,” he stated at a TIJ
          panel discussion in December 2019. “I hope that when society looks at
          these children, there will be no question as to why we have to help
          them. Many of our children are university athletes now who also do
          well in school.”
        </p>
      </>,
    ],
    caption: `Princess Bajrakitiyabha delivers her keynote address at the United Nations Expert Group Meeting on Integrating Sport into Youth Crime Prevention and Criminal Justice Strategies at the Plaza Athenee Hotel, Bangkok, December 2019.`,
  };
  const captions: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption:
        "The Bounce Be Good exhibition at the Plaza Athenee Hotel, Bangkok, December 2019.",
    },
    {
      position: "Bottom: ",
      caption:
        "Sport is an important part of the curriculum at all of Thailand’s Juvenile Vocational Training Centres.",
    },
  ];
  return (
    <ContainerLight dividerCaption="THE RIGHTS OF CHILDREN">
      <Row className="p-0">
        <Col xl={6} sm={12} xs={12} className="scroll1">
          <div className="d-none d-sm-none d-lg-grid">
            <ImageParagraphPage {...propsImageParagraph} />
          </div>
          <div className="d-block d-sm-block d-lg-none">
            <ImageParagraphPage {...propsImageParagraphMobile} />
          </div>
        </Col>
        <Col xl={6} sm={12} xs={12}>
          <Row className="p-0 gy-2">
            <Col xs={12} sm={12} lg={12}>
              <div>
                <img
                  src="./images/page-43-content/image-43-02.webp"
                  className="image-page"
                />
              </div>
            </Col>
            <Col xs={12} sm={8} lg={8}>
              <div>
                <img
                  src="./images/page-43-content/image-43-03.webp"
                  className="image-page"
                />
              </div>
            </Col>
            <Col xs={12} sm={4} lg={4} className="position-relative">
              <CaptionImagePosition style="monster" position="top" captions={captions} />
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerLight>
  );
}
