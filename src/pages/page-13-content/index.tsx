import React from "react";
import { Col, Row } from "react-bootstrap";
import TitleBlockquote, {
  IPropsTitleBlockquote,
} from "@/components/TitleBlockquote";
import { LayoutText } from "@/components";
import "./page-13-content.css";
import CaptionImagePosition, {
  ICaptionPosition,
} from "@/components/CaptionImagePosition";

const Page13Content = () => {
  const propsTitleBlockquote: IPropsTitleBlockquote = {
    title: (
      <>
        The Criminal Justice <br /> Background of
        <br />
        HRH Princess Bajrakitiyabha
      </>
    ),
    blockquote: `How did HRH Princess Bajrakitiyabha’s career bring her to the progressive frontier of correctional science and criminology and lead her to advocate for the first international standards for female offender welfare? This story begins years ago with her formative educational and early diplomatic experiences.`,
    paragraph1: (
      <>
        <p>
          A meeting with female inmates in 2001 at Bangkok Central Women
          Correctional Institution is often cited as the epiphany — the moment
          when Her Royal Highness Princess Bajrakitiyabha came to the
          realization that a heavily-burdened Thai prison system could improve
          the way it cared for its most vulnerable inmates. “I learned
          first-hand of the hardships faced by these women, and especially by
          their children, who are innocent, but often deprived of adequate
          family care and opportunities,” she later recounted.
        </p>
        <p>
          This encounter was undeniably formative. It inspired her to launch the
          Kamlangjai Project, a charity program that lends a helping hand to
          vulnerable prisoners and their offspring. However, to single it out as
          the sole reason for her career path would be simplistic, and ignore
          the complexity of the journey that
        </p>
      </>
    ),
    paragraph2: (
      <>
        <p>
          she has embarked upon. In fact, many people and events — from family
          members to mentors, and internships — have contributed to the
          development of Her Royal Highness’s ardently-held beliefs.
        </p>
        <p>
          For example, a rounded education saw her acquiring knowledge and
          experience in a range of criminal justice contexts. Upon completing
          her higher secondary school education at Bangkok’s Chitralada School,
          Her Royal Highness went on to study law at Thailand’s prestigious
          Thammasat University and political science at Sukhothai Thammathirat
          Open University. Then came a position as a legal officer at the Judge
          Advocate General’s Department of the Ministry of Defence, where she
          was responsible for drafting and submitting prosecution cases.
        </p>
      </>
    ),
  };

  const ImageTop = ({ src }: { src: string }) => {
    return <img src={src} alt="" className="image-page" />;
  };
  const imagesTop = [
    "./images/page-13-content/image-top-01.webp",
    "./images/page-13-content/image-top-02.webp",
  ];
  const imagesBottom = [
    "./images/page-13-content/image-bottom-01.webp",
    "./images/page-13-content/image-bottom-02.webp",
  ];

  const caption1 = `Princess Bajrakitiyabha holds
  aloft her Doctor of Juridical Science certification at Cornell Law School’s convocation ceremony, 2005.
  `;
  const caption2 = `Crown Prince Maha Vajiralongkorn awards Princess Bajrakitiyabha with her Honorary Doctorate degree in Justice Administration from Thammasat University, 2010.
  `
  const caption3 =`Her Royal Highness receives an Honorary Doctorate degree from Wuhan University, 2017.`
  const caption4 = `Princess Bajrakitiyabha receives her Honorary Doctor of Law degree from Chicago-Kent College of Law, Illinois Institute of Technology, 2012.`

  const captionsDesktop: ICaptionPosition[] = [
    {
      position: "Top left: ",
      caption: caption1,
    },
    {
      position: "Top right: ",
      caption: caption2,
    },

    {
      position: "Bottom left: ",
      caption: caption3,
    },

    {
      position: "Bottom right: ",
      caption: caption4,
    },
  ];

  const captionsMobile: ICaptionPosition[] = [
    {
      position: "1st: ",
      caption: caption1,
    },
    {
      position: "2nd: ",
      caption: caption2,
    },

    {
      position: "3rd: ",
      caption: caption3,
    },

    {
      position: "4th: ",
      caption: caption4,
    },
  ];
  return (
    <>
      <LayoutText
        leftComponent={<TitleBlockquote {...propsTitleBlockquote} />}
        rightComponent={
          <>
            <Row className="gx-3 gy-2 gy-lg-1">
              {imagesTop.map((image: string) => (
                <Col lg={6} xs={12} sm={6} className="wrapper-image-group-13">
                  <ImageTop src={image} />
                </Col>
              ))}
            </Row>
            <Row className="mt-1 gx-2 gx-lg-2 gy-lg-2 gy-3">
              <Col lg={9} xs={12} sm={8} className="hidden-xs">
                <img
                  style={{ height: "auto" }}
                  src="./images/page-13-content/image-page-13-fix.webp"
                />
              </Col>

              {imagesBottom.map((image: string) => (
                <Col xs={12} className="show-xs">
                  <ImageTop src={image} />
                </Col>
              ))}

              <Col lg={3} xs={12} sm={4}>
                <div className="d-none d-lg-block d-sm-block">
                <CaptionImagePosition captions={captionsDesktop} position="top" />

                </div>
                <div className="d-block d-lg-none d-sm-none">
                <CaptionImagePosition captions={captionsMobile} position="top" />
                </div>
              </Col>
            </Row>
          </>
        }
      />
    </>
  );
};

export default Page13Content;
