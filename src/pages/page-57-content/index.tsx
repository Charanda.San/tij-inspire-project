import { CaptionImagePosition, ContainerLight, QuoteImage } from "@/components";
import { ICaptionPosition } from "@/components/CaptionImagePosition";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page57Content() {
  const caption1 =
    "Princess Bajrakitiyabha surveys the Thai-Myanmar border during a UNODC study visit, 2015.";
  const caption2 = "Princess Srinagarindra with MR Disnadda Diskul.";
  const caption3 =
    "ML Dispanadda Diskul, Chief Executive Officer of the Mae Fah Luang Foundation, speaks during a UNODC study visit to Doi Tung, Northern Thailand, 2015.";

  const captions: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: caption1,
    },
    {
      position: "Opposite page top: ",
      caption: caption2,
    },
    {
      position: "Opposite page bottom: ",
      caption: caption3,
    },
  ];

  const captionsMobile: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: caption1,
    },
    {
      position: (
        <>
          The second bottom:&nbsp;&nbsp;
        </>
      ),
      caption: caption2,
    },
    {
      position: (
        <>
          The third bottom:&nbsp;&nbsp;
        </>
      ),
      caption: caption3,
    },
  ];
  return (
    <ContainerLight dividerCaption="The Rule of Law and Sustainable Development">
      <Row>
        <Col xs={12} sm={12} lg={6} className="scroll1">
          <Row className="p-0 gx-3 gy-2  d-sm-flex align-items-center">
            <Col
              xs={12}
              sm={6}
              lg={5}
              className="mt-3 mt-lg-0 order-1 order-lg-0 order-sm-0"
            >
              <QuoteImage
                fontSub=" Neue Haas Unica"
                quote={`“Through actions, even
by the smallest steps or scale, we can descend from grand rhetoric
to stand firmly on the ground...My point, and very much a personal one, is that we can do both exercises
in thinking and in action. And by taking action, we may come closer to reaching a better understanding
and more balanced appreciation of what the rule of law means, in our own terms.”`}
                subQuote="Her Royal Highness Princess Bajrakitiyabha at the International Conference
on Alternative Development 2 (ICAD2), Bangkok, 23 November, 2015"
              />
            </Col>
            <Col
              xs={12}
              sm={6}
              lg={7}
              className="order-0 order-lg-1 order-sm-1"
            >
              <div>
                <img
                  src="./images/page-57-content/image-57-01.webp"
                  className="image-page"
                />
              </div>
            </Col>
          </Row>
          <Row className="p-0 gx-3 gy-2  mt-2 ">
            <Col
              xs={12}
              sm={12}
              lg={2}
              className="position-relative d-none d-sm-none d-lg-block"
            >
              <CaptionImagePosition style="monster" captions={captions} position="top" />
            </Col>
            <Col
              xs={12}
              sm={10}
              lg={2}
              className="position-relative d-block d-sm-block d-lg-none"
            >
              <CaptionImagePosition style="monster" captions={captionsMobile} position="top" />
            </Col>
            <Col xs={12} sm={12} lg={10}>
              <Row className="p-0 gx-2">
                <Col xs={12} sm={12} lg={4}>
                  <p className="text-paragraph-nue">
                    issues, as well as the life lessons offered by role models,
                    help inmates to develop their social conscience, live
                    ethically and moderately, and to reduce their daily expenses
                    upon release. Experts are also enlisted to create the
                    practical curriculum, which spans basic life skills, such as
                    herbal insecticide and liquid soap-making and animal
                    breeding, water and land farm management, and tangential
                    forms of vocational training, including barista
                    apprenticeships. Communities also benefit from these open
                    prisons. Many of them serve as both study sites and tourist
                    attractions, helping to reduce the social stigma surrounding
                    prisoners while also disseminating the principles of
                    sustainability. Since SEP first began to be applied in these
                    four open prisons in April 2010, the farming project has
                    expanded to around 15 facilities.
                  </p>
                  <p className="text-paragraph-nue d-none d-lg-block">
                    Additionally, the Kamlangjai Project has also begun to offer
                    aftercare to prisoners who have already re-entered society.
                    This includes the recent creation of a halfway house, in the
                    southern Thai province of Narathiwat, that offers
                    accommodation, support and training
                  </p>

                  <p className="text-paragraph-nue d-block d-lg-none">
                    Additionally, the Kamlangjai Project has also begun to offer
                    aftercare to prisoners who have already re-entered society.
                    This includes the recent creation of a halfway house, in the
                    southern Thai province of Narathiwat, that offers
                    accommodation, support and training to former prisoners who
                    are especially vulnerable to falling back into a life of
                    crime.
                  </p>
                </Col>
                <Col xs={12} sm={12} lg={4}>
                  <p className="text-paragraph-nue d-none d-lg-block">
                    to former prisoners who are especially vulnerable to falling
                    back into a life of crime.
                  </p>
                  <p className="text-paragraph-nue text-paragraph-nue">
                    Thailand’s late King Bhumibol Adulyadej also appreciated
                    that socioeconomic problems in underdeveloped regions of the
                    country are far from simple, but often the result of a set
                    of complex, localized issues. For example, His Majesty’s
                    pioneering project in northern Thailand called The Royal
                    Project was inaugurated in the late 1970s to counter illicit
                    crop cultivation by the hill-tribe population, who had
                    become increasingly dependent on exploitative figures in the
                    drug trade for their income and livelihoods. His Majesty
                    understood that, as Princess Bajrakitiyabha articulated in a
                    2017 speech, these issues require “multi-faceted solutions
                    specific to the needs of each region and community” and that
                    “working with a ‘silo mentality’ cannot fully address the
                    development complexities of a nation”. Rather than try to
                    enforce the law only, The Royal Project also provided local
                    communities with alternative crops to grow and sell in the
                    market, an approach that proved successful.
                  </p>
                </Col>
                <Col xs={12} sm={12} lg={4}>
                  <p className="text-paragraph-nue">
                    In 1988, the Doi Tung Development Project was established by
                    King Bhumibol Adulyadej’s mother, Her Royal Highness
                    Princess Srinagarindra. Noting the lessons of The Royal
                    Project, Her Royal Highness Princess Srinagarindra
                    recognized that illicit opium cultivation, drug and human
                    trafficking, and deforestation issues in the area were
                    merely symptoms. The root causes of the problems were
                    poverty, poor health and lack of education. She sent her
                    Private Secretary, Mom Rajawongse Disnadda Diskul, to learn
                    from King Bhumibol Adulyadej’s project and adapt the lessons
                    of SEP to suit the conditions of Doi Tung. In what was
                    hailed as one of the first Alternative Development (AD)
                    programs in Thailand, they developed a holistic and
                    people-centric approach that focused on addressing the basic
                    needs of residents and providing them with alternative
                    livelihoods. Within years, the once marginalized had
                    established a supply chain – made up of high value crops
                    such as coffee and macadamia nuts, as well as handicrafts
                    and processed foods – that was connected to the mainstream
                    and reaped them an adequate income.
                  </p>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col xs={12} sm={12} lg={6} className="scroll1">
          <Row className="gy-2 gx-2">
            <Col xs={12} sm={12} lg={12}>
              <img
                src="./images/page-57-content/image-57-02.webp"
                className="image-page"
              />
            </Col>
            <Col xs={12} sm={4} lg={4}>
              <p className="text-paragraph-nue">
                Today, the DoiTung brand under which Doi Tung Development
                Project products are sold is internationally renowned. But that
                the brand and what it stands for have also inspired the coinage
                of the term “development-led approach” to crime prevention is
                not very well-known. During her period as Ambassador and
                Permanent Representative of Thailand at the United Nations,
                Princess Bajrakitiyabha was part of an international drive to
                develop a new set of standards for guiding Member States in
                promoting area-based and development-oriented drug policies. The
                United Nations Guiding Principles on Alternative Development, as
                they are today known, were adopted by the General Assembly in
                2014.
              </p>
              <p className="text-paragraph-nue d-none d-lg-block d-sm-block">
                As Dr. Phiset Sa-ardyen, then the Director for TIJ’s External
                Relations Office, recalls, “the term ‘development-led’
                originated from the discussion between TIJ executives and the
                Princess – literally on the airline shuttle bus
              </p>
              <p className="text-paragraph-nue d-block d-lg-none d-sm-none">
                As Dr. Phiset Sa-ardyen, then the Director for TIJ’s External
                Relations Office, recalls, “the term ‘development-led’
                originated from the discussion between TIJ executives and the
                Princess – literally on the airline shuttle bus carrying our
                entourage to board a flight during one of those international
                tours on which the TIJ team accompanied the Princess.” While the
                term ‘Alternative Development’ is widely recognized among policy
                makers in the drug control domain, there was a lack of suitable
                terminology for the similar application of development
                perspectives in the field of crime prevention and criminal
                justice. One of the entourage gave it a try and suggested the
                term ‘development-led’ and the Princess agreed that the term
                nicely captures the sense of what they were discussing. “That
                was how it all began and we at TIJ have been promoting the new
                concept ever since,” says Dr. Phiset.
              </p>
            </Col>
            <Col xs={12} sm={4} lg={4}>
              <p className="text-paragraph-nue d-none d-lg-block d-sm-block">
                carrying our entourage to board a flight during one of those
                international tours on which the TIJ team accompanied the
                Princess.” While the term ‘Alternative Development’ is widely
                recognized among policy makers in the drug control domain, there
                was a lack of suitable terminology for the similar application
                of development perspectives in the field of crime prevention and
                criminal justice. One of the entourage gave it a try and
                suggested the term ‘development-led’ and the Princess agreed
                that the term nicely captures the sense of what they were
                discussing. “That was how it all began and we at TIJ have been
                promoting the new concept ever since,” says Dr. Phiset.
              </p>
              <p className="text-paragraph-nue">
                Subsequently, she has, in her capacity as UNODC Goodwill
                Ambassador for the Rule of Law, helped to promote and
                disseminate these principles and popularize the Doi Tung
                Development Project as well as the development-led model among
                Member
              </p>
            </Col>
            <Col xs={12} sm={4} lg={4}>
              <img
                src="./images/page-57-content/image-57-03.webp"
                className="image-page h-auto"
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerLight>
  );
}
