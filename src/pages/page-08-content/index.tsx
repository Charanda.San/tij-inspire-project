import { WrapperContent } from "@/components/Widget";
import React from "react";
import { Col, Row } from "react-bootstrap";
import "./page-04-content.css";
import { CaptionImagePosition } from "@/components";

const Page04Content = () => {
  const caption: any[] = [
    {
      position: "",
      caption: (
        <>
          Handcrafted bells, created as part of the Kamlangjai Project, are a
          symbol of awareness about violence against women. This bell was one of
          many hanging from fencing at Chiang Mai{" "}
          <p>Women Correctional Institution.</p>{" "}
        </>
      ),
    },
  ];
  return (
    <WrapperContent>
      <Row className="h-100">
        <Col lg={6} className="flex-center d-none d-lg-flex">
          <div
            style={{ height: "400px", width: "400px", marginBottom: "10px" }}
          >
            <img
              src="./images/page-04-content/dayfive_chiangmai091.jpg"
              className="image-page"
            />
             <div className="mt-2">
            <CaptionImagePosition captions={caption} position="top" />
          </div>
          </div>
         
        </Col>
        <Col lg={6} className="d-block d-lg-none">
          <div
          >
            <img
              src="./images/page-04-content/dayfive_chiangmai091.jpg"
              className="image-page"
            />
             <div className="mt-2">
            <CaptionImagePosition captions={caption} position="top" />
          </div>
          </div>
         
        </Col>
        <Col
          lg={6}
          className="h-100 px-4 py-4 py-lg-5"
          style={{ backgroundColor: "#FFF5D5" }}
        >
          <h5 className="text-bookman mb-3 ">
            <p style={{ fontWeight: 500 }}>FOREWORD</p>
          </h5>
          <Row className="h-100 overflow-auto">
            <Col lg={6}>
              <div className="text-paragraph text-bookman">
                <p>
                  For more than a decade, Her Royal Highness Princess
                  Bajrakitiyabha has graced the field of criminal justice and
                  development with a zealous commitment to rectifying structural
                  injustices and improving the lives of marginalized populations
                  worldwide.
                </p>
                <p>
                  During that period, I had the privilege of witnessing first
                  hand how Her Royal Highness undertook various initiatives both
                  at the domestic and international levels. I consider such an
                  opportunity a privilege in the sense that it allows me to work
                  with someone who is deeply passionate about the cause that I
                  myself have pursued throughout almost my entire career - the
                  issues of justice, the rule of law and the rights of
                  vulnerable groups.
                </p>
                <p>
                  In 2011, when the Royal Thai Government established the
                  Thailand Institute of Justice (TIJ), Her Royal Highness
                  graciously accepted the invitation to serve as theChairperson
                  of the Special Advisory Board. As the Executive Director of
                  the TIJ, I speak for all the staff when I say that we are
                  enormously grateful to Her Royal Highness for the invaluable
                  guidance and support she has provided to the TIJ since its
                  inception. I believe that it is the responsibility of the TIJ
                  to undertake to document the works of Her Royal Highness and
                  provide a suitable reference for those who are interested to
                  learn how this devotion has touched the lives of countless
                  individuals and continues to send waves of impact throughout
                  the global community.
                </p>
                <p>
                  Many communities in Thailand and around the world continue to
                  face marginalization along with limited access to justice. The
                  impact of such hardships can be felt more prominently by
                  certain groups of people, such as women and children. Through
                  initiatives like the Kamlangjai Project and the Enhancing
                  Lives of Female Inmates advocacy campaign, Her Royal Highness
                  has been a devout advocate for the rights of women and
                  children in contact with the justice system. Her passion and
                  dedication have snowballed into large-scale efforts in the
                  realm of promoting gender-sensitive criminal justice.
                </p>
              </div>
            </Col>
            <Col lg={6}>
              <div className="text-paragraph text-bookman">
                <p>
                  This book sets out to describe these initiatives and many
                  others while exploring the ideas behind the work of Her Royal
                  Highness since her early years of involvement. We have relied
                  on a vast array of resources in different formats to achieve
                  these objectives. One primary source is the collection of
                  speeches by Her Royal Highness from 2009 to 2018 that the TIJ
                  published in 2019. We have also used photographs from the TIJ
                  archive and those of other organizations. Accompanying the
                  rich selection of archived pictures is the narrative
                  description and supplementary photoshoots by professional
                  photojournalists.
                </p>
                <p>
                  I want to thank the editorial and design team at Palotai
                  Publishing for their hard work on the research, interviews of
                  key people, writing, design and production of this
                  publication. Many thanks to Paula Bronstein and Lauren DeCicca
                  for visiting various sites within the Thai criminal justice
                  system to shoot additional photographs that enrich this
                  volume, and to Dr. Phiset Sa-ardyen and Sommanat Juaseekoon
                  for overseeing the project and working closely with the
                  editorial team on behalf of the TIJ.
                </p>
                <p>
                  The TIJ would also like to extend its sincere appreciation to
                  all individuals and organizations who supported this project
                  in any way.
                </p>
                <p>
                  While this book attempts to illustrate the invaluable
                  contributions made by Her Royal Highness, it is also a record
                  of past and current initiatives in criminal justice reform. In
                  this sense, I hope that the book will inform and inspire the
                  next generation of justice makers.
                </p>
                <div style={{ height: "100%", margin: "0px" }}>
                  <img
                    src="./images/page-04-content/sign.webp"
                    className="image-page"
                  />
                </div>
                <div style={{ textAlign: "center" }}>
                  <p className="mb-0">Kittipong Kittayarak</p>
                  <p className="mb-0">Executive Director</p>
                  <p>Thailand Institute of Justice</p>
                </div>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </WrapperContent>
  );
};

export default Page04Content;
