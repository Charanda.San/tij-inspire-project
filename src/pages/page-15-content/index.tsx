import { CaptionImagePosition, LayoutText } from "@/components";
import { ICaptionPosition } from "@/components/CaptionImagePosition";
import React from "react";
import { Col, Row } from "react-bootstrap";

const Page11Content = () => {
  const captions : ICaptionPosition[] = [
    {
      position: 'Top: ',
      caption:`Princess Bajrakitiyabha speaks at the United Nations High-Level Panel Discussion on Transnational Organized Crime in New York, June 2017`
      
    },
    {
      position: 'Bottom: ',
      caption:`Princess Bajrakitiyabha and a UNODC delegation are shown around Tangerang Women’s Prison on the outskirts of Jakarta, April 2018.
      `
    }
  ]
 
  return (
    <LayoutText
      divider="The Criminal Justice Background of HRH Princess Bajrakitiyabha"
      leftComponent={
        <Row className="gx-3"
        >
          <Col lg={6} sm={6} xs={12}>
            <div className="text-paragraph">
              <p>
                for that matter. “She enjoys talking to witnesses and gathering
                information by herself,” he says.
              </p>
              <p>
                Coinciding with her gradual rise through the ranks of the Thai
                prosecution service in the late 2000s were various charitable
                endeavors, among them the Kamlangjai Project: a charity program
                she set up in order to address some of the glaring health,
                welfare and training gaps in the Thai prison service. Its
                creation in October 2006, shortly after her return from New
                York, using funds from her own purse, was a turning point at
                which the plight and prospects of female prisoners became a key
                focus of her work.
              </p>
              <p>
                Domestically, alliances with the private sector allowed the
                Kamlangjai Project to start rolling out everything from optical
                care to motherhood courses within several women's prisons. And
                the Kamlangjai Project was soon making its presence felt
                internationally, too — on Her Royal Highness's initiative, a
                Kamlangjai Project exhibition was presented, in April 2008, at
                the opening of the 17<sup>th</sup> Session of the United Nations Commission
                on Crime Prevention and Criminal Justice (CCPCJ) in Vienna,
                Austria.
              </p>
              <p>
                Here began Her Royal Highness's push for the development and
                adoption of the first set of UN standards for female offenders,
                a multi-pronged global campaign to garner support for what
                would later become known as the Bangkok Rules. Through a
                collaborative drafting process and tactical lobbying of a broad
                array of criminal justice actors — from governments to NGOs,
                human rights activists to academics — this campaign proved
                successful within only two years: the United Nations General
                Assembly unanimously adopted them in December 2010. Even more
                impressive was the fact that this diplomatic success had been
                achieved whilst Her Royal Highness still fulfilled her public
                prosecutor duties back at home.
              </p>
              <p>
                More accomplishments within high-level multilateralism followed.
                Between 2011 and 2012, she chaired several important meetings of
                the United Nations Commission on Crime Prevention and Criminal
                Justice (CCPCJ). And from 2012 to 2014, she put her prosecutor
                career on hold to serve
              </p>
            </div> 
          </Col>
          <Col lg={6} sm={6} xs={12}>
            <div className="text-paragraph">
              <p>
                as the Ambassador and Permanent Representative of Thailand to
                the UN in Vienna. In this prestigious yet demanding role, she
                further bolstered Thailand's reputation as an emerging UN
                player by championing another criminal justice cause close to
                her heart: violence against children. During this period, she
                also successfully lobbied for rule of law to be included in
                the United Nations' Post-2015 Development Agenda, and worked
                closely with the United Nations Office on Drugs and Crime
                (UNODC) and other nations, such as Peru, on community-based
                Alternative Development approaches to drug control.
              </p>
              <p>
                This period further reveals the larger character of her work —
                the complementary nature of her diplomacy, charity and criminal
                prosecutor roles — that has helped her to expand the impact of
                her ideas in different arenas. For example, while the Bangkok
                Rules were designed to benefit women offenders worldwide, the
                momentum and sense of ownership they created were the catalyst
                that led to the creation of the Thailand Institute of Justice
                (TIJ): a research institute with a broad rule of law remit that
                includes promoting their implementation.
              </p>
              <p>
                Similarly, her more recent lobbying for United Nations members
                to prioritize new approaches to child crime prevention, put more
                emphasis on rule of law, and view community development as a
                form of crime prevention, have been matched by tangible progress
                at home: she has launched a sports club that gives juveniles new
                purpose; hard-wired in the TIJ's remit is fostering a culture of
                lawfulness; and a new pilot project in a Thai border community
                vulnerable to drug trafficking is being implemented.
              </p>
              <p>
                HRH's many encounters with female prisoners may have led her to
                a stark, simple realization that women prisoners have special
                needs and are more vulnerable than men. But, as this brief
                introduction has shown and this book will extensively explore,
                her life experiences and expansive criminal justice background
                have also led her towards a much broader conclusion: a
                multi-dimensional, holistic approach to crime and justice — one
                that deals with root causes not just criminals, that protects
                the vulnerable as well as metes out punishment — is the way
                forward.
              </p>
            </div>
          </Col>
        </Row>
      }
      rightComponent={
        <>
          <Row className="gy-3 gx-3">
            <Col lg={12} sx={12} xs={12}>
              <img
                src="./images/page-11-content/image-11.webp"
                className="image-page"
              />
            </Col>
            <Col lg={8} sm={7} xs={12}> 
              <img
                src="./images/page-11-content/image-11-1.webp"
                className="image-page"
              />
            </Col>
            <Col lg={4} sm={5} xs={12} > 
            <CaptionImagePosition captions={captions} position="top"/>
             </Col>
          </Row>
         
        </>
      }
    ></LayoutText>
  );
};

export default Page11Content;
