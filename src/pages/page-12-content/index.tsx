import React from "react";
import { Col, Row } from "react-bootstrap";
import "./page-12-content.css";

const Page08Content = () => {
  return (
    <div className="bg-main root-main-div ">
      <Row className="bg-white wrapper-row-image px-lg-2 py-lg-3 h-100 px-3 py-3 py-sm-0 gy-3 gy-lg-0 ">
        <Col lg={12} className="d-block overflow-auto px-lg-0">
          <img
            src="./images/page-08-content/image-08.webp"
            className="image-page scroll-image-view"
          />
        </Col>
        {/* <Col lg={12} className="d-lg-none d-block">
          <img
            src="./images/page-08-content/image-08-left.png"
            className="image-page"
          />
        </Col>
        <Col lg={12} className="d-lg-none d-block">
          <img
            src="./images/page-08-content/image-08-right.png"
            className="image-page"
          />
        </Col> */}
      </Row>
    </div>
  );
};

export default Page08Content;
