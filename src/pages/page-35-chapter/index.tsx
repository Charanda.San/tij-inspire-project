import React from "react";
import { Chapter } from "src/components";
import { IPropsChapter } from "@/components/Chapter";
export default function Page35Chapter() {
  const data: IPropsChapter = {
    src: "./images/page-35-chapter/cover-35.webp",
    title: "THE RIGHTS OF CHILDREN",
    chapterText: "chapter 2",
    content:
      "Princess Bajrakitiyabha's early advocacy and charity initiatives were inspired by her belief that society has a duty to protect and nurture the precious bond between female prisoners and their newborn babies. Recently, however, her child rights work has focused more broadly on children in contact with the wider justice system — be they young offenders or young victims of crime — or at high risk of being in contact. Her achievements in this regard include playing a key advocacy role in the drafting, adoption and dissemination of the United Nations' 'Model Strategies' for eliminating violence against children, and also championing the use of sport as a promising yet still nascent youth crime prevention tool, both in Thailand and internationally.",
    descriptionImg: (
      <p style={{ maxWidth: "210px" }}>
        Boys fifile towards the dining area at the Juvenile Vocational Training
        Centre in Chiang Mai, Northern Thailand.
      </p>
    ),
  };
  return <Chapter {...data} />;
}
