import { CaptionImagePosition, ContainerAfterWorld } from "@/components";
import { ICaptionPosition } from "@/components/CaptionImagePosition";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page65Content() {
  const captions: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: `Princess Bajrakitiyabha delivers a video statement for a webinar organized by the UNODC, April 2012`,
    },
    {
      caption: ` Her Royal Highness at the United Nations in New York in 2017.`,
      position: "Top right: ",
    },
    {
      position: "Bottom",
      caption: `Her Royal Highness with Phiset Sa-ardyen, special advisor to TIJ, at the Crime Congress held
      in Doha, Qatar, in 2015.`,
    },
  ];
  const captionsMobile: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: `Princess Bajrakitiyabha delivers a video statement for a webinar organized by the UNODC, April 2012`,
    },
    {
      caption: ` Her Royal Highness at the United Nations in New York in 2017.`,
      position: (
        <>
          1 <sup>st</sup> page bottom:&nbsp;&nbsp;
        </>
      ),
    },
    {
      position: (
        <>
          2 <sup>nd</sup> page bottom:&nbsp;&nbsp;
        </>
      ),
      caption: `Her Royal Highness with Phiset Sa-ardyen, special advisor to TIJ, at the Crime Congress held
      in Doha, Qatar, in 2015.`,
    },
  ];
  return (
    <ContainerAfterWorld dividerCaption="AFTERWORD">
      <Row className="">
        <Col xs={12} sm={12} lg={6}>
          <Row className="gy-2 gx-2 gx-sm-3 gy-sm-3">
            <Col xs={12} sm={6} lg={6}>
              <Row className="gy-2 gx-2">
                <Col xs={12} sm={12} lg={12}>
                  <img
                    src="./images/page-65-content/image-65-01.png"
                    className="image-page"
                  />
                </Col>
                <Col xs={12} sm={12} lg={6} className="position-relative">
                  <div className="d-none d-sm-block d-lg-block">
                    <CaptionImagePosition style="monster" captions={captions} position="top" />
                  </div>
                  <div className="d-block d-sm-none d-lg-none">
                    <CaptionImagePosition
                    style="monster"
                      captions={captionsMobile}
                      position="top"
                    />
                  </div>
                </Col>
              </Row>
            </Col>

            <Col xs={12} sm={6} lg={6}>
              <Row className="gy-2 gx-2">
                <Col xs={12} sm={12} lg={12}>
                  <img
                    src="./images/page-65-content/image-65-02.png"
                    className="image-page"
                  />
                </Col>
                <Col xs={12} sm={12} lg={12}>
                  <img
                    src="./images/page-65-content/image-65-03.png"
                    className="image-page"
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col xs={12} sm={12} lg={6} className="scroll1">
          <Row className="text-paragraph py-2">
            <Col xs={12} sm={6} lg={6}>
              <p>
                for action by the General Assembly. The first was on the
                “Standard Minimum Rules for the Treatment of Prisoners” (GA Res.
                67/188) and the second on “Strengthening the rule of law and the
                reform of criminal justice institutions, particularly in the
                areas related to the United Nations system-wide approach to
                fighting transnational organized crime and drug trafficking” (GA
                Res. 67/186). Thus Thailand has become one of the most prolific
                countries presenting viable proposals for consideration and
                further action by the Commission.
              </p>
              <p>
                Tireless and resolute in her advocacy mission, she then traveled
                around the world as an invited distinguished guest and keynote
                speaker. She powerfully and convincingly spread her message that
                we have a common and shared responsibility to ensure that our
                crime prevention and criminal justice institutions are
                efficient, fair, accountable and humane, and that they uphold
                and maintain the rule of law through enhanced capacity building,
                effective technical assistance and strengthened international
                cooperation, in order to ensure truly sustainable development.
              </p>
              <p>
                Among the above-mentioned resolutions, there is one which I
                would like to highlight in particular due to its historic
                relevance. That is the “Standard Minimum Rules for the Treatment
                of Prisoners” (GA Res. 67/188). This resolution, drawing on the
                recommendations of the open-ended intergovernmental expert group
                that met for the first time in Vienna in early 2012, created an
                unexpected positive snow-ball effect leading to a major revision
                of the Rules more than 55 years after their approval by the
                First UN Congress in Geneva. Or, as more simply defined by Her
                Royal Highness, had “a catalytic impact on subsequent movement
                in the UN”.
              </p>
              <p>
                For four consecutive years, the expert group worked very hard,
                with key meetings held in Vienna, Buenos Aires, Vienna again and
                Cape Town; regularly reporting every year to the Commission on
                its progress; revising, drafting, receiving comments and
                observations from governments; redrafting, negotiating,
                discussing, re-negotiating in order to reach consensus; and
                managing to complete its task in time to report first to the
                Thirteenth UN Crime Congress in Doha and later on to the
                Commission. This long, intense and complex process culminated
                with the adoption by the General Assembly in December
              </p>
            </Col>
            <Col xs={12} sm={6} lg={6}>
              <p>
                2015 of the revised Rules, or Nelson Mandela Rules, named in
                honor of the late President of South Africa (GA Res. 70/175).
              </p>
              <p>
                The contribution of Thailand, guided by the vision of Her Royal
                Highness and renewed by the momentum of the Bangkok Rules, had
                been crucial not only for the participation of high-caliber
                experts and the co-sponsorship of the four successive relevant
                draft resolutions in the Commission, but also for ensuring the
                presence of the same standing rapporteur in all four meetings,
                thus providing the necessary continuity, consistency and proper
                coherence.
              </p>
              <p>
                Meanwhile, with the Thailand Institute of Justice (TIJ) being
                established in 2011 under the guidance of Her Royal Highness as
                Chairperson of its Special Advisory Board, work started first at
                home and later was extended to other countries to promote the
                implementation of the Bangkok Rules through research, training
                and capacity building, and to undertake other important
                initiatives, often in close cooperation with UNODC.
              </p>
              <p
                style={{
                  textAlign: "center",
                  color: "#4d4e4d",
                  fontSize:'14px',
                  lineHeight: 1.3,
                }}
                className="text-normal py-3 py-lg-1"
              >
                “Tireless and resolute in her advocacy mission, she then
                traveled around the world as an invited distinguished guest and
                keynote speaker in order to spread her message, powerfully and
                convincingly.”
              </p>
              <p>
                Elected in 2013 as second Vice-Chair of the Commission on
                Narcotic Drugs, Her Royal Highness’s initiatives then continued
                unabated at the twenty-second session of the Commission on Crime
                Prevention and Criminal Justice where, as the Head of the Thai
                delegation, she was very active during the approval of the draft
                resolutions on the “Preparations for the Thirteenth UN Congress”
                and on the “Standard Minimum Rules”. In addition, she submitted
                three other draft resolutions for their final adoption by the
                General Assembly: the first on “The rule of law, crime
                prevention and criminal justice in the United Nations
                development agenda beyond 2015” (GA Res. 68/188); the second on
                “Model Strategies and Practical Measures on the Elimination of
                Violence Against Children in the Field of Crime Prevention and
                Criminal Justice” (GA Res. 68/189); and the third draft
                resolution
              </p>
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerAfterWorld>
  );
}
