import {
  CaptionImagePosition,
  ContainerOrange,
  QuoteImage,
} from "@/components";
import { ICaptionPosition } from "@/components/CaptionImagePosition";
import { Inpractice } from "@/components/Widget";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const Page53Content = () => {
  const captions: ICaptionPosition[] = [
    {
      position: "This page: ",
      caption: "Banana trees are grown on the grounds of Khao Prik.",
    },
    {
      position: "Opposite page: ",
      caption: "A group of inmates make their way to their allotted farm zone.",
    },
  ];
  const captionsMobile: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: "Banana trees are grown on the grounds of Khao Prik.",
    },
    {
      position: "Bottom: ",
      caption: "A group of inmates make their way to their allotted farm zone.",
    },
  ];

  return (
    <ContainerOrange>
      <Row className="h-100 gy-3 gy-sm-3">
        <Col lg={6} className="scroll1 h-100">
          <Row className="h-100 gy-2">
            <Col lg={12} className="">
              <img src="./images/page-53-content/page-53.webp" alt="" />
            </Col>
            <Col xs={12} lg={4} className="px-2 order-lg-1 order-3">
              <div className="d-none d-sm-none d-lg-block">
                <CaptionImagePosition style="monster" captions={captions} position="top" />
              </div>
              <div className="d-block d-sm-block d-lg-none">
                <CaptionImagePosition
                style="monster" 
                  captions={captionsMobile}
                  position="top"
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col lg={6} className="scroll1 h-100">
          <Inpractice
            title="HOW AN OPEN PRISON TEACHES SELF-SUFFICIENCY"
            img="./images/page-53-content/page-53-1.webp"
            paragraph={[
              <>
                <p>
                  <span className="first-letter-orange">B</span>
                  efore the sun rises, the inmates of Khao Prik Agriculture
                  Industrial Institution are already awake and dressing for
                  work. After gathering to receive their assignments for the
                  day, they fan out across the largest agricultural industry
                  learning center in the country, which covers 1,055 rai in
                  Nakhon Ratchasima province, in Northeastern Thailand.
                </p>
                <p>
                  Relaxed and uncrowded, the atmosphere at this correctional
                  institution is different from what you might expect at a
                  prison. Against a backdrop of hills are rice paddies, small
                  farms, and trees providing welcome shade. This environment is
                  appreciated by the prisoners, who all noted that they feel
                  good here, free of stress and pressure, while enjoying their
                  professional training.
                </p>
                <p>
                  Khao Prik Agriculture Industrial Institution is a correctional
                  institution that has been launched through the efforts of
                  Princess Bajrakitiyabha, who wanted to combine the techniques
                  of modern agriculture with the principles of the Sufficiency
                  Economy, as a source of learning and vocational training to
                  enable inmates to improve themselves, become more
                  self-sufficient and and make a smooth return to society. Her
                  Royal Highness twice visited the facility within a period of
                  three years.
                </p>
              </>,
              <>
                <p>
                  The Khao Prik inmates – 321 at the time of the visit – have
                  been rewarded for their good behavior and have arrived in this
                  oasis as part of their pre-release program. Here they are
                  taught to farm – sweet corn, cassava, bananas, papaya, limes,
                  sugarcane, dragon fruit, and seasonal vegetables – and to
                  raise animals, so that their chances of generating income and
                  succeeding after their release improve.
                </p>
                <p>
                  The conglomerate Charoen Phokpand (CP), famous for its
                  agribusiness, has also become involved in helping the inmates.
                  CP helps ensure the training is comprehensive and provides
                  up-to-date job skills, from budgeting for crop procurement
                  through cultivation. Khao Prik also features chicken coops
                  which are supported directly by CP. In the chicken coops, the
                  inmates are like normal employees – they wear uniforms and
                  receive a salary, with CP staff monitoring the work.
                </p>
                <p>
                  There are also other vocational training programs at Khao Prik
                  such as a noodle shop and also a café, where the inmates can
                  learn skills as part of Her Royal Highness’s Kamlangjai
                  Project. Furthermore, the prison is open to the general
                  public, so that anyone can visit in order to study and learn
                  the art of growing organic vegetables and fruits, or animal
                  husbandry, under the Thai concept and tagline: “Ride a Bike,
                  Shop, Sightsee, and Taste Food.”
                </p>
              </>,
            ]}
          />
        </Col>
      </Row>
    </ContainerOrange>
  );
};

export default Page53Content;
