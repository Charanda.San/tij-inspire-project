import { ContainerOrange } from "@/components";
import CaptionImagePosition, {
  ICaptionPosition,
} from "@/components/CaptionImagePosition";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page54Content() {
  const captions: ICaptionPosition[] = [
    {
      position: "This page: ",
      caption: `Chilli, dragon 103 fruit and eggs are all
      produced at the prison
      through the labor
      of the inmates. The conglomerate CP helps run some vocational programs, including one focused on the production of eggs.`,
    },
    {
      position: "Opposite page: ",
      caption: `Raising animals is an activity that helps teach responsibility and empathy.`,
    },
  ];
  const captionsMobile: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: `Raising animals is an activity that helps teach responsibility and empathy.`,
    },
    {
      position: "Bottom: ",
      caption: `Chilli, dragon 103 fruit and eggs are all
      produced at the prison
      through the labor
      of the inmates. The conglomerate CP helps run some vocational programs, including one focused on the production of eggs.`,
    },
  ];
  const imagesTop = [
    "./images/page-54-content/image-54-01.webp",
    "./images/page-54-content/image-54-02.webp",
  ];
  const imagesRight = [
    "./images/page-54-content/image-54-04.webp",
    "./images/page-54-content/image-54-05.webp",
  ];
  return (
    <ContainerOrange>
      <>
        <Row className="gy-lg-4 d-none d-sm-none d-lg-block">
          <Col xl={12}>
            <Row className="h-100 gx-lg-5">
              <Col xl={6}>
                <Row className="h-100">
                  {imagesTop.map((item) => (
                    <Col xs={12} sm={6} lg={6}>
                      <img src={item} className="image-page" />
                    </Col>
                  ))}
                </Row>
              </Col>
              <Col xl={6}>
                <Row className="h-100">
                  {imagesRight.map((item) => (
                    <Col xs={12} sm={6} lg={6}>
                      <img src={item} className="image-page" />
                    </Col>
                  ))}
                </Row>
              </Col>
            </Row>
          </Col>
          <Col xl={12}>
            <Row className="h-100 gx-lg-5">
              <Col xl={6} className="h-100">
                <img
                  src="./images/page-54-content/image-54-03.webp"
                  className="image-page"
                />
              </Col>
              <Col xl={6}>
                <Row className="h-100">
                  <Col xl={9}>
                    <img
                      src="./images/page-54-content/image-54-06.webp"
                      className="image-page"
                    />
                  </Col>
                  <Col xl={3}>
                    <div className="my-2">
                      <hr className="divider-short-orange" />
                    </div>
                    <div className="position-relative">
                      <CaptionImagePosition
                        style="monster"
                        captions={captions}
                        position="top"
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>

        <Row className="gy-3 d-block d-sm-block d-lg-none">
          <Col xs={12} lg={6} sm={12}>
            <Row className="gx-3 gy-3">
              {imagesTop.map((item) => (
                <Col xs={12} sm={6} lg={6}>
                  <img src={item} className="image-page" />
                </Col>
              ))}
              <Col xs={12} sm={12} lg={12}>
                <img
                  src="./images/page-54-content/image-54-03.webp"
                  className="image-page"
                />
              </Col>
            </Row>
          </Col>
          <Col xs={12} sm={10} lg={3} className="d-block d-sm-block d-lg-none">
            <div className="my-2">
              <hr className="divider-short-orange" />
            </div>
            <div className="position-relative">
              <CaptionImagePosition style="monster" captions={captionsMobile} position="top" />
            </div>
          </Col>
          <Col xs={12} lg={6} sm={12}>
            <Row className="gx-3 gy-3">
              {imagesRight.map((item) => (
                <Col xs={12} sm={6} lg={6}>
                  <img src={item} className="image-page" />
                </Col>
              ))}
              <Col xs={12} sm={12} lg={9}>
                <img
                  src="./images/page-54-content/image-54-06.webp"
                  className="image-page"
                />
              </Col>
              <Col
                xs={12}
                sm={4}
                lg={3}
                className="d-none d-sm-none d-lg-block"
              >
                <div className="my-2">
                  <hr className="divider-short-orange" />
                </div>
                <div className="position-relative">
                  <CaptionImagePosition style="monster" captions={captions} position="top" />
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </>
    </ContainerOrange>
  );
}
