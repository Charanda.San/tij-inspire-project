import { ContainerAfterWorld, QuoteImage } from "@/components";
import React from "react";
import { Col, Row } from "react-bootstrap";

const Page16Content = () => {
  return (
    <ContainerAfterWorld dividerCaption="THE RIGHTS OF WOMEN">
      <Row className="h-100">
        <Col lg={6} sm={12} xs={12} className="h-100">
          <Row className="gx-lg-2 gy-lg-3 gx-sm-2 gy-1 h-100 overflow-auto text-roboto-condensed">
            <Col xs={12} sm={4} xl={2} className="position-relative">
              <p className="text-caption-image position-text-image">
                An energy and environment class arranged by the Kamlangjai
                Project at Ayutthaya Provincial Prison, 2017.
              </p>
            </Col>
            <Col sm={12} xs={12} xl={10} className="mb-2 mb-lg-1">
              <img
                src="./images/page-21-content/image-21.webp"
                className="image-page"
              />
            </Col>
            <Col lg={4} sm={4} xs={12}>
              <div>
                <p>
                  began by inviting organizations,” she explains, “but later,
                  when the Kamlangjai Project became known, other organizations
                  started to volunteer.”
                </p>
                <p>
                  Another defining attribute is its incremental and localized
                  approach: instead of being rolled out in a blanket fashion,
                  its activities have always been sensitively designed to meet
                  the cultural context of each prison and the needs of each
                  target group within it. According to Dr. Kittipong Kittayarak,
                  who was serving in the Ministry of Justice at the time, it
                  could have been a bigger, more scattershot nationwide
                  initiative from the beginning. However, this offer was
                  politely declined by the Princess. “She just wanted her own
                  small project — something she could oversee and that would help
                  women,” he says.
                </p>

                <p>
                  Given that there was, in the late 2000s, a lack of
                  evidence-based consensus about what constitutes humane
                  treatment of female prisoners, this slowly-but-surely approach
                  appears shrewd in retrospect. The predicament Thailand faced
                  back then was shared by many countries: a growing female
                  prison population within a male-centered penitentiary system.
                  But the dynamics of this predicament were
                </p>
              </div>
            </Col>
            <Col lg={4} sm={4} xs={12}>
              <div>
                <p>
                  understood by few. While research into female prison culture,
                  as well as the profile of female offenders and their pathways
                  to crime, had been done by academics working at the nexus of
                  criminology, penology and feminist theory, there were no
                  actionable, gender-sensitive norms for women's prisons in
                  place.
                </p>
                <p>
                  Moreover, the de facto standards for female inmate management —
                  the 1955 United Nations Standard Minimum Rules for the
                  Treatment of Prisoners, or SMRs' — had been tailor-made, like
                  the majority of prison infrastructure, for men, and at a time
                  when female prison populations were much smaller. As Penal
                  Reform International stated later, the SMR's few
                  gender-sensitive provisions included requests for separate
                  facilities for women and men, and that women prisoners be
                  exclusively supervised and attended to by female officers.
                  They also included rules concerning pre and post natal care
                  and nursing of infants. However, in other areas impacting
                  women's dignity and chances of reintegration — such as hygiene
                  and personal care, reproductive care, women centered substance
                  abuse and mental health programs, and gender-appropriate
                  education and training — they were deficient.
                </p>
              </div>
            </Col>
            <Col lg={4} sm={4} xs={12}>
              <div>
                <p>
                  Within this patriarchal context, the Kamlangjai Project
                  functioned in its early years as a policy testbed and
                  launchpad that, alongside the Thai corrections department,
                  worked to put often experimental ideas in the neglected field
                  of women's corrections into practice. Some of these welfare,
                  education and rehabilitation pilot projects were deemed
                  successes and adopted as best practices. Others faded away.
                  But through the act of trying, it helped and inspired
                  thousands of incarcerated women while sensitizing corrections
                  staff to both the gender gap in their practices and the latent
                  potential of external collaboration.
                </p>
                <p>
                  It also helped lay the groundwork for what followed: a
                  multilateral push for a much needed paradigm shift in how
                  women's prisons are managed. In 2008, not long after being
                  founded, the Kamlangjai Project became the casestudy that
                  spurred the creation of a set of international standards that,
                  while not legally binding, would finally address the stark
                  gender imbalances of the SMRs and criminal justice systems
                  everywhere. Change was not just coming to Thai women's prisons
                  — it was being recommended for all women's prisons.
                </p>
              </div>
            </Col>
          </Row>
        </Col>
        <Col lg={6} className="h-100 overflow-auto">
          <Row className="gy-3">
            <Col lg={12}>
              <div>
                <img
                  src="./images/page-21-content/image-21-1.webp"
                  className="image-page"
                />
              </div>
            </Col>
            <Col
              lg={9}
              xs={12}
              sm={9}
              className="order-1 order-sm-0 order-lg-0"
            >
              <div className="px-lg-3 px-sm-3 px-1">
                <QuoteImage
                  fontSub="Neue Haas Unica"
                  subQuote={
                    <>
                      Excerpt from Her Royal Highness Princess Bajrakitiyabha’s
                      speech at the 15<sup>th</sup> Meeting of Women
                      Parliamentarians, 122<sup>nd</sup> Assembly of the
                      Inter-Parliamentary Union, Bangkok, 27 March, 2010
                    </>
                  }
                  quote={`“While Kamlangjai offers a micro-perspective of how Thailand is trying to improve its own standards, the fact remains that the insensitive treatment of women prisoners, including the violence
                  against them, is a global phenomenon, albeit with varying degrees. To correct this anachronistic course, we need to look at the source of inspiration for correctional policies and management worldwide.”`}
                ></QuoteImage>
              </div>
            </Col>
            <Col
              lg={3}
              xs={12}
              sm={3}
              className="order-0 order-sm-1 order-lg-1"
            >
              <p style={{fontFamily:'Neue Haas Unica'}}  className="text-caption-image">
                Prisons in Thailand offer practical classes to mothers to foster
                healthy pregnancies and strengthen mother child bonds, as well
                as give selected inmates the hands on knowledge they need to
                take care of infants.
              </p>
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerAfterWorld>
  );
};

export default Page16Content;
