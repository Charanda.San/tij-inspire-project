import { ContainerOrange } from "@/components";
import CaptionImagePosition, {
  ICaptionPosition,
} from "@/components/CaptionImagePosition";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page55Content() {
  const captions: ICaptionPosition[] = [
    {
      position: "This page: ",
      caption: `The on-site noodle shop and café caters to the general public. The interactions between prisoners and customers helps to assuage common doubts and fears about reintegration.`,
    },
    {
      position: "Opposite page: ",
      caption: 'Activities at the facility include planting rice and constructing a cow shed.'
    },
  ];
  const captionsMobile: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: 'Activities at the facility include planting rice and constructing a cow shed.'
    },
    {
      position: "Bottom: ",
      caption: `The on-site noodle shop and café caters to the general public. The interactions between prisoners and customers helps to assuage common doubts and fears about reintegration.`,
    }
  ];
  const imagesTop = [
    "./images/page-55-content/image-55-02.webp",
    "./images/page-55-content/image-55-03.webp",
  ];
  const imagesRight = [
    "./images/page-55-content/image-55-04.webp",
    "./images/page-55-content/image-55-05.webp",
  ];
  return (
    <ContainerOrange>
      <>
        <Row className="gy-lg-4 d-none d-sm-none d-lg-block">
          <Col xl={12}>
            <Row className="h-100 gx-lg-5">
              <Col xl={6}>
              <img
                  src="./images/page-55-content/image-55-01.webp"
                  className="image-page"
                />
               
              </Col>
              <Col xl={6}>
                <Row className="h-100">
                  {imagesRight.map((item) => (
                    <Col xs={12} sm={6} lg={6}>
                      <img src={item} className="image-page" />
                    </Col>
                  ))}
                </Row>
              </Col>
            </Row>
          </Col>
          <Col xl={12}>
            <Row className="h-100 gx-lg-5">
              <Col xl={6} className="h-100">
                 <Row className="h-100">
                  {imagesTop.map((item) => (
                    <Col xs={12} sm={6} lg={6}>
                      <img src={item} className="image-page" />
                    </Col>
                  ))}
                </Row>
              </Col>
              <Col xl={6}>
                <Row className="h-100">
                  <Col xl={9}>
                    <img
                      src="./images/page-55-content/image-55-06.webp"
                      className="image-page"
                    />
                  </Col>
                  <Col xl={3}>
                    <div className="my-2">
                      <hr className="divider-short-orange" />
                    </div>
                    <div className="position-relative">
                      <CaptionImagePosition
                      style="monster"
                        captions={captions}
                        position="top"
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>

        <Row className="gy-3 d-block d-sm-block d-lg-none">
          <Col xs={12} lg={6} sm={12}>
            <Row className="gx-3 gy-3">
            <Col xs={12} sm={12} lg={12}>
                <img
                  src="./images/page-55-content/image-55-01.webp"
                  className="image-page"
                />
              </Col>
              {imagesTop.map((item) => (
                <Col xs={12} sm={6} lg={6}>
                  <img src={item} className="image-page" />
                </Col>
              ))}
            </Row>
          </Col>
          <Col xs={12} sm={10} lg={3} className="d-block d-sm-block d-lg-none">
            <div className="my-2">
              <hr className="divider-short-orange" />
            </div>
            <div className="position-relative">
              <CaptionImagePosition style="monster" captions={captionsMobile} position="top" />
            </div>
          </Col>
          <Col xs={12} lg={6} sm={12}>
            <Row className="gx-3 gy-3">
              {imagesRight.map((item) => (
                <Col xs={12} sm={6} lg={6}>
                  <img src={item} className="image-page" />
                </Col>
              ))}
              <Col xs={12} sm={12} lg={9}>
                <img
                  src="./images/page-55-content/image-55-06.webp"
                  className="image-page"
                />
              </Col>
              <Col
                xs={12}
                sm={4}
                lg={3}
                className="d-none d-sm-none d-lg-block"
              >
                <div className="my-2">
                  <hr className="divider-short-orange" />
                </div>
                <div className="position-relative">
                  <CaptionImagePosition style="monster" captions={captions} position="top" />
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </>
    </ContainerOrange>
  );
}
