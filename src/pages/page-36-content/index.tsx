import React from "react";
import { ContentGreen, ImageParagraphPage } from "@/components";
import { IPropsContent } from "@/components/ContentGreen";
import { IPropsImageParagraphPage } from "@/components/ImageParagraph";
export default function Page36() {
  const propsImageParagraph: IPropsImageParagraphPage = {
    image: "./images/page-36-content/page-36.webp",
    positionCaption: "right",
    paragraphs: [
      <>
        <p className="d-none d-lg-block">
          Based on consultations between the Office of the High Commissioner of
          Human Rights (OHCHR), the United Nations Office on Drugs and Crime
          (UNODC) and Marta Santos Pais, the Special Representative of the
          Secretary General on Violence against Children, the joint report laid
          out plainly what experts thought needed to change. But as with many UN
          reports, its recommendations alone were not enough to enact that
          change. Crucially, what was needed next was inter-governmental
          advocacy to create debate and momentum
        </p>
        <p className="d-block d-lg-none">
          Based on consultations between the Office of the High Commissioner of
          Human Rights (OHCHR), the United Nations Office on Drugs and Crime
          (UNODC) and Marta Santos Pais, the Special Representative of the
          Secretary General on Violence against Children, the joint report laid
          out plainly what experts thought needed to change. But as with many UN
          reports, its recommendations alone were not enough to enact that
          change. Crucially, what was needed next was inter-governmental
          advocacy to create debate and momentum within the sphere of the
          Commission on Crime Prevention and Criminal Justice (CCPCJ), the
          principal policymaking body of the United Nations in the field of
          crime prevention and criminal justice.
        </p>
      </>,
      <>
        <p className="d-none d-lg-block">
          within the sphere of the Commission on Crime Prevention and Criminal
          Justice (CCPCJ), the principal policymaking body of the United Nations
          in the field of crime prevention and criminal justice.
        </p>
        <p className="d-none d-lg-block">
          It was at this juncture that the criminal justice experience and
          diplomatic wherewithal of Princess Bajrakitiyabha proved invaluable.
          After being approached by the co-authors of the joint report, she
          began working strategically in collaboration with likeminded
        </p>
        <p className="d-block d-lg-none">
          It was at this juncture that the criminal justice experience and
          diplomatic wherewithal of Princess Bajrakitiyabha proved invaluable.
          After being approached by the co-authors of the joint report, she
          began working strategically in collaboration with likeminded partners
          from Austria, UN bodies and other members of the Thai delegation to
          help repackage it in a form that would trigger the formal diplomatic
          responses from Member States, including the rounds of negotiation
          necessary to make its recommendations agreeable and legitimate to
          them. Most notably, she hosted a briefing and an informal consultation
          concerning two United Nations General Assembly Resolutions (67/166 and
          68/189) that had been tabled by Thailand.
        </p>
      </>,
      <p className="d-none d-lg-block">
        partners from Austria, UN bodies and other members of the Thai
        delegation to help repackage it in a form that would trigger the formal
        diplomatic responses from Member States, including the rounds of
        negotiation necessary to make its recommendations agreeable and
        legitimate to them. Most notably, she hosted a briefing and an informal
        consultation concerning two United Nations General Assembly Resolutions
        (67/166 and 68/189) that had been tabled by Thailand.
      </p>,
    ],
    caption: `A boy displays his tattoos at the Juvenile Vocational Training Centre in Chiang Mai.`,
  };
  const propsContent: IPropsContent = {
    title: (
      <div>
        Preventing Violence <br /> Against Children
      </div>
    ),
    textDividerLeft: "inspire",
    textDividerRight: "THE RIGHTS OF CHILDREN",
    character: "i",
    subTitle: `s your justice system doing all it can to prevent violence against children? Is it responding to incidents of violence against children in an effective and appropriate manner? And is it dealing with children in conflict with
the law in a manner which reduces their risk of being further victimized or abused? It is these kinds of questions that Princess Bajrakitiyabha helped to pose to the international community between 2012 and 2014, when she used her tenure as the Ambassador and Permanent Representative of Thailand to the United Nations in Vienna to position Thailand as a leading advocate of a new set of international norms for the protection of children.`,
    paragraphs: [
      <p>
        Prior to Her Royal Highness’s and Thailand’s involvement, the
        multilateral movement driving this change had begun to gather pace with
        the release, in June 2012, of a joint report commissioned by the Human
        Rights Council of the United Nations. At least one million children were
        being deprived of their liberty worldwide, it claimed, and the majority
        of them were awaiting trial for minor or first-time offences. It also
        noted that perceptions of surging youth crime – a perception often not
        borne out by the statistics – were, through the swaying of political
        discourse, leading to the adoption of disproportionately punitive
        legislation that weakens children’s rights.
      </p>,
      <p>
        When it came to reducing criminalization and penalization of children,
        the report also highlighted a paucity of crime prevention plans: “There
        is an increasing body of knowledge about effective crime prevention
        strategies, and the demonstrated cost-saving benefits of prevention, but
        in contrast with this principle, many countries invest in building
        detention facilities for children, rather than prioritizing investment
        in prevention measures.” It also noted a worrying trend to criminalize
        children living or working in the streets, the overrepresentation of
        certain groups of children – the homeless, mentally ill or drug
        addicted, among others – and the low minimum ages of crimina
      </p>,
      <>
        <p>
          responsibility in many states.“Deprivation of liberty tends to become
          a preferred solution,” the report stated, “rather than a measure of
          last resort, and is often accompanied by extortion, ill treatment and
          sexual abuse.”
        </p>
        <p>
          The risks of violence that children are exposed to and the systemic
          factors contributing to this violence were also identified and
          analyzed. A number of strategies were recommended, including the use
          of “diversion mechanisms” that prevent and respond to violence against
          children in contact with the juvenile justice system, such as
          restorative justice programs and the use of non-custodial measures.
        </p>
      </>,
    ],
    componentRightSide: <ImageParagraphPage {...propsImageParagraph} />,
  };

  return (
    <>
      <ContentGreen {...propsContent}></ContentGreen>
    </>
  );
}
