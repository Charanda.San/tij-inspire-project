import React from "react";
import { Col, Row, Image } from "react-bootstrap";
import "./page-38-content.css";
import { ContentRules, WrapperContent } from "@/components/Widget";
export default function Page38Content() {
  return (
    <Row className="page-38-content">
      <Col lg={6} sm={12} xs={12} className="col-left h-100 scroll2">
        <ContentRules
          title="HIGHLIGHTS OF THE MODEL STRATEGIES"
          type="strategy"
          content={[
            {
              rule: 1,
              text: "Ensure the prohibition by law of all forms of violence against children",
            },
            {
              rule: 4,
              text: "Establish effective detection and reporting mechanisms",
            },
            {
              rule: 5,
              text: "Offer effective protection to child victims of violence",
            },
            {
              rule: 9,
              text: "Ensure that sentencing reflects the serious nature of violence against children",
            },
            {
              rule: 10,
              text: "Strengthen capacity and training of criminal justice professionals",
            },
            {
              rule: 13,
              text: "Ensure that deprivation of liberty is used only as a measure of last resort",
            },
            {
              rule: 16,
              text: "Prevent and respond to violence against children in places of detention",
            },
          ]}
          top={
            <div className="text-param text-center py-2">
              <p>
                The United Nations’ 17 Model Strategies for preventing and
                responding to violence against children are split into three
                broad categories: general prevention strategies, strategies for
                improving the ability and capacity of the criminal justice
                system, and strategies for protecting children in contact with
                the justice system.
              </p>
            </div>
          }
          bottom={
            <div className="d-block mx-auto px-0 px-lg-3  ">
              <p className="text-paragraph-nue" style={{ fontSize: "14px" }}>
                “Violence against children is never justified and all violence
                against children can be effectively prevented. With strong
                political will, wide mobilization and steady action, it can be
                brought to an end.”
              </p>
              <div className="d-block ms-auto caption-end">
                <strong className="text-paragraph-nue fst-italic ">
                  – Marta Santos Pais, Special Representative of the UN
                  Secretary-General on Violence against Children
                </strong>
              </div>
            </div>
          }
        />
      </Col>
      <Col lg={6} sm={12} xs={12} className="col-right h-100 scroll2">
        <Row className="gx-2">
          <Col lg={12} className="hidden-xs text-xl-end">
            <p className="text-divider-grey mb-1">THE RIGHTS OF CHILDREN</p>
            <hr className="divider-orange" />
          </Col>
          <Col lg={8} sm={12}>
            <Row className="gx-2">
              <Col lg={6} xs={12}>
                <p className="text-paragraph-nue">
                  The first resolution asked the CCPCJ to consider developing,
                  through broad stakeholder engagement, a set of model
                  strategies for the elimination of violence against children;
                  the second called for the UNODC to convene an “open-ended
                  intergovernmental expert group meeting” with a view to
                  drafting them. She then went on to personally chair that
                  meeting, which was attended by international experts and UN
                  and Member State representatives.
                  <br />
                  <br />
                  Consisting of what she later called a “rather difficult and
                  delicate negotiation process”, this three-day meeting, held at
                  Bangkok's Shangri-La Hotel in February 2014, built on the
                  original recommendations of the joint report and the
                  groundwork laid at a subsequent consultation meeting. The
                  hard-won outcome was a soft law instrument that those in
                  attendance believed all Member States would be able to support
                  — 17 Model Strategies spanning three broad categories.
                  Addressing the practical aspects of child protection, these
                  ranged from the general (such as the prohibition by law of all
                  forms of violence against children) to those targeted at
                  specific groups (such
                </p>
              </Col>
              <Col lg={6} xs={12}>
                <p className="quote-text" style={{ fontStyle: "normal" }}>
                  “The multidimensional nature of violence against children
                  calls for a multifaceted response and requires a range of
                  strategies to respond to the diverse manifestations of
                  violence and the various settings in which it occurs, both in
                  private and in public life.”
                </p>
                <p className="quote-sub-text">
                  UNODC's Introducing the United Nations Model Strategies and
                  Practical Measures on the Elimination of Violence against
                  Children in the Field of Crime Prevention and Criminal
                  Justice, February 2015
                </p>
              </Col>
            </Row>
            <Row className="gy-2 gy-sm-3 mt-2 mt-lg-1">
              <Col
                lg={12}
                // className="div-image-right"
              >
                <img
                  className="image"
                  src="./images/page-38-content/image-38-01.webp"
                ></img>
              </Col>
              <Col lg={12} sm={8} className=" d-block d-lg-none ">
                <p className="text-caption-image-nue">
                  Princess Bajrakitiyabha leads an expert group meeting at the
                  Shangri-La Hotel in Bangkok, February 2014. The United Nations
                  Model Strategies for Preventing and Responding to Violence
                  Against Children were drafted at this gathering of
                  international specialists.
                </p>
              </Col>
            </Row>
          </Col>
          <Col lg={4} sm={12} style={{ position: "relative" }}>
            <p className="text-paragraph-nue">
              as children vulnerable to criminal gangs). Other strategies
              limited or prohibited certain punishments, or suggested that child
              victims of violence should have access to complaint mechanisms,
              counseling and support services.
              <br />
              <br />
              This multilateral drafting process, which culminated with a
              high-level panel discussion and exhibition on the margins of the
              69<sup>th</sup> session of the General Assembly, organized by the
              Thai and Austrian governments, proved successful. In December
              2014, the Model Strategies — officially known as the 'United
              Nations Model Strategies and Practical Measures on the Elimination
              of Violence against Children in the Field of Crime Prevention and
              Criminal Justice' — were officially approved by the United Nations
              General Assembly. The timing was symbolic: 30 November, 2014
              marked the 25<sup>th</sup> anniversary of the UN Convention on the
              Rights of the Child (CRC). For Princess Bajrakitiyabha, and many
              others involved, the Model Strategies represented not just a
              simple reaffirmation of commitment to these profoundly important
              principles but the mainstreaming of a system-wide, rights-based
              approach to violence against children. Article 19 of the CRC
              states that nations shall take “appropriate measures” to protect
              children “from all forms of physical or mental violence, injury or
              abuse, neglect or negligent treatment, maltreatment or
              exploitation”. In the Model Strategies, the international
              community now had a comprehensive framework to develop and
              implement the necessary legal, policy and operational reform to do
              precisely that.
            </p>
            <p className="text-caption-image-nue d-none d-lg-flex ">
              Princess Bajrakitiyabha leads an expert group meeting at the
              Shangri-La Hotel in Bangkok, February 2014. The United Nations
              Model Strategies for Preventing and Responding to Violence Against
              Children were drafted at this gathering of international
              specialists.
            </p>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}
