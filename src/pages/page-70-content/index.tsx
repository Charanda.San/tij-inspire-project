import {
  CaptionImagePosition,
  ContainerAfterWorld,
  TextBrown,
} from "@/components";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page70Content() {
  const paragraph1: string[] = [
    `The 1955 UN Standard Minimum Rules for the Treatment of Prisoners are updated and renamed the
    Nelson Mandela Rules`,
    `Model Prison program is initiated by the TIJ and Thai Department of Corrections`,
    `Presides over the opening of the International Conference on Alternative Development (ICAD II)`,
  ];
  const paragraph2: any[] = [
    `Princess Bajrakitiyabha creates a sports club, Bounce Be Good (BBG), for young offenders and vulnerable children`,
    `Presides over the First ASEAN Conference on Crime Prevention and Criminal Justice in Bangkok`,
    <>
      The 13<sup>th</sup> United Nations Congress on Crime Prevention and
      Criminal Justice is held in Doha, Qatar
    </>,
  ];
  const year2016: string[] = [
    `Participates in the High-Level Seminar on Promoting
the Rule of Law for Sustainable Development Cooperation in Beijing as the official guest of the Chinese government`,
    `Appointed as the Honorary Advisor of the China-ASEAN Legal Research Center`,
    `Appointed as an Honorary Professor of Beijing Normal University`,
  ];
  const year2017: string[] = [
    `Acting Provincial Chief Public Prosecutor for Civil Rights Protection, Legal Aid and Legal Execution, Office of Rayong Public Prosecution`,
    `Works as Expert Public Prosecutor, Office of Public Prosecution Region 2`,
    `Receives an Honorary Doctorate degree in Law from Wuhan University`,
  ];
  const paragraph3: string[] = [
    `Thailand’s Penitentiary Act is updated so that it is more consistent with international standards`,
    `First BBG Princess Cup tournament is held`,
    `Accepts role as UNODC Goodwill Ambassador on the
Rule of Law and Criminal Justice for Southeast Asia`,
  ];
  const year2018: string[] = [
    `Leads the study visit program on Empowering Vulnerable Communities and Women for Sustainable Development at Chiang Mai and Chiang Rai`,
    `Official visit to the ASEAN Secretariat, Jakarta, Indonesia`,
  ];

  return (
    <ContainerAfterWorld dividerCaption="Appendices">
      <Row s className="py-1 px-2 scroll1 timeline-responsive">
        <Col xs={12} lg={3} className="grid-divider">
          <TextBrown bullets={paragraph1} />
          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-70-content/image-70-01.webp"
              className="image-page"
            />
          </div>
          <TextBrown bullets={paragraph2} />
          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-70-content/image-70-02.webp"
              className="image-page"
            />
          </div>
        </Col>
        <Col xs={12} lg={3} className="grid-divider">
          <TextBrown year="2016" bullets={year2016} />
          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-70-content/image-70-03.webp"
              className="image-page"
            />
          </div>
          <TextBrown year="2017" bullets={year2017} />
        </Col>
        <Col xs={12} lg={3} className="grid-divider">
          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-70-content/image-70-04.webp"
              className="image-page"
            />
          </div>
          <TextBrown bullets={paragraph3} />
          <TextBrown year="2018" bullets={year2018} />

          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-70-content/image-70-05.webp"
              className="image-page"
            />
          </div>
        </Col>
        <Col xs={12} lg={3} className="grid-divider">
          <TextBrown
            bullets={[
              "Visits Qatar to promote Thailand–Qatar cooperation on youth crime prevention through sports",
            ]}
          />

          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-70-content/image-70-06.webp"
              className="image-page"
            />
          </div>
          <TextBrown
            year="2019"
            bullets={[
              "Thailand drafts a United Nations resolution entitled ‘Integrating Sport into Youth Crime Prevention and Criminal Justice Strategies’",
            ]}
          />

          <div style={{ marginBottom: "10px" }}>
            <img
              src="./images/page-70-content/image-70-07.webp"
              className="image-page"
            />
          </div>
        </Col>
      </Row>
    </ContainerAfterWorld>
  );
}
