import React from "react";
import { ContainerLight } from "@/components";
import { Col, Row } from "react-bootstrap";

export default function Page37content() {
  return (
    <>
      <ContainerLight dividerCaption="THE RIGHTS OF CHILDREN">
        <Row className="gy-3 ">
          <Col xl={6} sm={12} xs={12} >
            <Row className="gx-2 gy-2 gy-sm-3 gx-sm-3">
              <Col xs={12} sm={6} lg={5} >
                <img
                  src="./images/page-37-content/image-37-01.webp"
                  className="image-page"
                />
              </Col>
              <Col xs={12} sm={6} lg={7}>
                <img
                  src="./images/page-37-content/image-37-02.webp"
                  className="image-page"
                />
              </Col>
              <Col xs={12} sm={12} lg={3} className="order-last order-lg-2 order-sm-last">
                <p className="text-caption-image-nue">
                  In order to help young offenders escape the detrimental
                  situations they may be trapped in, and to provide them
                  opportunities, life at Chiang Mai’s Juvenile Vocational
                  Training Centre revolves around hands-on lessons in woodwork
                  and engineering, among other practical trades. These have the
                  potential to give them a sustainable income upon release.
                </p>
              </Col>
              <Col xs={12} sm={12} lg={9}>
                <img
                className="image-page"
                  src="./images/page-37-content/image-37-03.webp"
                />
              </Col>
            </Row>
          </Col>
          <Col xl={6} sm={12} xs={12} className="text-lg-end" >
            <img
              src="./images/page-37-content/image-37-04.webp"
              className="image-page"
            />
          </Col>
        </Row>
      </ContainerLight>
    </>
  );
}
