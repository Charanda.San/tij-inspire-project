import { ContainerOrange } from "@/components";
import { Inpractice } from "@/components/Widget";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const Page46Content = () => {
  return (
    <ContainerOrange>
      <Row className="h-100 gy-3 gy-lg-0">
        <Col lg={6}>
          <Row className="gx-2 gy-2">
            <Col lg={12} className="pb-3 px-2" >
              <img src="./images/page-46-content/page-46.webp" alt="" />
            </Col>
            <Col xs={12} lg={4} className="px-2 order-lg-1 order-3 ">
              <p className="text-description-img">
                While the routine is similar to that of an average school, there
                is an informality to life at Ban Pranee. Girls are allowed to
                speak openly with their teachers and encouraged to look out and
                care for one another, as in a healthy family.
              </p>
            </Col>
          </Row>
        </Col>
        <Col lg={6} className="h-100 scroll1">

          <Inpractice
            title="BAN PRANEE JUVENILE VOCATIONAL TRAINING CENTRE FOR GIRLS"
            img="./images/page-46-content/page-46-1.webp"
            paragraph={[
              <>
                <p>
                  <span className="first-letter-orange">J</span>
                  udging from the smiles and laughter that fill the classroom at
                  Ban Pranee Juvenile Vocational Training Centre for Girls in
                  the province of Nakhon Pathom near Bangkok, you would never
                  know that many of the 48 pupils here suffer from depression as
                  well as behavioral and emotional challenges, and that some
                  were once homeless.
                </p>
                <p>
                  What makes this Juvenile Vocational Training Centre unique is
                  that it focuses on supporting these girls through the efforts
                  and treatments provided by social workers, experts and
                  psychologists, who apply individually tailored programs
                  created to match each case and then follow up and monitor the
                  progress. Common issues among the girls include depression or
                  difficulty controlling their anger or aggression. Activities
                  at Ban Pranee include both solo and group therapy aimed at
                  making the youth more aware and understanding of their own
                  emotional or behavioral issues, and eventually leading to
                  long-lasting solutions.
                </p>
              </>,
              <>
                <p>
                  “Do you know that on Tuesday I did not want to go to class
                  because of stress?” one youth in Ban Pranee admitted to her
                  teacher. Even that remark shows how the environment at Ban
                  Pranee is different from that of other schools. The girls can
                  speak openly with their teachers. Authority figures are
                  referred to by the Thai pronoun of mother or aunt, and the
                  girls call each other sister. The focus is not on competition but on learning, understanding, cooperation and
                  compassion for the challenges at hand. The social life of the
                  home is positive. Everyone is kind and cares for each other
                  like in a family.
                </p>
                <p>
                  This environment is also reflected in the girls’ studies,
                  which appear to inspire genuine enjoyment not tedium. Besides
                  the mental health treatments, the girls receive both academic
                  and vocational training. Classes include music, sports,
                  beauty, cooking, sewing, computers, art, Thai massage and
                  agriculture. There is special focus on creating career
                  opportunities under the slogan “creating people, creating
                  jobs, creating careers” so that the young women of Ban Pranee
                  can move on from their difficult pasts and enjoy a brighter
                  future.
                </p>
              </>,
            ]}
          />
          {/* <img src="./images/page-46-content/page-46-1.webp" style={{height:'auto',objectFit:'inherit'}} alt="" /> */}
        </Col>
      </Row>
    </ContainerOrange>
  );
};

export default Page46Content;
