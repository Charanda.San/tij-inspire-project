import { ContainerOrange } from "@/components";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import "./page-34-content.css";

const Page34Content = () => {
  return (
    <ContainerOrange>
      <Row className="gx-3 gy-3">
        <Col lg={6}>
          <Row className="gy-3 gx-3">
            <Col lg={12}>
              <div className="wrapper-content-group-39">
                <div className="text-description-img">
                  <p>
                    Other vocational skills include hairdressing, sewing,
                    cooking and massage. Earnings can be saved, sent home or
                    used to buy food and items using the prison’s cashless
                    payment system. The prison also offers a range of classes,
                    including dhamma teaching, meditation, yoga, mindfulness,
                    watercolor painting and poetry.
                  </p>
                </div>
                <img src="./images/page-34-content/page-34.webp" alt="" />
              </div>
            </Col>
            <Col lg={6} sm={6} xs={12}>
              <img src="./images/page-34-content/page-34-1.webp" alt="" />
            </Col>
            <Col lg={6} sm={6} xs={12}>
              <img src="./images/page-34-content/page-34-2.webp" alt="" />
            </Col>
          </Row>
        </Col>
        <Col lg={6}>
          <Row className="gx-3 gy-3">
            <Col lg={12}>
              <img src="./images/page-34-content/page-34-3.webp" alt="" />
            </Col>
            <Col lg={6} sm={6} xs={12}>
              <img src="./images/page-34-content/page-34-4.webp" alt="" />
            </Col>
            <Col lg={6} sm={6} xs={12}>
              <img src="./images/page-34-content/page-34-5.webp" alt="" />
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerOrange>
  );
};

export default Page34Content;
