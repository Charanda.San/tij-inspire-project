import { ContainerOrange } from "@/components";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page45Content() {
  return (
    <ContainerOrange>
      <Row className="gy-3">
        <Col xs={12} lg={6} sm={12}>
          <Row className="gx-2 gy-3">
            <Col lg={12}>
              <img
                src="./images/page-45-content/image-45-01.webp"
                className="image-page"
              />
            </Col>
            <Col xs={12} sm={4} lg={3}>
              <div className="my-2">
                <hr className="divider-short-orange" />
              </div>
              <p className="text-caption-image-nue">
                The annual BBG Princess Cup is highly publicized within
                Thailand, attracting the attention of media outlets and the
                general public, as well as financial sponsorship from major
                corporations. Participants include child victims of crime and
                children from impoverished areas with high crime rates, as well
                as children from the country’s juvenile detention facilities.
              </p>
            </Col>
            <Col xs={12} sm={8} lg={9}>
              <img
                src="./images/page-45-content/image-45-02.webp"
                className="image-page"
              />
            </Col>
          </Row>
        </Col>
        <Col xs={12} lg={6} sm={12}>
          <div style={{ height: "100%" }}>
            <img
              src="./images/page-45-content/image-45-03.webp"
              className="image-page"
            />
          </div>
        </Col>
      </Row>
    </ContainerOrange>
  );
}
