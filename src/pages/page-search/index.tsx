import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import algoliasearch from "algoliasearch/lite";
import { InstantSearch, SearchBox, Hits, Snippet, Highlight } from "react-instantsearch-dom";
import { pages, IPages } from "./pages";

const SearchPage = () => {
  const { search } = useParams();

  const searchClient = algoliasearch(
    import.meta.env.VITE_ALGOLIA_APP_ID,
    import.meta.env.VITE_ALGOLIA_API_KEY
  );


  const handleClickNavigate = (path: string) => {
    window.location.href = path;
    // navigate(path);
  };

  const findTheParagraph = (content: string, word: string): string => {
    const transLower = content.toLowerCase();
    const pos = transLower.indexOf(word.toLowerCase());
    const left = transLower.slice(0, pos + 1).search(/\S+$/);
    const right = transLower.toLowerCase().slice(pos).search(/\s/);
    // console.log(word.toLowerCase())
    if (right < 0) {
      return transLower.slice(left - 100);
    } else {
      return transLower.slice(
        left === 0 ? left : left - 100,
        right + pos + (left === 0 ? 200 : 100)
      );
    }
  };

  const HitsCustom = (props: any) => {
    const {hit} = props
    return (
      <div>
        <a
          onClick={(e: any) => handleClickNavigate(hit?.path)}
          style={{ cursor: "pointer", display: "inline-block" }}
        >
          <h6
            style={{
              textTransform: "uppercase",
              color: "rgb(174, 80, 35)",
              fontFamily: '"Lora", serif',
            }}
          >
            {hit.page}
          </h6>
        </a>
        <p style={{ fontFamily: '"PT Serif", serif', maxWidth: "500px" }}>
          <Snippet attribute="content" hit={hit}/>
        </p>
      </div>
    );
  };

  return (
    <InstantSearch indexName="tij_search" searchClient={searchClient}>
      <Container className="h-100 py-5">
        <div className="h-100 overflow-auto">
          <h6
            style={{
              fontFamily: '"PT Serif", serif',
              fontWeight: 700,
              fontSize: "1.25rem",
            }}
          >
            Results for "{search}"
          </h6>
          <div className="d-none">

          <SearchBox defaultRefinement={search}/>
          </div>
          <Hits hitComponent={HitsCustom} />
          {/* {result.length > 0 ? (
          result.map((item: IPages) => {
            return (
              <div key={item.page}>
                <a
                  onClick={(e: any) => handleClickNavigate(item.path)}
                  style={{ cursor: "pointer", display: "inline-block" }}
                >
                  <h6
                    style={{
                      textTransform: "uppercase",
                      color: "rgb(174, 80, 35)",
                      fontFamily: '"Lora", serif',
                    }}
                  >
                    {item.page}
                  </h6>
                </a>
                <p
                  style={{ fontFamily: '"PT Serif", serif', maxWidth: "500px" }}
                >
                  {findTheParagraph(
                    item.content,
                    (search as string).toLowerCase()
                  )}
                </p>
              </div>
            );
          })
        ) : (
          <>
            <h5>Not found result for "{search}"</h5>
          </>
        )} */}
        </div>
      </Container>
    </InstantSearch>
  );
};

export default SearchPage;
