import { ContainerOrange } from "@/components";
import React from "react";
import { Col, Row } from "react-bootstrap";

export default function Page47Content() {
  return (
    <ContainerOrange>
      <Row>
        <Col xs={12} lg={6} sm={12}>
          <div style={{ height: 400 }}>
            <img
              src="./images/page-47-content/image-47-01.webp"
              className="image-page"
            />
          </div>
          <Row className="mt-2">
            <Col xs={12} sm={6} lg={4}>
              <div className="my-2">
                <hr className="divider-short-orange" />
              </div>
              <p className="text-caption-image-nue">
                Vocational training classes at Ban Pranee include agriculture,
                construction, beauty and Thai massage. Underpinning the focus on
                personal development is a belief that new skills can provide a
                basis for further study or a career in the future.
              </p>
            </Col>
          </Row>
        </Col>
        <Col xs={12} lg={6} sm={12} className="d-none d-sm-none d-lg-block">
          <Row className="gx-3 ">
            <Col xs={12} lg={6} sm={12}>
              <Row className="gy-3 ">
                <Col xs={12} lg={12} sm={12}>
                  <div style={{ height: 300 }}>
                    <img
                      src="./images/page-47-content/image-47-02.webp"
                      className="image-page"
                    />
                  </div>
                </Col>
                <Col xs={12} lg={12} sm={12}>
                  <div style={{ height: 200 }}>
                    <img
                      src="./images/page-47-content/image-47-04.webp"
                      className="image-page"
                    />
                  </div>
                </Col>
              </Row>
            </Col>
            <Col xs={12} lg={6} sm={12}>
              <Row className="gy-3">
                <Col xs={12} lg={12} sm={12}>
                  <div style={{ height: 200 }}>
                    <img
                      src="./images/page-47-content/image-47-03.webp"
                      className="image-page"
                    />
                  </div>
                </Col>
                <Col xs={12} lg={12} sm={12}>
                  <div style={{ height: 300 }}>
                    <img
                      src="./images/page-47-content/image-47-05.webp"
                      className="image-page"
                    />
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>

        <Col xs={12} lg={6} sm={12} className="d-block d-sm-block d-lg-none">
          <Row className="p-0 gx-sm-3 gy-sm-3 gy-3 ">
            <Col xs={12} lg={6} sm={12}>
              <Row className="gy-3 gx-sm-3 gy-sm-3">
                <Col xs={12} lg={12} sm={6}>
                  
                    <img
                      src="./images/page-47-content/image-47-04.webp"
                      className="image-page"
                    />
                </Col>
                <Col xs={12} lg={12} sm={6}>
                    
                    <img
                      src="./images/page-47-content/image-47-03.webp"
                      className="image-page"
                    />
                </Col>
              </Row>
            </Col>

            <Col xs={12} lg={6} sm={12}>
              <Row className="gy-3 p-0 gx-sm-3 gy-sm-3">
                <Col xs={12} lg={12} sm={6}>
                      <img
                      src="./images/page-47-content/image-47-02.webp"
                      className="image-page"
                    />
                </Col>
                <Col xs={12} lg={12} sm={6}>
                    <img
                      src="./images/page-47-content/image-47-05.webp"
                      className="image-page"
                    />
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerOrange>
  );
}
