import React from "react";
import { ContentGreen, ImageBackground } from "@/components";
import { IPropsContent } from "@/components/ContentGreen";
import CaptionImagePosition, {
  ICaptionPosition,
} from "@/components/CaptionImagePosition";
import { Col, Row } from "react-bootstrap";

export default function Page56Content() {
  const textP = `In recent years, Her Royal Highness has driven for the tenets of SEP
  to be applied within the Thai prison system, mainly by offering
  support and opportunity to male prisoners nearing release. Aiming to
  reduce recidivism by changing anti-social and criminal attitudes,
  behaviors and lifestyles, this Kamlangjai Project-led initiative
  initially began with pilot`;
  const textSEP = `Philosophy (SEP), as it became known, also encouraged the acquisition
  of knowledge and the cultivation of virtue as a surer path to both
  successful long-term development and happiness. Indeed, his
  recommendations ran counter to the institutional corruption that was
  often seen as hampering Thailand’s progress.`
  const textThisWork = `This work also continues the development legacy of her late
  grandfather, King Bhumibol Adulyadej. During his long reign
  (1946–2016), King Bhumibol Adulyadej strived to impart upon the Thai
  people the benefits of creating self- sufficiency and sustainable
  development through the application of moderation, prudence and
  reasonableness, throughout all facets of life, by individuals,
  families, businesses or communities. Communicated as a foil against
  greed, materialism and the risks posed by global capitalism, this
  Sufficiency Economy`
  const captions: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption:
        "An archival image of the grandfather of Princess Bajrakitiyabha, His Majesty King Bhumibol Adulyadej, who is talking to officers at the Pikun Thong Royal Development Study Centre, one of the late king’s main development projects, located in Narathiwat Province, Southern Thailand.",
    },
    {
      position: "Bottom: ",
      caption:
        "Princess Bajrakitiyabha meets hill-tribe communities at Doi Tung, a mountain in the highlands of Northern Thailand.",
    },
  ];
  const propsContent: IPropsContent = {
    title: (
      <div>
        Development-led Approaches <br /> To Crime Prevention
      </div>
    ),
    textDividerLeft: "inspire",
    textDividerRight: "The Rule of Law and Sustainable Development",
    character: "w",
    subTitle: `hether trying to normalize gender-sensitive prison policy, protect vulnerable children, or cultivate a culture of lawfulness, Her Royal Highness Princess Bajrakitiyabha has long preferred action to
    discussion, concrete examples to abstract concepts. “We can occasionally or professionally indulge ourselves in deep intellectual conversation or debate on what exactly the rule of law means,” she stated in 2016, “but we can pause the thinking and start doing something as well. And by taking action, and reflecting on it, we can deepen our appreciation of the concept and the reality surrounding it.”`,
    paragraphs: [
      <p className="d-none d-lg-block">
        Her belief that the best way to tackle the issue of the rule of law is
        to “put the concept into ‘operation’ from various angles and
        perspectives” was originally derived from her experiences working, both
        within the United Nations’ framework and on-the-ground in Thailand, to
        mainstream the rights of female offenders and children. However, in
        recent years her action-centered approach has also been informed by her
        growing interest and involvement in community empowerment and
        development. Centered on prisoner reintegration strategies and a more
        recent community-based initiative to create alternative opportunities to
        the illicit drug trade, this evolving work feeds into the promotion of
      </p>,
      <>
        <p className="d-block d-lg-none">
          Her belief that the best way to tackle the issue of the rule of law is
          to “put the concept into ‘operation’ from various angles and
          perspectives” was originally derived from her experiences working,
          both within the United Nations’ framework and on-the-ground in
          Thailand, to mainstream the rights of female offenders and children.
          However, in recent years her action-centered approach has also been
          informed by her growing interest and involvement in community
          empowerment and development. Centered on prisoner reintegration
          strategies and a more recent community-based initiative to create
          alternative opportunities to the illicit drug trade, this evolving
          work feeds into the promotion of justice and Sustainable Development
          Goals 16 and 17 while bringing tangible improvements to people’s
          lives.
        </p>
        <p className="d-none d-lg-block">
          justice and Sustainable Development Goals 16 and 17 while bringing
          tangible improvements to people’s lives.
        </p>
        <p className="d-sm-none d-none d-lg-block">
         {textThisWork}
        </p>
        <p className="d-sm-block d-block d-lg-none">
         {`${textThisWork} ${textSEP}`}
         
        </p>
      </>,
      <>
        <p className="d-sm-none d-none d-lg-block">
          {textSEP}
        </p>
        <p className="d-sm-none d-none d-lg-block">{textP}</p>
      </>,
    ],
    componentRightSide: (
      <>
        <Row className="gx-2 gy-2">
          <Col xs={12} sm={12} lg={12}>
            <ImageBackground
              src="./images/page-56-content/image-56-01.webp"
              height={250}
            />
          </Col>
          <Col xs={12} sm={6} lg={4} className="text-paragraph-nue">
            <p className="d-sm-block d-lg-none d-block">{textP}</p>
            <p className="text-paragraph-nue">
              projects at four temporary prisons in the Thai provinces: Khao
              Ragum in Trat Province, Khae Noi in Petchabun Province, Doi Hang
              in Chiang Rai Province, and Khao Plong Temporary Prison in Chai
              Nat Province. At these bucolic open farm-style facilities, male
              inmates selected from nearby prisons on account of their good
              behavior and short time left to serve learn how to farm in order
              to achieve the self-sufficiency goals of SEP.
            </p>
            <p className="text-paragraph-nue">
              As with other aspects of the Kamlangjai Project, partnerships with
              non-governmental organizations and the private sector form an
              intrinsic and vital part of its holistic value chain. Lectures on
              how to apply SEP, work ethics and legal
            </p>
          </Col>
          <Col xs={12} sm={6} lg={5}>
            <ImageBackground
              height={201}
              src="./images/page-56-content/image-56-02.webp"
            />
          </Col>
          <Col xs={12} sm={6} lg={3} className="position-relative">
            <CaptionImagePosition
              style="monster"
              captions={captions}
              position="top"
            />
          </Col>
        </Row>
      </>
    ),
  };

  return (
    <>
      <ContentGreen {...propsContent}></ContentGreen>
    </>
  );
}
