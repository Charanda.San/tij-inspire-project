import { CaptionImagePosition, ContainerLightOnly } from "@/components";
import { ICaptionPosition } from "@/components/CaptionImagePosition";
import React from "react";
import { Col, Row } from "react-bootstrap";
import { ContentImage } from "src/components/Widget";

const Page21Content = () => {
  const captionsTop = (
    <>
      Her Royal Highness attends the 65<sup>th</sup> Session of the United
      Nations General Assembly (UNGA) in 2010 to officially open the ELFI
      exhibition titled “Standard Minimum Rules: A New Horizon for Women
      Prisoners.”
    </>
  );
  const captionBottom = `Princess Bajrakitiyabha signs a “Say No to Violence Against Women” campaign billboard at Hua Lamphong train station in Bangkok, 2008.`;

  const captionsDesk: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: captionsTop,
    },
    {
      position: "Opposite page: ",
      caption: captionBottom,
    },
  ];
  const captionMobile: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: captionsTop,
    },
    {
      position: "Bottom: ",
      caption: captionBottom,
    },
  ];

  const captionImage: ICaptionPosition[] = [
    {
      position: "",
      caption: captionsTop,
    },
  ];
  return (
    <>
      <Row className="h-100 p-0">
        <Col lg={6} sm={12} xs={12} className="scroll2 h-100">
          <ContainerLightOnly>
            <Row className="gx-2 gy-2 text-paragraph-nue">
              <Col lg={4} xs={12} sm={12}>
                <p>
                  Having Thailand oversee the drafting of a progressive new set
                  of actionable UN standards was not enough, of course — if they
                  were to be embraced by the international community, a
                  convincing case needed to be built for them. And so, in the
                  following months, when not busy with her prosecutor duties in
                  Thailand, Her Royal Highness lent her support by travelling,
                  alongside key members of the Thai Ministry of Foreign Affairs
                  and Ministry of Justice, to events and conferences. At many of
                  these, she gave speeches that articulated how women are made
                  vulnerable in the criminal justice system and underscored the
                  cross-cutting nature of how that vulnerability could be
                  mitigated. Most of all, these speeches floated the idea that
                  female perpetrators of crimes are often themselves victims.
                </p>
                <p>
                  Particularly important to the ELFI campaign — Thailand's attempt
                  to legitimize the draft rules by creating broad, multilateral
                  support for them — was the 18<sup>th</sup> session of the CCPCJ,
                  held in Vienna in April 2009. In her opening statement, Her
                  Royal Highness once again presented a cogent case for
                  reviewing the SMRs in light of the singularity of women's
                  issues. She also opened an ELFI Project exhibition alongside
                  UNODC Executive Director Antonio Maria Costa, and joined a
                  meeting at which Thailand's proposal to present the draft
                  rules at the United Nations General Assembly was presented and
                  subsequently co-sponsored by Brazil, the Czech Republic (on
                  behalf of the EU), United States, Canada, Japan and China,
                  among other nation states.
                </p>
                <p>
                  ELFI's advocacy was not simply confined to UN meetings,
                  however. For example, at the 13<sup>th</sup> International
                  Symposium on Victimology, held in Mito, Japan in August 2009,
                  Her Royal Highness gave a carefully calibrated keynote address
                  that, as well as touching on themes such as victim
                  compensation, restorative justice and human trafficking,
                  placed the rights of women prisoners directly under
                </p>
              </Col>
              <Col lg={8} xs={12} sm={12}>
                <Row className="gx-2 gy-2 ">
                  <Col lg={12} xs={12} sm={12}>
                    <div className="position-relative">
                      <img
                        src="./images/page-21-content/page-21.webp"
                        alt=""
                        className="image-page"
                      />
                      <div className="description-image-inside">
                        <CaptionImagePosition
                          style="monster"
                          captions={captionImage}
                          position="top"
                        />
                      </div>
                    </div>
                  </Col>
                  <Col
                    lg={6}
                    xs={12}
                    sm={12}
                    className="order-2 order-lg-1 order-sm-2"
                  >
                    <p>
                      the academic purview of gender-sensitive victimology.
                      “More often than not,” she stated to a room full of
                      criminal justice professionals, “women prisoners are
                      subject to re-victimization while serving their
                      sentences.” She then cited one of the draft rule
                      provisions — ‘women prisoners who report abuse shall be
                      provided immediate protection and support, with full
                      respect to the principle of confidentiality’ — as an
                      “example of how the dynamics of international standards
                      and norms can complement the strengthening of the
                      victim-based approach.”
                    </p>
                    <p>
                      It is exactly this type of lateral thinking and bold,
                      cross-disciplinary consensus-building that helped smooth
                      the path of the ‘Draft United Nations Rules for the
                      Treatment of Women Prisoners and Non-Custodial
                    </p>
                  </Col>
                  <Col
                    lg={6}
                    xs={12}
                    sm={12}
                    className="order-3 order-lg-2 order-sm-3"
                  >
                    <p>
                      Measures for Women Offenders’ to the UN General Assembly,
                      which adopted them in December 2010. As Vongthep, a key
                      figure in the ELFI campaign, puts it: “When the Bangkok
                      Rules were adopted, a lot of people who had been working
                      with us said that they had never seen UN norms developed
                      and adopted this quickly: in only two years.”
                      Substantively, the Bangkok Rules had been built through
                      broad international consensus, but for him, and many
                      others, the credit for their swift progress through the
                      legislative machinery of the United Nations belongs to one
                      person. “In my opinion, had it not been for the wisdom,
                      support, initiative and gathering power of Her Royal
                      Highness, we would have gotten nowhere. We would never
                      have even come close.”
                    </p>
                  </Col>
                  {/* <Col
                    lg={2}
                    xs={12}
                    sm={12}
                    className="order-1 order-lg-3 order-sm-1 "
                  >
                    <div className="d-none d-lg-block d-sm-none">
                      <CaptionImagePosition
                        style="monster"
                        captions={captionsDesk}
                        position="top"
                      />
                    </div>
                    <div className="d-block d-lg-none d-sm-block mt-2">
                      <CaptionImagePosition
                        style="monster"
                        captions={captionMobile}
                        position="top"
                      />
                    </div>
                  </Col> */}
                </Row>
              </Col>
            </Row>
          </ContainerLightOnly>
        </Col>
        <Col
          lg={6}
          style={{ backgroundColor: "#B49479" }}
          className="scroll2 h-100"
        >
          <ContentImage
            title="FIGHTING VIOLENCE AGAINST WOMEN"
            img={{
              path: "./images/page-21-content/page-21-1.webp",
              description: captionBottom,
            }}
            paragraph={[
              <>
                <p>
                  On 5 October, 2008, Her Royal Highness Princess Bajrakitiyabha
                  was appointed the United Nations Development Fund for Women
                  (UNIFEM) Goodwill Ambassador for Thailand. The appointment was
                  a direct outcome of her Kamlangjai Project work and her
                  diplomatic experience. Her Royal Highness’s sympathetic stance
                  towards female inmates and, more importantly, her proactive
                  and productive efforts to empower them across both grassroots
                  and transnational contexts made her an attractive choice for
                  the position.
                </p>
                <p>
                  Her first task entailed working alongside various cross-sector
                  stakeholders to encourage the Thai public to take a stand
                  against domestic violence. Comprising talk show appearances, a
                  bicycle rally, TV and billboard advertising, and a role model
                  contest, UNIFEM’s “Say No to Violence Against Women” campaign
                  went on to garner over 3 million signatures a number well in
                  excess of the original target.
                </p>
                <p>
                  This ambitious awareness-raising drive came one year after the
                  introduction of a critical piece of Thai legislation: the
                  Victims of Domestic Violence Protection Act B.E. 2550 (2007).
                  Fearing a low level of understanding of its full judicial and
                  societal ramifications,
                </p>
              </>,
              <>
                <p>
                  both among the public and government officials, Her Royal
                  Highness complemented the nationwide campaign by commissioning
                  a seminar on the act and sharing her opinions at it.
                </p>
                <p>
                  On the back of the campaign’s success, she also launched the
                  ‘Youth Say No to Violence Against Women Project’ in May 2009.
                  In collaboration with Thailand’s Office of Basic Education
                  Commission (OBEC), Ministry of Education and Ministry of
                  Justice, action plans for creating campaigns and activities
                  that sensitize students to the ideas, perceptions, values and
                  behaviors surrounding this issue were launched in eight
                  schools nationwide.
                </p>
                <p>
                  Galvanizing different sectors of Thai society, Her Royal
                  Highness’s work to eliminate violence against women was
                  singled out, along with her prison reform work, in April 2009,
                  when she became just the third person to receive an Award of
                  Recognition medal from the UNODC. It is an area in which she
                  remains active to this day, both in her continued role as a UN
                  Women Goodwill Ambassador, and through her work as a
                  prosecutor and an outspoken proponent of gender-sensitive
                  judicial systems. As she said in 2009, “Legal measures are a
                  key approach to preventing and solving this chronic problem.”
                </p>
              </>,
            ]}
          />
        </Col>
      </Row>
    </>
  );
};

export default Page21Content;
