import { CaptionImagePosition, ContainerAfterWorld } from "@/components";
import { ICaptionPosition } from "@/components/CaptionImagePosition";
import { ContentImage } from "@/components/Widget";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const Page28Content = () => {
  const captions: ICaptionPosition[] = [
    {
      position: "Top and bottom left: ",
      caption: `Corrections staff on duty at Chiang Mai Women Correctional Institution.`,
    },
    {
      position: "Bottom right: ",
      caption: `Chanthaburi Provincial Prison. Training prison officers how to apply the Bangkok Rules and getting their buy-in have been crucial to their successful implementation.`,
    },
  ];
  const section4 = `Rules – which were adopted by the UN General Assembly
  later that year – was accompanied by a declaration calling
  for the Commission on Crime Prevention and Criminal
  Justice to consider convening an intergovernmental expert
  group on the revision of the SMRs`
  return (
    <ContainerAfterWorld dividerCaption="THE RIGHTS OF WOMEN">
      <Row className="gy-sm-3 gy-2 h-100">
        <Col lg={6} className="scroll1 h-100">
          <Row className="gx-2 gy-sm-3 gy-2 h-100 overflow-auto">
            <Col lg={8}>
              <ContentImage
                title="THE RISE OF THE MANDELA RULES"
                // img="./images/page-21-content/page-21-1.webp"
                paragraph={[
                  <>
                    <p>
                      The diplomatic success of the Bangkok Rules in 2010 did
                      more than just galvanize global attention on female
                      prisoners. As Her Royal Highness Princess Bajrakitiyabha
                      later pointed out, they also triggered critical momentum
                      on the broader question of the treatment of all prisoners.
                    </p>
                  </>,

                  <>
                    <p className="d-lg-block d-none d-sm-none">
                     {section4}
                    </p>
                  </>,
                  {
                    col: 12,
                    content: (
                      <>
                        <div className="d-flex mb-3">
                          <div
                            style={{ width: "80%", color: "#FFFFFF" }}
                            className="d-block mx-auto px-0 px-lg-3"
                          >
                            <p
                              className="text-center "
                              style={{
                                fontSize: "16px",
                                fontFamily: "Neue Haas Unica",
                              }}
                            >
                              “No one truly knows a nation until one has been
                              inside its jails. A nation should not be judged by
                              how it treats its highest citizens but its lowest
                              ones.”
                            </p>
                            <div
                              className="d-block ms-auto"
                              style={{ width: "60%" }}
                            >
                              <p
                                className="text-center"
                                style={{
                                  fontSize: "12px",
                                  fontFamily: "Neue Haas Unica",
                                  fontStyle: "italic",
                                }}
                              >
                                –Nelson Mandela
                              </p>
                            </div>
                          </div>
                          <div>
                            <img
                              src="./images/page-28-content/page-28.webp"
                              style={{ height: "110px" }}
                              alt=""
                            />
                          </div>
                        </div>
                      </>
                    ),
                  },
                  <>
                    <p>
                      Created amid the renewed international unity and cautious
                      optimism of the post-World War II period, the 1955 UN
                      Standard Minimum Rules for the Treatment of Prisoners
                      (SMRs) were tremendously valuable. They helped shape
                      corrections laws, policies and practices in Member States
                      all over the world. Yet for many stakeholders impacted by
                      or working with them in the 21<sup>st</sup> Century, they
                      were increasingly antiquated both on paper and in
                      practice. This was because, as Thailand’s Enhancing Lives
                      of Female Inmates (ELFI) campaign had argued, the SMRs
                      took no account of the many advancements in international
                      human rights law and correctional science since 1955.
                    </p>
                    <p className="d-none d-lg-block">
                      A multinational movement to revise the SMRs gained
                      traction at the Twelfth United Nations Congress on Crime
                      Prevention and Criminal Justice in Salvador, Brazil in
                      April 2010. Here the congress’s endorsement of the
                    </p>
                    <p className="d-block d-lg-none">
                      A multinational movement to revise the SMRs gained
                      traction at the Twelfth United Nations Congress on Crime
                      Prevention and Criminal Justice in Salvador, Brazil in
                      April 2010. Here the congress’s endorsement of the Part of
                      the Salvador Declaration, this request set in motion a
                      four-year drafting, revision and adoption process that
                      culminated with the UN General Assembly adopting the
                      updated rules in December 2015. These new SMRs were named
                      the Mandela Rules, “to honor”, as the UN stated, “the
                      legacy of the late President of South Africa, Nelson
                      Rolihlahla Mandela, who spent 27 years in prison in the
                      course of his struggle for global human rights, equality,
                      democracy and the promotion of a culture of peace.”
                    </p>
                  </>,
                  <>
                    <p className="d-none d-lg-block">
                      Part of the Salvador Declaration, this request set in
                      motion a four-year drafting, revision and adoption process
                      that culminated with the UN General Assembly adopting the
                      updated rules in December 2015. These new SMRs were named
                      the Mandela Rules, “to honor”, as the UN stated, “the
                      legacy of the late President of South Africa, Nelson
                      Rolihlahla Mandela, who spent 27 years in prison in the
                      course of his struggle for global human rights, equality,
                      democracy and the promotion of a culture of peace.”
                    </p>
                    <p>
                      Today, the Mandela Rules – with their many evidence-based
                      measures designed at minimizing harm and maximizing
                      rehabilitation of the general prison population – are held
                      up alongside the supplementary, female offender-focused
                      Bangkok Rules and the United Nations Standard Minimum
                      Rules for Non-custodial Measures, or Tokyo Rules, as the
                      standards that all criminal justice systems should aspire
                      to satisfy.
                    </p>
                  </>,
                ]}
              />
            </Col>
            <Col lg={4}>
              <div className="text-paragraph-nue">
                <p>
                  the TIJ to benchmark levels of Bangkok Rules understanding and
                  the gaps in implementation in Thailand and ASEAN member
                  countries. With that concrete information, training modules
                  for prison staff were developed and, in 2016, training
                  sessions, led by TIJ’s in-house experts and independent
                  advisors, introduced. Concurrently, academic research into
                  incarcerated women’s backgrounds and pathways to prison –
                  called for by Rules 67-69 of the Bangkok Rules – was conducted
                  by the TIJ in various ASEAN countries, Including the
                  Philippines, Indonesia, Cambodia and Thailand.
                </p>
                <p>
                  All these early efforts paid off. In 2016, TIJ succeeded in
                  applying for and becoming the latest member of the UN-PNI, an
                  affiliation that is instrumental in helping the TIJ fulfill
                  its mandate to implement the Bangkok Rules and in carrying out
                  other missions.
                </p>
                <p>
                  Within Thailand, the procedural changes the Bangkok Rules have
                  brought, especially in women’s wings within predominantly male
                  prisons, have been profound. Initially, however, the barriers
                  to implementation were challenging.
                </p>
                <p>
                  According to Archaree Srisunakhua, director of Chiang Mai
                  Women Correctional Institution, there was some resistance
                  within Thailand’s Department of Corrections in the beginning,
                  especially among prison staff. “Even though the officers were
                  usually women, there were questions such as, ‘Why are we doing
                  this?’. For example, the search protocol in the Bangkok Rules
                  says that a woman shall have a closed, designated area for
                  searches. But back then, officers were asking why that is
                  necessary, because they are already within a woman’s facility
                  where there are usually no males present.”
                </p>
                <p>
                  Her Royal Highness has, on her many visits to Thai women’s
                  prisons, helped to encourage buy-in among staff by giving her
                  thoughts on the utility and practical benefits of the Bangkok
                  Rules. “In places the Princess visited and gave direct advice,
                  change tended to happen very quickly,” recalls Archaree. And
                  while the TIJ – a public organization with no jurisdiction
                  over prison policy – can itself only encourage, not force,
                  compliance with the Bangkok Rules, the Thai government has
                  made substantive
                </p>
              </div>
            </Col>
          </Row>
        </Col>
        <Col lg={6} className="h-100">
          <Row className="gy-lg-2 gx-lg-2 gy-sm-3 gy-2">
            <Col lg={10} sm={12} xs={12} className=" ">
              <img src="./images/page-28-content/page-28-4.webp" alt="" />
            </Col>
            <Col
              lg={2}
              className="position-relative d-sm-none d-none d-lg-block "
            >
              <CaptionImagePosition
                style="monster"
                captions={captions}
                position="bottom"
              />
            </Col>
            <Col sm={12} className=" d-block d-lg-none d-sm-block ">
              <CaptionImagePosition
                style="monster"
                captions={captions}
                position="top"
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </ContainerAfterWorld>
  );
};

export default Page28Content;
