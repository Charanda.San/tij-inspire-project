import {
  CaptionImagePosition,
  ContainerLightOnly,
  QuoteImage,
} from "@/components";
import { ICaptionPosition } from "@/components/CaptionImagePosition";
import { ContentImage, WrapperContent } from "@/components/Widget";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import "./page-59-content.css";

const Page59Content = () => {
  const captions: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: `Princess Bajrakitiyabha fulfilling her duties as UNODC Goodwill Ambassador, 2018.`,
    },
    {
      position: "Right: ",
      caption: `Barista training is a key vocational program offered by the Kamlangjai Project.`,
    },
    {
      position: "Opposite page:",
      caption: `Princess Bajrakitiyabha with executives participating in the TIJ’s Rule of Law and Development Program, 2017.`,
    },
  ];
  const captionsTablet: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: `Princess Bajrakitiyabha fulfilling her duties as UNODC Goodwill Ambassador, 2018.`,
    },
    {
      position: "Right: ",
      caption: `Barista training is a key vocational program offered by the Kamlangjai Project.`,
    },
    {
      position: "Bottom:",
      caption: `Princess Bajrakitiyabha with executives participating in the TIJ’s Rule of Law and Development Program, 2017`,
    },
  ];
  const captionsMobile: ICaptionPosition[] = [
    {
      position: (
        <>
          The first:&nbsp;&nbsp;
        </>
      ),
      caption: `Princess Bajrakitiyabha fulfilling her duties as UNODC Goodwill Ambassador, 2018.`,
    },
    {
      position: (
        <>
          The second:&nbsp;&nbsp;
        </>
      ),
      caption: `Barista training is a key vocational program offered by the Kamlangjai Project.`,
    },
    {
      position: "Bottom:",
      caption: `Princess Bajrakitiyabha with executives participating in the TIJ’s Rule of Law and Development Program, 2017`,
    },
  ];
  return (
    <>
      <Row className="gx-2 gy-2 h-100">
        <Col lg={6} className="scroll1 h-100">
          <ContainerLightOnly>
            <>
              <Row className="gx-2 gy-2 h-100 ">
                <Col lg={12}>
                  <Row className="h-100 gx-lg-2 gy-3">
                    <Col lg={8} sm={12} xs={12}>
                      <img
                        src="./images/page-59-content/page-59.webp"
                        className="image-page"
                        alt=""
                      />
                    </Col>
                    <Col lg={4} sm={12} xs={12}>
                      <Row className="d-flex align-items-center gy-3">
                        <Col lg={12} sm={6} xs={12}>
                          <QuoteImage
                            fontSub="Neue Haas Unica"
                            quote={
                              "“Empowering communities is the best immunity against illicit activities.”"
                            }
                            subQuote={
                              "Her Royal Highness Princess Bajrakitiyabha at the International Conference on Alternative Development 2 (ICAD2), Bangkok, 23 November, 2015"
                            }
                          />
                        </Col>
                        <Col lg={12} sm={6} xs={12}>
                          <img
                            src="./images/page-59-content/page-59-1.webp"
                            className="image-page"
                            alt=""
                          />
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>
                <Col lg={12} sm={12} xs={12}>
                  <Row className="gx-2 gy-2">
                    <Col lg={3} xs={12} sm={12}>
                      <div className="d-block d-lg-none d-sm-none">
                        <CaptionImagePosition
                          style="monster"
                          captions={captionsMobile}
                          position="top"
                        />
                      </div>
                      <div className="d-none d-sm-block d-lg-none">
                        <CaptionImagePosition
                          style="monster"
                          captions={captionsMobile}
                          position="top"
                        />
                      </div>
                      <div className="d-none d-lg-block">
                        <CaptionImagePosition
                          style="monster"
                          captions={captions}
                          position="top"
                        />
                      </div>
                    </Col>
                    <Col lg={3} xs={12}>
                      <div className="text-paragraph-nue">
                        <p>
                          States by giving speeches and leading field visits as
                          part of international seminars and workshops held in
                          Thailand.
                        </p>
                        <p>
                          More recently, in collaboration with Thailand’s Office
                          of the Narcotics Control Board, Ministry of Justice
                          and the Mae Fah Luang Foundation behind the Doi Tung
                          Development Project, Her Royal Highness has been
                          working on applying and adapting AD to a new context:
                          a rural community in Chiang Mai that has previously
                          served as a transit point for the multi-billion
                          dollar, transnational trade in synthetic drugs. Still
                          in its proof-of-concept infancy, this project aims to
                          build the resilience of this vulnerable community so
                          that it is resistant to both the trafficking and use
                          of the highly addictive methamphetamines afflicting
                          Thailand and the region at large.
                        </p>
                        <p className="d-lg-block d-none">
                          According to ML Dispanadda Diskul, Chief Executive
                          Officer of the Mae Fah Luang Foundation, her personal
                          vision of a culture of lawfulness is “not something
                          written in a book, but community based, the behavior
                          of
                        </p>

                        <p className="d-block d-lg-none">
                          According to ML Dispanadda Diskul, Chief Executive
                          Officer of the Mae Fah Luang Foundation, her personal
                          vision of a culture of lawfulness is “not something
                          written in a book, but community based, the behavior
                          of citizens or a group of people that agree on something
                          and abide by that agreement.” Like the work of King
                          Bhumibol Adulyadej and the Princess Mother, her work
                          at the nexus of the rule of law and sustainable
                          development revolves around getting people to
                          understand the very foundations of law – a respect for
                          public order and communal responsibility – through
                          empowerment. Additionally, it is, in those contexts
                          where corruption or crime or inequities thrive, also
                          concerned with re-engineering the system so that
                          injustices are eradicated and benefits more justly
                          distributed.
                        </p>
                      </div>
                    </Col>
                    <Col lg={3} xs={12}>
                      <div className="text-paragraph-nue">
                        <p className="d-lg-block d-none">
                          citizens or a group of people that agree on something
                          and abide by that agreement.” Like the work of King
                          Bhumibol Adulyadej and the Princess Mother, her work
                          at the nexus of the rule of law and sustainable
                          development revolves around getting people to
                          understand the very foundations of law – a respect for
                          public order and communal responsibility – through
                          empowerment. Additionally, it is, in those contexts
                          where corruption or crime or inequities thrive, also
                          concerned with re-engineering the system so that
                          injustices are eradicated and benefits more justly
                          distributed.
                        </p>
                        <p>
                          ML Dispanadda likens her expansive criminal justice
                          and rule of law work to a fleet of boats, all
                          navigating different streams and rivers. Some face
                          fast currents, others slow, yet a sense of shared
                          purpose and direction unites them. “You don’t
                          necessarily know where the other boats are exactly,”
                          he says of her broad, rights-based vision, “but at the
                          end of the day, we’ll all meet somewhere in the open
                          seas.”
                        </p>
                      </div>
                    </Col>
                    <Col lg={3} xs={12}>
                      <div className="text-paragraph-nue">
                        <p>
                          Today, the research institute she helped create and
                          serves as Chairperson of the Special Advisory Board
                          for, the Thailand Institute of Justice (TIJ),
                          continues to forge ahead with its own development-led
                          strategies for promoting the rule of law. But whereas
                          Her Royal Highness has historically been focused on
                          direct crime prevention interventions and bringing
                          access to justice to vulnerable groups, the TIJ’s
                          various evolving projects and initiatives also span
                          broader civil society, governmental and private sector
                          engagement and the incubation of new ideas. And yet,
                          “her wisdom shows the way” explains the TIJ’s
                          Executive Director, Dr. Kittipong Kittayarak. Whether
                          it’s a one-week annual workshop for emerging leaders,
                          funding a ‘chatbot’ application for reporting violence
                          against women, or staging a forum that unpacks
                          disruptive technology through a justice lens, all of
                          the TIJ’s partnership-based capacity building and
                          policy advocacy work, he explains, “stems from her
                          leadership and from our association with her.”
                        </p>
                      </div>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </>
          </ContainerLightOnly>
        </Col>
        <Col
          lg={6}
          className="scroll1 h-100"
          style={{ backgroundColor: "#B49479" }}
        >
          <ContentImage
            title="TIJ EXECUTIVE PROGRAM ON THE RULE OF LAW & DEVELOPMENT: RoLD PROGRAM"
            img="./images/page-59-content/page-59-2.webp"
            paragraph={[
              <>
                <p>
                  Her Royal Highness Princess Bajrakitiyabha’s work has directly
                  inspired one of the Thailand Institute of Justice’s most
                  progressive and people-centric initiatives. Since 2016, the
                  think tank’s Rule of Law and Development Program (RoLD) has
                  hosted a multi-stakeholder engagement platform through which
                  working professionals and leaders of the future can explore
                  the full ramifications of – and fine-tune their ability to
                  impact – Sustainable Development Goal 16: Peace, Justice and
                  Strong Institutions.
                </p>
                <p>
                  What began in 2016 as a one-week intensive workshop for
                  emerging leaders, designed in collaboration with the Institute
                  for Global Law and Policy at Harvard University, soon grew
                  into a six-month RoLD program aimed at top executives.
                </p>
                <p>
                  “Because of Goal 16,” explains Executive Director Dr.
                  Kittipong Kittayarak, “the definition of the rule of law has
                  become clearer, as the international community now has to
                  measure it. So that’s created more debate about how to make
                  the rule of law more understandable and more measurable.
                  That’s to our benefit. And we’ve used that benefit to bring
                  top bankers, top economists and top NGO people together.”
                </p>
                <p>
                  Each Thursday for the half-year duration, the high-caliber
                  executives selected from across the broad spectrum of Thai
                  society would gather – sometimes online, sometimes in person –
                  to learn about the relevance and complexity of the rule of law
                  in real-life contexts. The main aim was to encourage
                  engagement and ownership, but in a subtle way. “We wanted them
                  to understand that justice belongs to them – that the rule of
                  law is not simply the rule of lawyers,” says Dr. Kittipong.
                </p>
                <p>
                  The aim was also to get them thinking – to generate ideas.
                  “When they see the realities of the criminal justice system,
                  they begin to see the inequality and structural problems of
                  society,”
                </p>
              </>,
              <>
                <p>
                  Today, this flagship executive program has been paused so that
                  the TIJ can focus less on training and more on partnerships
                  among its 200-plus alumni. “Our goal is trying to have more
                  actions on the ground,” she adds. “Now we are in the stage of
                  trying to get projects moving – putting more resources into
                  grooming ideas and developing prototypes.” The goal is to
                  showcase these pilot projects with a view to starting a chain
                  reaction. “We believe that if we can prove a new idea works
                  the traditional sector will then feel they will become
                  obsolete if they don’t change.”
                </p>
                <p>
                  This arm of the program, known as ‘RoLD in Action’, has
                  already given rise to a diverse array of real-life pilot
                  applications, including new social reintegration partnerships,
                  a project exploring the digital employability of vulnerable
                  groups, and an attempt to promote financial and legal literacy
                  through board games.
                </p>
                <p>
                  A TIJ public forum is also held a few times a year on average.
                  And meanwhile, the original one-week RoLD workshop for
                  emerging leaders, created in collaboration with the Institute
                  for Global Law and Policy at Harvard University, continues.
                  This is aimed at young professionals, government employees,
                  social enterprise entrepreneurs, and business or civil society
                  leaders who are keen to learn about the rule of law and its
                  connection to their work – and also resolved to adapt in light
                  of what they’ve learnt and the professional networks they’ve
                  established at the workshop.
                </p>
                <p>
                  “In Thailand before, people felt powerless about, and distant
                  from, justice issues,” says Dr. Anuwan. “But we feel it’s time
                  for people to absorb and digest them.”
                </p>
              </>,
            ]}
          />
        </Col>
      </Row>
    </>
  );
};

export default Page59Content;
