import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import "./page-14-content.css";
import { CaptionImagePosition, ContainerAfterWorld } from "@/components";
import { ICaptionPosition } from "@/components/CaptionImagePosition";

const Page14Content = () => {
  const captions: ICaptionPosition[] = [
    {
      position: "Top: ",
      caption: `Princess Bajrakitiyabha meets mothers
      and babies on her first visit to Central Women Correctional
      Institution in Bangkok, July 2001.`,
    },
    {
      position: "Bottom: ",
      caption: `Princess Bajrakitiyabha observes an
      ophthalmologist from Rutnin Eye Hospital conducting a check-
      up at Central Women Correctional Institution in Bangkok, as
      part of the 'The Gift of Sight is the Gift of Life' campaign,
      2007.`,
    },
  ];
  return (
    <ContainerAfterWorld dividerCaption="THE RIGHTS OF WOMEN">
      <Row className="gy-lg-3 h-100">
        <Col xl={6} sm={12} xs={12} className="scroll1">
          <Row className="gx-2 gx-lg-2 gy-1 text-roboto-condensed ">
            <Col
              xs={12}
              sm={3}
              lg={3}
              className="position-relative order-1 order-sm-0 order-lg-0"
            >
              <CaptionImagePosition style='monster' captions={captions} position="top" />
            </Col>
            <Col
              xs={12}
              sm={9}
              lg={9}
              className="order-0 order-sm-0 order-lg-0 h-100"
            >
              <div className="mb-2">
                <img
                  src="./images/page-14-content/image-14.webp"
                  className="image-page"
                />
              </div>
            </Col>
            <Col
              xs={12}
              sm={6}
              lg={4}
              className="order-3 order-sm-2 order-lg-2"
            >
              <p>
                In those early years, the Kamlangjai Project also addressed
                wider female inmate welfare. At Central Women Correctional
                Institution, Her Royal Highness was also struck by the numbers
                of elderly inmates with poor eyesight. Not long after she had
                this observation, free eye check-ups, eyeglasses and cataract
                surgeries were being offered by the Thai capital’s esteemed
                Rutnin Eye Hospital. Elsewhere, shortages of basic necessities
                were addressed through initiatives such as the ‘Bra Charity:
                Spirit of Love’ campaign, which saw the public donating over
                70,000 items of underwear to Udon Thani Central Prison.
                Meanwhile, the Kamlangjai Project began facilitating
                gender-sensitive vocational training courses, such as baking,
                basketry, wood carving and hairdressing. Mini-concerts by famous
                singers and Buddhist meditation sessions were also given in some
                facilities in an attempt to raise spirits.
              </p>
              <p className="d-none d-lg-block d-sm-block">
                Today, the Kamlangjai Project has been transformed from an
                informal charity network that supplements the work of the Thai
              </p>
            </Col>
            <Col
              xs={12}
              sm={6}
              lg={4}
              className="order-3 order-sm-2 order-lg-2"
            >
              <p className="d-block d-lg-none d-sm-none">
                Today, the Kamlangjai Project has been transformed from an
                informal charity network that supplements the work of the Thai
                authorities to a multi-component body that now has an official
                administrative unit within the government’s Office of the
                Permanent Secretary, Ministry of Justice. Still driven by Her
                Royal Highness’s vision, who remains Chairperson and CEO, its
                principal focus, these days, is on rehabilitating and
                reintegrating the general prison population – not just women –
                through a Thai-inflected, development-led approach and
                vocational training that draws upon the tenets of moderation
                that underpin the late King Bhumibol Adulyadej’s Sufficiency
                Economy Philosophy <a href="/page-49-chapter">(see Chapter 3)</a>.
              </p>
              <p className="d-none d-lg-block d-sm-block">
                authorities to a multi-component body that now has an official
                administrative unit within the government’s Office of the
                Permanent Secretary, Ministry of Justice. Still driven by Her
                Royal Highness’s vision, who remains Chairperson and CEO, its
                principal focus, these days, is on rehabilitating and
                reintegrating the general prison population – not just women –
                through a Thai-inflected, development-led approach and
                vocational training that draws upon the tenets of moderation
                that underpin the late King Bhumibol Adulyadej’s Sufficiency
                Economy Philosophy <a href="/page-49-chapter">(see Chapter 3)</a>.
              </p>
              <p>
                But while its scope has become broader, some things have stayed
                the same. One lasting characteristic of the Kamlangjai Project
                is its dependence on partnerships. Over the years, it has
                enlisted support from a number of charities, companies and
                individuals, from farmers to entertainment figures. According to
                former prison executive director Angkhaneung Lepnak, the model
                for these collaborations has evolved naturally over time.
                “Central Women Correctional Institution
              </p>
            </Col>
            <Col
              xs={12}
              sm={12}
              lg={4}
              className="order-2 order-sm-2 order-lg-2"
            >
              <img
                src="./images/page-14-content/image-14-1.webp"
                className="image- h-auto"
              />
            </Col>
          </Row>
        </Col>
        <Col xl={6} sm={12} xs={12}>
          <img
            src="./images/page-14-content/image-14-2.webp"
            className="image-page"
            style={{ height: "auto" }}
          />
        </Col>
      </Row>
    </ContainerAfterWorld>
  );
};

export default Page14Content;
