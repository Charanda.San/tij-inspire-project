import { ImageWiteQuote } from "@/components";
import { IPropsImageWithQuote } from "@/components/ImageWithQuote";
import React from "react";

export default function Page24Content() {
  const dataProps: IPropsImageWithQuote = {
    src: "./images/page-24-content/image-24-01.webp",
    quote: `“Despite the fact that women prisoners are increasing in number, they are often considered the forgotten population in prison settings. The 1955 United Nations Standard Minimum Rules for the Treatment of Prisoners apply to all
    prisoners without discrimination, including women. But in our rapidly changing world, we must
    confront the reality that the SMRs can no longer accommodate the specific needs of women.”
    `,
    subQuote: (
      <>
        Excerpt from the opening statement of Her Royal Highness Princess
        Bajrakitiyabha at the 18<sup>th</sup> Session of the Commission on Crime Prevention
        and Criminal Justice in Vienna from 16-24 April, 2009
      </>
    ),
    caption: `An inmate prays at the spirit house at Chiang Mai Women Correctional Institution.`,
  };
  return (
    <>
      <ImageWiteQuote {...dataProps} />
    </>
  );
}
