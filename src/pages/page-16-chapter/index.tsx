import React from "react";
import { Chapter } from "src/components";
import { IPropsChapter } from "@/components/Chapter";
export default function Page16Chapter() {
  const data: IPropsChapter = {
    src: "./images/page-16-chapter/image-chapter-16.webp",
    title: "THE RIGHTS OF WOMEN",
    chapterText: "CHAPTER 1",
    content:
      "Recognizing that female inmates were not being treated with gender sensitivity, Princess Bajrakitiyabha moved to change prison systems not only in Thailand but worldwide, first through the development and implementation of the Kamlangjai Project, and later through the introduction of the so-called Bangkok Rules, which were adopted by the United Nations. The impacts are now being seen, with prisons offering new programs, vocational training, and mother care facilities, among other initiatives. This significant work was achieved through hands-on engagement and international diplomacy by the Princess, the Thailand Institute of Justice, the Ministry of Justice and other government agencies.",
    descriptionImg: (
      <p style={{maxWidth:'260px'}}>
        Morning roll call at Chiang Mai Women Correctional Institution, which
        has a population of over 2,000 inmates.
      </p>
    ),
  };
  return <Chapter {...data} />;
}
