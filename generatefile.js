const fs = require("fs");
const babel = require("@babel/core");

const tsxFilePath =
  __dirname + "/src/pages/page-05-content/index.tsx";
const outputFile = __dirname + "/.file-template/page.txt";

const babelOptions = {
  presets: ["@babel/preset-react"],
};

fs.readFile(tsxFilePath, "utf8", (err, data) => {
  if (err) {
    console.error("Error reading the TSX file:", err);
    return;
  }

  babel.transform(data, babelOptions, (err, result) => {
    if (err) {
      console.error("Error transpiling TSX to JS:", err);
      return;
    }

    console.log(result)

    const textContent = extractTextContent(result.code);
    fs.writeFile(outputFile, textContent, "utf8", (err) => {
      if (err) {
        console.error("Error writing output file:", err);
        return;
      }

      console.log("Text content extracted and saved to", outputFile);
    });
  });
});

function extractTextContent(jsCode) {
  // Extract text content from JavaScript code using regular expressions
  const textRegex = />([^<]+)</g;
  const matches = jsCode.match(textRegex);

  // Join all matches into a single string
  const textContent = matches
    ? matches.map((match) => match.slice(1, -1)).join("\n")
    : "";

  return textContent;
}
